<?php

namespace RetoApiBundle\Entity;

use PtiBundle\Entity\Traits\DatetimeTrait;
use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Traits\BancSabadellIdTrait;
use RetoApiBundle\Entity\Interfaces\HistoricRedAccumulatedInterface;

class HistoricRedAccumulated implements HistoricRedAccumulatedInterface
{
    use IdentifyTrait;
    use DatetimeTrait;
    use BancSabadellIdTrait;

    private $red;

    private $challenge;

    private $anual;

    private $trim1;

    private $trim2;

    private $trim3;

    private $trim4;

    public function create(
        $red,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    ) {
        $this->red = $red;
        $this->challenge = $challenge;
        $this->bancsabadellId = $bankSabadellId;
        $this->trim1 = $trim1;
        $this->trim2 = $trim2;
        $this->trim3 = $trim3;
        $this->trim4 = $trim4;
        $this->anual = $anual;
    }

    public function update(
        $red,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    ) {
        $this->create(
            $red,
            $challenge,
            $bankSabadellId,
            $trim1,
            $trim2,
            $trim3,
            $trim4,
            $anual
        );
    }

    public function setUnit($red)
    {
        $this->red = $red;
    }

    public function getRed()
    {
        return $this->red;
    }

    public function setChallenge($challenge)
    {
        $this->challenge = $challenge;
    }

    public function setAnual($anual)
    {
        $this->anual = $anual;
    }

    public function setTrim1($trim1)
    {
        $this->trim1 = $trim1;
    }

    public function setTrim2($trim2)
    {
        $this->trim2 = $trim2;
    }

    public function setTrim3($trim3)
    {
        $this->trim3 = $trim3;
    }

    public function setTrim4($trim4)
    {
        $this->trim4 = $trim4;
    }
}
