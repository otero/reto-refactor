<?php

namespace RetoApiBundle\Entity;

use PtiBundle\Entity\Traits\DatetimeTrait;
use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Traits\NameTrait;
use RetoApiBundle\Entity\Traits\OnficinaTrait;
use Doctrine\Common\Collections\ArrayCollection;
use RetoApiBundle\Entity\Traits\BancSabadellIdTrait;
use RetoApiBundle\Entity\Interfaces\TerritorialInterface;
use RetoApiBundle\Entity\Interfaces\HeadquarterInterface;

class Territorial implements HeadquarterInterface, TerritorialInterface
{
    use IdentifyTrait;
    use BancSabadellIdTrait;
    use NameTrait;
    use OnficinaTrait;
    use DatetimeTrait;

    protected $bank;

    protected $redes;

    protected $historicTerritorial;

    protected $historicTerritorialAccumulated;

    public function __construct()
    {
        $this->redes = new ArrayCollection();
    }

    public function create($bancSabadellId, $name)
    {
        $this->name = $name;
        $this->bancsabadellId = $bancSabadellId;
    }

    public function getBank()
    {
        return $this->bank;
    }

    public function setBank($bank)
    {
        $this->bank = $bank;
    }

    public function addRed(Red $red)
    {
        $this->redes[] = $red;
        return $this;
    }

    public function removeRed(Red $red)
    {
        $this->redes->removeElement($red);
        return $this;
    }

    public function getRedes()
    {
        return $this->redes;
    }

    public function __toString()
    {
        return $this->getBancSabadellId() . ' :: ' . $this->getName();
    }
}