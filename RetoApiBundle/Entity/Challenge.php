<?php

namespace RetoApiBundle\Entity;

use PtiBundle\Entity\Traits\DatetimeTrait;
use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Traits\NameTrait;
use RetoApiBundle\Entity\Interfaces\ChallengeInterface;

class Challenge implements ChallengeInterface
{
    use IdentifyTrait;
    use NameTrait;
    use DatetimeTrait;

    /**
     * @var int
     */
    protected $year;

    /**
     * @var int
     */
    protected $active;


    /**
     * @var
     */
    private $historics;


    private $historicOfficeAccumulated;
    private $historicRegionalAccumulated;
    private $historicRedAccumulated;
    private $historicTerritorialAccumulated;



    /**
     * Return object as string
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }


    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }
}