<?php

namespace RetoApiBundle\Entity;


use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Interfaces\RankingRedInterface;

class RankingRed implements RankingRedInterface
{
    use IdentifyTrait;

    const LAST_DOWN = 0;

    const LAST_EQUAL = 1;

    const LAST_UP = 2;

    protected $historicOfficeAccumulated;

    protected $cmpAccumulated;

    protected $rank;

    protected $last;

    private $ranking;

    public function create($historicOfficeAccumulated, $cmpAccumulated, $ranking, $last)
    {
        $this->historicOfficeAccumulated = $historicOfficeAccumulated;
        $this->cmpAccumulated = $cmpAccumulated;
        $this->ranking = $ranking;
        $this->last = $last;
    }

    public function update($cmpAccumulated, $ranking, $last)
    {
        $this->cmpAccumulated = $cmpAccumulated;
        $this->ranking = $ranking;
        $this->last = $last;
    }

    /**
     * Set ranking
     *
     * @param integer $ranking
     *
     * @return RankingRed
     */
    public function setRanking($ranking)
    {
        $this->ranking = $ranking;

        return $this;
    }

    /**
     * Get ranking
     *
     * @return integer
     */
    public function getRanking()
    {
        return $this->ranking;
    }


    /**
     * Get last
     *
     * @return integer
     */
    public function getLast()
    {
        return $this->last;
    }

    /**
     * Get cmpAccumulated
     *
     * @return float
     */
    public function getCmpAccumulated()
    {
        return $this->cmpAccumulated;
    }

    /**
     * Get historicOfficeAccumulated
     *
     * @return \RetoApiBundle\Entity\HistoricOfficeAccumulated
     */
    public function getHistoricOfficeAccumulated()
    {
        return $this->historicOfficeAccumulated;
    }
}
