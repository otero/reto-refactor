<?php

namespace RetoApiBundle\Entity;

use PtiBundle\Entity\Traits\DatetimeTrait;
use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Traits\NameTrait;
use RetoApiBundle\Entity\Interfaces\TargetInterface;

class Target implements TargetInterface
{
    use IdentifyTrait;
    use NameTrait;
    use DatetimeTrait;

    protected $bsIndicador;

    protected $historicOffice;

    protected $historicRegional;

    protected $historicRed;

    protected $historicTerritorial;

    protected $targetInformation;

    protected $targetMeans;

    private $targetTips;

    public function create($bsIndicador, $name)
    {
        $this->bsIndicador = $bsIndicador;
        $this->name = $name;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getBsIndicador()
    {
        return $this->bsIndicador;
    }

    public function setBsIndicador($bsIndicador)
    {
        $this->bsIndicador = $bsIndicador;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->historicOffice = new \Doctrine\Common\Collections\ArrayCollection();
        $this->historicRegional = new \Doctrine\Common\Collections\ArrayCollection();
        $this->historicRed = new \Doctrine\Common\Collections\ArrayCollection();
        $this->historicTerritorial = new \Doctrine\Common\Collections\ArrayCollection();
        $this->targetMean = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add historicOffice
     *
     * @param \RetoApiBundle\Entity\HistoricOffice $historicOffice
     *
     * @return Target
     */
    public function addHistoricOffice(\RetoApiBundle\Entity\HistoricOffice $historicOffice)
    {
        $this->historicOffice[] = $historicOffice;

        return $this;
    }

    /**
     * Remove historicOffice
     *
     * @param \RetoApiBundle\Entity\HistoricOffice $historicOffice
     */
    public function removeHistoricOffice(\RetoApiBundle\Entity\HistoricOffice $historicOffice)
    {
        $this->historicOffice->removeElement($historicOffice);
    }

    /**
     * Get historicOffice
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistoricOffice()
    {
        return $this->historicOffice;
    }

    /**
     * Add historicRegional
     *
     * @param \RetoApiBundle\Entity\HistoricRegional $historicRegional
     *
     * @return Target
     */
    public function addHistoricRegional(\RetoApiBundle\Entity\HistoricRegional $historicRegional)
    {
        $this->historicRegional[] = $historicRegional;

        return $this;
    }

    /**
     * Remove historicRegional
     *
     * @param \RetoApiBundle\Entity\HistoricRegional $historicRegional
     */
    public function removeHistoricRegional(\RetoApiBundle\Entity\HistoricRegional $historicRegional)
    {
        $this->historicRegional->removeElement($historicRegional);
    }

    /**
     * Get historicRegional
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistoricRegional()
    {
        return $this->historicRegional;
    }

    /**
     * Add historicRed
     *
     * @param \RetoApiBundle\Entity\HistoricRed $historicRed
     *
     * @return Target
     */
    public function addHistoricRed(\RetoApiBundle\Entity\HistoricRed $historicRed)
    {
        $this->historicRed[] = $historicRed;

        return $this;
    }

    /**
     * Remove historicRed
     *
     * @param \RetoApiBundle\Entity\HistoricRed $historicRed
     */
    public function removeHistoricRed(\RetoApiBundle\Entity\HistoricRed $historicRed)
    {
        $this->historicRed->removeElement($historicRed);
    }

    /**
     * Get historicRed
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistoricRed()
    {
        return $this->historicRed;
    }

    /**
     * Add historicTerritorial
     *
     * @param \RetoApiBundle\Entity\HistoricTerritorial $historicTerritorial
     *
     * @return Target
     */
    public function addHistoricTerritorial(\RetoApiBundle\Entity\HistoricTerritorial $historicTerritorial)
    {
        $this->historicTerritorial[] = $historicTerritorial;

        return $this;
    }

    /**
     * Remove historicTerritorial
     *
     * @param \RetoApiBundle\Entity\HistoricTerritorial $historicTerritorial
     */
    public function removeHistoricTerritorial(\RetoApiBundle\Entity\HistoricTerritorial $historicTerritorial)
    {
        $this->historicTerritorial->removeElement($historicTerritorial);
    }

    /**
     * Get historicTerritorial
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistoricTerritorial()
    {
        return $this->historicTerritorial;
    }

    /**
     * Add targetMean
     *
     * @param \RetoApiBundle\Entity\TargetMean $targetMean
     *
     * @return Target
     */
    public function addTargetMean(\RetoApiBundle\Entity\TargetMean $targetMean)
    {
        $this->targetMean[] = $targetMean;

        return $this;
    }

    /**
     * Remove targetMean
     *
     * @param \RetoApiBundle\Entity\TargetMean $targetMean
     */
    public function removeTargetMean(\RetoApiBundle\Entity\TargetMean $targetMean)
    {
        $this->targetMean->removeElement($targetMean);
    }

    /**
     * Get targetMean
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargetMean()
    {
        return $this->targetMean;
    }

    /**
     * Set targetInformation
     *
     * @param \RetoApiBundle\Entity\TargetInformation $targetInformation
     *
     * @return Target
     */
    public function setTargetInformation(\RetoApiBundle\Entity\TargetInformation $targetInformation = null)
    {
        $this->targetInformation = $targetInformation;

        return $this;
    }

    /**
     * Get targetInformation
     *
     * @return \RetoApiBundle\Entity\TargetInformation
     */
    public function getTargetInformation()
    {
        return $this->targetInformation;
    }

    /**
     * Get targetMeans
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargetMeans()
    {
        return $this->targetMeans;
    }

    /**
     * Add targetTip
     *
     * @param \RetoApiBundle\Entity\TargetTip $targetTip
     *
     * @return Target
     */
    public function addTargetTip(\RetoApiBundle\Entity\TargetTip $targetTip)
    {
        $this->targetTips[] = $targetTip;

        return $this;
    }

    /**
     * Remove targetTip
     *
     * @param \RetoApiBundle\Entity\TargetTip $targetTip
     */
    public function removeTargetTip(\RetoApiBundle\Entity\TargetTip $targetTip)
    {
        $this->targetTips->removeElement($targetTip);
    }

    /**
     * Get targetTips
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargetTips()
    {
        return $this->targetTips;
    }
}
