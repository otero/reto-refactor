<?php
/**
 * Created by PhpStorm.
 * User: Marc
 * Date: 17/07/2017
 * Time: 9:54
 */

namespace RetoApiBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use PtiBundle\Entity\Traits\DatetimeTrait;
use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Interfaces\HeadquarterInterface;
use RetoApiBundle\Entity\Traits\BancSabadellIdTrait;
use RetoApiBundle\Entity\Traits\NameTrait;

class Bank implements HeadquarterInterface
{
    use IdentifyTrait;
    use BancSabadellIdTrait;
    use NameTrait;
    use DatetimeTrait;





    /**
     * @var Collection
     */
    protected $territorials;




    public function __construct()
    {
        $this->territorials = new ArrayCollection();
    }

    /**
     * @param Territorial $territorial
     * @return $this
     */
    public function addTerritorial(Territorial $territorial)
    {
        $this->territorials[] = $territorial;
        return $this;
    }

    /**
     * @param Territorial $territorial
     * @return $this
     */
    public function removeTerritorial(Territorial $territorial)
    {
        $this->territorials->removeElement($territorial);
        return $this;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getTerritorials()
    {
        return $this->territorials;
    }

    /**
     * Return object as string
     * @return string
     */
    public function __toString()
    {
        return $this->getBancSabadellId() . ' :: ' . $this->getName();
    }
}