<?php

namespace RetoApiBundle\Entity;

use RetoApiBundle\Entity\Interfaces\HistoricLogInterface;

class HistoricLog implements HistoricLogInterface
{
    const BUNDLE_RETO = 'reto';

    CONST CHANNEL_CSV = 'process_csv';

    CONST CHANNEL_PROJECTION_RANKING_TERRITORIAL = 'projection_ranking_territorial';

    CONST CHANNEL_PROJECTION_RANKING_REGIONAL = 'projection_ranking_regional';

    CONST CHANNEL_PROJECTION_RANKING_RED = 'projection_ranking_red';

    CONST CHANNEL_PROJECTION_RANKING_OFFICE = 'projection_ranking_office';

    private $id;

    private $channel;

    private $bundle;

    private $message;

    private $context;

    private $level;

    private $levelName;

    private $extra;

    private $createdAt;

    public function create(
        $channel,
        $bundle,
        $message,
        $context,
        $level,
        $levelName,
        $extra = array()
    ) {
        $this->channel = $channel;
        $this->bundle = $bundle;
        $this->message = $message;
        $this->context = $context;
        $this->level = $level;
        $this->levelName = $levelName;
        $this->extra = $extra;
    }

    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getChannel()
    {
        return $this->channel;
    }

    public function getBundle()
    {
        return $this->bundle;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function getLevelName()
    {
        return $this->levelName;
    }

    public function getExtra()
    {
        return $this->extra;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
