<?php

namespace RetoApiBundle\Entity;


use RetoApiBundle\Entity\Interfaces\TargetMeanInterface;

class TargetMean implements TargetMeanInterface
{
    const TYPE_INTERNAL = 1;

    const TYPE_EXTERNAL = 2;

    const DIR = 'uploads/targetMean';

    protected $target;

    protected $title;

    protected $description;

    protected $fileMean;

    protected $typeMean;

    protected $link;

    private $id;

    public static function getTypes()
    {
        return array(
            self::TYPE_INTERNAL => 'Recursos internos',
            self::TYPE_EXTERNAL => 'Recursos externos'
        );
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return TargetMean
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return TargetMean
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set fileMean
     *
     * @param string $fileMean
     *
     * @return TargetMean
     */
    public function setFileMean($fileMean)
    {
        // Avoid deleting image when no new image selected on admin edit
        if ($fileMean !== null) {
            $this->fileMean = $fileMean;
        }

        return $this;
    }

    /**
     * Get fileMean
     *
     * @return string
     */
    public function getFileMean()
    {
        return $this->fileMean;
    }

    /**
     * Set typeMean
     *
     * @param integer $typeMean
     *
     * @return TargetMean
     */
    public function setTypeMean($typeMean)
    {
        $this->typeMean = $typeMean;

        return $this;
    }

    /**
     * Get typeMean
     *
     * @return integer
     */
    public function getTypeMean()
    {
        return $this->typeMean;
    }

    /**
     * Set target
     *
     * @param \RetoApiBundle\Entity\Target $target
     *
     * @return TargetMean
     */
    public function setTarget(\RetoApiBundle\Entity\Target $target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \RetoApiBundle\Entity\Target
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }
}
