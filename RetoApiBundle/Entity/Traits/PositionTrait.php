<?php
/**
 * Created by PhpStorm.
 * User: Marc
 * Date: 18/07/2017
 * Time: 8:18
 */

namespace RetoApiBundle\Entity\Traits;

trait PositionTrait
{
    /**
     * @var integer
     */
    protected $position;

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return PositionTrait
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }
}