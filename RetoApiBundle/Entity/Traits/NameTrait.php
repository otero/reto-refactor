<?php
/**
 * Created by PhpStorm.
 * User: Marc
 * Date: 17/07/2017
 * Time: 17:15
 */

namespace RetoApiBundle\Entity\Traits;

//todo: remove setters
trait NameTrait
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}