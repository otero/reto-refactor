<?php

namespace RetoApiBundle\Entity\Traits;

//todo: remove setters
trait BancSabadellIdTrait
{
    /**
     * @var integer
     */
    protected $bancsabadellId;

    /**
     * @return mixed
     */
    public function getBancSabadellId()
    {
        return $this->bancsabadellId;
    }

    /**
     * @param mixed $bsId
     */
    public function setBancSabadellId($bsId)
    {
        $this->bancsabadellId = $bsId;
    }
}
