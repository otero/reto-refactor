<?php
/**
 * Date: 14/08/2017
 * Time: 2:15
 */

namespace RetoApiBundle\Entity\Traits;

trait CurrentLastPositionTrait
{
    /**
     * @var int
     */
    private $position;

    /**
     * @var int
     */
    private $last_position;


    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return CurrentLastPositionTrait
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return int
     */
    public function getLastPosition()
    {
        return $this->last_position;
    }

    /**
     * @param int $last_position
     * @return CurrentLastPositionTrait
     */
    public function setLastPosition($last_position)
    {
        $this->last_position = $last_position;
        return $this;
    }
}