<?php
/**
 * Created by PhpStorm.
 * User: Marc
 * Date: 18/07/2017
 * Time: 8:15
 */

namespace RetoApiBundle\Entity\Traits;

trait OnficinaTrait
{
    /**
     * @var integer
     */
    protected $onficinas;

    /**
     * @return int
     */
    public function getOnficinas()
    {
        return $this->onficinas;
    }

    /**
     * @param int $onficinas
     * @return OnficinaTrait
     */
    public function setOnficinas($onficinas)
    {
        $this->onficinas = $onficinas;
        return $this;
    }
}