<?php

namespace RetoApiBundle\Entity\Traits;

trait TotalsTrait
{
    /**
     * @var int
     */
    protected $total_reto_offices;

    /**
     * @var int
     */
    protected $total_onficinas;

    /**
     * @var int
     */
    protected $total_accomplisheds;

    /**
     * @var int
     */
    protected $total_reto_fields;

    /**
     * @return int
     */
    public function getTotalRetoOffices()
    {
        return $this->total_reto_offices;
    }

    /**
     * @param int $total_reto_offices
     * @return TotalsTrait
     */
    public function setTotalRetoOffices($total_reto_offices)
    {
        $this->total_reto_offices = $total_reto_offices;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalOnficinas()
    {
        return $this->total_onficinas;
    }

    /**
     * @param int $total_onficinas
     * @return TotalsTrait
     */
    public function setTotalOnficinas($total_onficinas)
    {
        $this->total_onficinas = $total_onficinas;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalAccomplisheds()
    {
        return $this->total_accomplisheds;
    }

    /**
     * @param int $total_accomplisheds
     * @return TotalsTrait
     */
    public function setTotalAccomplisheds($total_accomplisheds)
    {
        $this->total_accomplisheds = $total_accomplisheds;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalRetoFields()
    {
        return $this->total_reto_fields;
    }

    /**
     * @param int $total_reto_fields
     * @return TotalsTrait
     */
    public function setTotalRetoFields($total_reto_fields)
    {
        $this->total_reto_fields = $total_reto_fields;
        return $this;
    }
}