<?php

namespace RetoApiBundle\Entity;

class CSV
{
    const DELIMITER = ';';
    const POS_UNIT = 1;
    const POS_TARGET = 3;
    const POS_TARGET_DESCR = 4;
    const POS_TERRITORIAL_BS_ID = 7;
    const POS_TERRITORIAL_NAME = 8;
    const POS_RED_BS_ID = 11;
    const POS_RED_NAME = 12;
    const POS_REGIONAL_BS_ID = 13;
    const POS_REGIONAL_NAME = 14;
    const POS_OFFICE_BS_ID = 15;
    const POS_OFFICE_NAME = 16;
    const POS_TARGET_ACCMP = 17;
    const POS_ACCMP_ACUMULAT = 18;
    const POS_ACCMP_TRIM_1 = 19;
    const POS_ACCMP_TRIM_2 = 20;
    const POS_ACCMP_TRIM_3 = 21;
    const POS_ACCMP_TRIM_4 = 22;
}
