<?php

namespace RetoApiBundle\Entity;

use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Interfaces\HistoricTerritorialInterface;


class HistoricTerritorial implements HistoricTerritorialInterface
{
    use IdentifyTrait;

    private $accomplishment;

    private $position;

    protected $targets;

    protected $createdAt;

    private $territorial;

    private $historic;

    private $target;

    public function create($territorial, $historic, $target, $accomplishment, $position)
    {
        $this->territorial = $territorial;
        $this->historic = $historic;
        $this->target = $target;
        $this->accomplishment = $accomplishment;
        $this->position = $position;
    }

    public function updateTimestamps()
    {
        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getAccomplishment()
    {
        return $this->accomplishment;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function getTargets()
    {
        return $this->targets;
    }

    public function getHistoric()
    {
        return $this->historic;
    }

    public function getTarget()
    {
        return $this->target;
    }

    public function getTerritorial()
    {
        return $this->territorial;
    }
}
