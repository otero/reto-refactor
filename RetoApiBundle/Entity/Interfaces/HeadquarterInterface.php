<?php

namespace RetoApiBundle\Entity\Interfaces;

interface HeadquarterInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getBancSabadellId();

    /**
     * @param $bsId
     */
    public function setBancsabadellId($bsId);

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @param $name
     */
    public function setName($name);

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @param \DateTime $dateTime
     */
    public function setCreatedAt(\DateTime $dateTime);

    /**
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * @param \DateTime $dateTime
     */
    public function setUpdatedAt(\DateTime $dateTime);
}
