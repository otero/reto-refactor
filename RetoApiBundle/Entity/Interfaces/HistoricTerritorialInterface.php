<?php

namespace RetoApiBundle\Entity\Interfaces;


interface HistoricTerritorialInterface
{
    public function create($territorial, $historic, $target, $accomplishment, $position);
}
