<?php

namespace RetoApiBundle\Entity\Interfaces;

interface TerritorialInterface
{
    public function create($bancSabadellId, $name);
}
