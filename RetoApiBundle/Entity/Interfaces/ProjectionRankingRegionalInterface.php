<?php

namespace RetoApiBundle\Entity\Interfaces;


interface ProjectionRankingRegionalInterface
{
    public function create(
        $historicRegionalAccumulatedId,
        $regionalId,
        $regionalName,
        $redId,
        $redName,
        $territorialId,
        $territorialName,
        $ranking,
        $medalPlatinum,
        $medalGolden,
        $medalSilver,
        $medalBronze,
        $cmpAnnual,
        $cmpQuarterly1,
        $cmpQuarterly2,
        $cmpQuarterly3,
        $cmpQuarterly4,
        $officesTransform
    );

    public function update(
        $historicRegionalAccumulatedId,
        $regionalId,
        $regionalName,
        $redId,
        $redName,
        $territorialId,
        $territorialName,
        $ranking,
        $medalPlatinum,
        $medalGolden,
        $medalSilver,
        $medalBronze,
        $cmpAnnual,
        $cmpQuarterly1,
        $cmpQuarterly2,
        $cmpQuarterly3,
        $cmpQuarterly4,
        $officesTransform
    );

    public function updateRankingOfficesTransform($ranking, $position);

    public function getId();

    public function getHistoricRegionalAccumulatedId();

    public function getRegionalId();

    public function getRegionalName();

    public function getRedId();

    public function getRedName();

    public function getRankingOfficesTransform();

    public function getTerritorialId();

    public function getTerritorialName();

    public function getRanking();

    public function getMedals();

    public function getOfficeTransform();

    public function getCmpAnnual();

    public function getCmpQuarterly1();

    public function getCmpQuarterly2();

    public function getCmpQuarterly3();

    public function getCmpQuarterly4();

    public function getCreatedAt();

    public function getUpdatedAt();
}
