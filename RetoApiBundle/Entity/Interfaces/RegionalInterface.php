<?php

namespace RetoApiBundle\Entity\Interfaces;

interface RegionalInterface
{
    public function create($bancSabadellId, $name, $red);
}
