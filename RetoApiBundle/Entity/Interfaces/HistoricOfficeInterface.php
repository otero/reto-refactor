<?php

namespace RetoApiBundle\Entity\Interfaces;


interface HistoricOfficeInterface
{
    public function create($office, $historic, $target, $accomplishment, $position);
}
