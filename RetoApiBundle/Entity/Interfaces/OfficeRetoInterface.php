<?php

namespace RetoApiBundle\Entity\Interfaces;

interface OfficeRetoInterface
{
    public function create($bancSabadellId, $name, $regional);
}
