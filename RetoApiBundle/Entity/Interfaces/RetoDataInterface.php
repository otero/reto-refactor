<?php
/**
 * Date: 19/07/2017
 * Time: 10:47
 */

namespace RetoApiBundle\Entity\Interfaces;

interface RetoDataInterface
{
    public function getId();

    public function getChallengeField();

    public function setChallengeField($challenge_field);

    public function getValue();

    public function setValue($value);

    public function getValueType();

    public function setValueType($value_type);

    public function getAccomplished();

    public function setAccomplished($accomplished);

    public function getWeekNumber();

    public function setWeekNumber($week_number);

    public function getYear();

    public function setYear($year);

    public function getCreatedAt();

    public function setCreatedAt(\DateTime $createdAt);

    public function getUpdatedAt();

    public function setUpdatedAt(\DateTime $updatedAt);
}