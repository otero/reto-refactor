<?php

namespace RetoApiBundle\Entity\Interfaces;

interface HistoricRedAccumulatedInterface
{
    public function create(
        $red,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    );

    public function update(
        $red,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    );
}