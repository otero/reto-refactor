<?php

namespace RetoApiBundle\Entity\Interfaces;

interface HistoricRegionalAccumulatedInterface
{
    public function create(
        $regional,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    );

    public function update(
        $regional,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    );
}