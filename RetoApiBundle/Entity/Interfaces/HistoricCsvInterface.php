<?php
/**
 * Created by PhpStorm.
 * User: Marc
 * Date: 24/07/2017
 * Time: 18:21
 */

namespace RetoApiBundle\Entity\Interfaces;

interface HistoricCsvInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getPath();

    /**
     * @param $path
     * @return mixed
     */
    public function setPath($path);

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @param \DateTime $dateTime
     */
    public function setCreatedAt(\DateTime $dateTime);
}