<?php

namespace RetoApiBundle\Entity\Interfaces;


interface HistoricTerritorialAccumulatedInterface
{
    public function create(
        $territorial,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    );

    public function update(
        $territorial,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    );
}