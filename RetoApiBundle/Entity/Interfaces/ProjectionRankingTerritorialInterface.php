<?php
namespace RetoApiBundle\Entity\Interfaces;


interface ProjectionRankingTerritorialInterface
{
    public function create(
        $historicTerritorialAccumulatedId,
        $territorialId,
        $territorialName,
        $ranking,
        $cmpAnnual,
        $cmpQuarterly1,
        $cmpQuarterly2,
        $cmpQuarterly3,
        $cmpQuarterly4,
        $officesTransform
    );

    public function update(
        $historicTerritorialAccumulatedId,
        $territorialId,
        $territorialName,
        $ranking,
        $cmpAnnual,
        $cmpQuarterly1,
        $cmpQuarterly2,
        $cmpQuarterly3,
        $cmpQuarterly4,
        $officesTransform
    );

    public function updateRankingOfficesTransform($ranking, $position);

    public function getId();

    public function getHistoricTerritorialAccumulatedId();

    public function getRanking();

    public function getRankingOfficesTransform();

    public function getLast();

    public function getRedId();

    public function getRedName();

    public function getTerritorialId();

    public function getTerritorialName();

    public function getOfficesTransform();

    public function getCmpAnnual();

    public function getCmpQuarterly1();

    public function getCmpQuarterly2();

    public function getCmpQuarterly3();

    public function getCmpQuarterly4();

    public function getCreatedAt();

    public function getUpdatedAt();
}