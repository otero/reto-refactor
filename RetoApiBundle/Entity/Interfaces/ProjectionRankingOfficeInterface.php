<?php

namespace RetoApiBundle\Entity\Interfaces;


interface ProjectionRankingOfficeInterface
{
    public function create(
        $historicOfficeAccumulatedId,
        $officeId,
        $officeName,
        $regionalId,
        $regionalName,
        $territorialId,
        $territorialName,
        $redId,
        $redName,
        $ranking,
        $cmpQuarterly1,
        $cmpQuarterly2,
        $cmpQuarterly3,
        $cmpQuarterly4,
        $cmpAnnual,
        $indexTopRanking,
        $isRedPlatinum
    );

    public function update(
        $historicOfficeAccumulatedId,
        $officeId,
        $officeName,
        $regionalId,
        $regionalName,
        $territorialId,
        $territorialName,
        $redId,
        $redName,
        $ranking,
        $cmpQuarterly1,
        $cmpQuarterly2,
        $cmpQuarterly3,
        $cmpQuarterly4,
        $cmpAnnual,
        $indexTopRanking,
        $isRedPlatinum
    );
    public function getHistoricOfficeAccumulatedId();

    public function getId();

    public function getOfficeId();

    public function getOfficeName();

    public function getTerritorialId();

    public function getTerritorialName();

    public function getRedId();

    public function getRedName();

    public function getRegionalId();

    public function getRegionalName();

    public function getRanking();

    public function getRankingLast();

    public function getMedal();

    public function getOfficeTransform();

    public function getCmpQuarterly1();

    public function getCmpQuarterly2();

    public function getCmpQuarterly3();

    public function getCmpQuarterly4();

    public function getCreatedAt();

    public function getCmpAnnual();

    public function getCmpAnnualPercentage();

    public function getCmpAnnualLast();

    public function setCmpAnnualLast($last);

    public function setRankingLast($last);
}
