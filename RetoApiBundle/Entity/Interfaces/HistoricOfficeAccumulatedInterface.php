<?php

namespace RetoApiBundle\Entity\Interfaces;

interface HistoricOfficeAccumulatedInterface
{
    public function create(
        $office,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    );

    public function update(
        $office,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    );
}