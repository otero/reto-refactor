<?php

namespace RetoApiBundle\Entity\Interfaces;


interface HistoricRedInterface
{
    public function create($red, $historic, $target, $accomplishment, $position);
}
