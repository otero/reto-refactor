<?php

namespace RetoApiBundle\Entity\Interfaces;


interface HistoricLogInterface
{
    public function create(
        $channel,
        $bundle,
        $message,
        $context,
        $level,
        $levelName,
        $extra
    );

    public function onPrePersist();

    public function getId();

    public function getChannel();

    public function getBundle();

    public function getMessage();

    public function getContext();

    public function getLevel();

    public function getLevelName();

    public function getExtra();

    public function getCreatedAt();
}