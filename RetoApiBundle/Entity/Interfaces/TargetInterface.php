<?php

namespace RetoApiBundle\Entity\Interfaces;


interface TargetInterface
{
    public function create($bsIndicador, $name);
}