<?php

namespace RetoApiBundle\Entity\Interfaces;


interface HistoricInterface
{
    public function getFilename();

    public function changeStatusToProcessed();
}
