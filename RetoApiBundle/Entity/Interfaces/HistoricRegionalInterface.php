<?php

namespace RetoApiBundle\Entity\Interfaces;


interface HistoricRegionalInterface
{
    public function create($regional, $historic, $target, $accomplishment, $position);
}
