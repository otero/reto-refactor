<?php

namespace RetoApiBundle\Entity\Interfaces;

interface RankingRedInterface
{
    public function create($historicOfficeAccumulated, $cmpAccumulated, $ranking, $last);

    public function update($cmpAccumulated, $ranking, $last);
}
