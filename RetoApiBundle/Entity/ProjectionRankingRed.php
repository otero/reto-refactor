<?php

namespace RetoApiBundle\Entity;


use RetoApiBundle\Entity\Interfaces\ProjectionRankingRedInterface;

class ProjectionRankingRed implements ProjectionRankingRedInterface
{
    const LAST_DOWN = 0;

    const LAST_EQUAL = 1;

    const LAST_UP = 2;

    protected $id;

    protected $historicRedAccumulatedId;

    protected $ranking;

    protected $rankingOfficesTransform;

    protected $redId;

    protected $redName;

    protected $territorialId;

    protected $territorialName;

    protected $officesTransform;

    protected $cmpAnnual;

    protected $cmpQuarterly1;

    protected $cmpQuarterly2;

    protected $cmpQuarterly3;

    protected $cmpQuarterly4;

    protected $last;

    protected $createdAt;

    protected $updatedAt;

    public function create(
        $historicRedAccumulatedId,
        $redId,
        $redName,
        $territorialId,
        $territorialName,
        $ranking,
        $cmpAnnual,
        $cmpQuarterly1,
        $cmpQuarterly2,
        $cmpQuarterly3,
        $cmpQuarterly4,
        $officesTransform
    ) {
        $this->historicRedAccumulatedId = $historicRedAccumulatedId;
        $this->redId = $redId;
        $this->redName = $redName;
        $this->territorialId = $territorialId;
        $this->territorialName = $territorialName;
        $this->ranking = $ranking;
        $this->cmpAnnual = $cmpAnnual;
        $this->cmpQuarterly1 = $cmpQuarterly1;
        $this->cmpQuarterly2 = $cmpQuarterly2;
        $this->cmpQuarterly3 = $cmpQuarterly3;
        $this->cmpQuarterly4 = $cmpQuarterly4;
        $this->officesTransform = $officesTransform;
        $this->createdAt = new \DateTime('NOW');
        $this->updatedAt = new \DateTime('NOW');
    }

    public function update(
        $historicRedAccumulatedId,
        $redId,
        $redName,
        $territorialId,
        $territorialName,
        $ranking,
        $cmpAnnual,
        $cmpQuarterly1,
        $cmpQuarterly2,
        $cmpQuarterly3,
        $cmpQuarterly4,
        $officesTransform
    ) {
        $this->historicRedAccumulatedId = $historicRedAccumulatedId;
        $this->redId = $redId;
        $this->redName = $redName;
        $this->territorialId = $territorialId;
        $this->territorialName = $territorialName;
        $this->ranking = $ranking;
        $this->cmpAnnual = $cmpAnnual;
        $this->cmpQuarterly1 = $cmpQuarterly1;
        $this->cmpQuarterly2 = $cmpQuarterly2;
        $this->cmpQuarterly3 = $cmpQuarterly3;
        $this->cmpQuarterly4 = $cmpQuarterly4;
        $this->officesTransform = $officesTransform;
        $this->updatedAt = new \DateTime('NOW');
    }

    public function updateRankingOfficesTransform($ranking, $position)
    {
        $this->rankingOfficesTransform = $ranking;
        $this->last = $position;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRankingOfficesTransform()
    {
        return $this->rankingOfficesTransform;
    }

    public function getHistoricRedAccumulatedId()
    {
        return $this->historicRedAccumulatedId;
    }

    public function getRanking()
    {
        return $this->ranking;
    }

    public function getRedId()
    {
        return $this->redId;
    }

    public function getRedName()
    {
        return $this->redName;
    }

    public function getTerritorialId()
    {
        return $this->territorialId;
    }

    public function getTerritorialName()
    {
        return $this->territorialName;
    }

    public function getOfficesTransform()
    {
        return $this->officesTransform;
    }

    public function getCmpAnnual()
    {
        return $this->cmpAnnual;
    }

    public function getCmpQuarterly1()
    {
        return $this->cmpQuarterly1;
    }

    public function getCmpQuarterly2()
    {
        return $this->cmpQuarterly2;
    }

    public function getCmpQuarterly3()
    {
        return $this->cmpQuarterly3;
    }

    public function getCmpQuarterly4()
    {
        return $this->cmpQuarterly4;
    }


    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
