<?php

namespace RetoApiBundle\Entity;


use RetoApiBundle\Entity\Interfaces\ProjectionRankingOfficeInterface;

class ProjectionRankingOffice implements ProjectionRankingOfficeInterface
{
    const MEDAL_PLATINUM = 0;

    const MEDAL_GOLD = 1;

    const MEDAL_SILVER = 2;

    const MEDAL_BRONZE = 3;

    const OFFICE_TRANSFORM = TRUE;

    const OFFICE_NOT_TRANSFORM = FALSE;

    protected $id;

    protected $historicOfficeAccumulatedId;

    protected $officeId;

    protected $officeName;

    protected $territorialId;

    protected $territorialName;

    protected $redId;

    protected $redName;

    protected $regionalId;

    protected $regionalName;

    protected $ranking;

    protected $rankingLast;

    protected $medal;

    protected $officeTransform;

    protected $cmpAnnual;

    protected $cmpAnnualPercentage;

    protected $cmpAnnualLast;

    protected $cmpQuarterly1;

    protected $cmpQuarterly2;

    protected $cmpQuarterly3;

    protected $cmpQuarterly4;

    protected $createdAt;

    protected $updatedAt;

    private function isOfficeTransform()
    {
        $return = false;
        if ($this->cmpAnnual >= 1) {
            $return = true;
        }

        return $return;
    }

    private function setWithOutRedPlatinum($indexTopRanking)
    {
        $this->officeTransform = self::OFFICE_NOT_TRANSFORM;

        if ($this->isOfficeTransform()) {
            $this->officeTransform = self::OFFICE_TRANSFORM;

            if ($indexTopRanking >= 1 && $indexTopRanking <= 5) {
                $this->medal = self::MEDAL_GOLD;
            }

            if ($indexTopRanking >= 6 && $indexTopRanking <= 10) {
                $this->medal = self::MEDAL_SILVER;
            }

            if ($indexTopRanking >= 11 && $indexTopRanking <= 15) {
                $this->medal = self::MEDAL_BRONZE;
            }
        }
    }

    private function setWithRedPlatinum($indexTopRanking)
    {
        $this->officeTransform = self::OFFICE_NOT_TRANSFORM;

        if ($this->isOfficeTransform()) {
            $this->officeTransform = self::OFFICE_TRANSFORM;

            if ($indexTopRanking === 1) {
                $this->medal = self::MEDAL_PLATINUM;
            }

            if ($indexTopRanking >= 2 && $indexTopRanking <= 6) {
                $this->medal = self::MEDAL_GOLD;
            }

            if ($indexTopRanking >= 7 && $indexTopRanking <= 11) {
                $this->medal = self::MEDAL_SILVER;
            }

            if ($indexTopRanking >= 12 && $indexTopRanking <= 16) {
                $this->medal = self::MEDAL_BRONZE;
            }
        }
    }

    private function setOfficeTransformAndMedal($indexTopRanking, $isRedPlatinum)
    {
        if ($isRedPlatinum) {

            $this->setWithRedPlatinum($indexTopRanking);
        }

        if (!$isRedPlatinum) {

            $this->setWithOutRedPlatinum($indexTopRanking);
        }
    }

    public function create(
        $historicOfficeAccumulatedId,
        $officeId,
        $officeName,
        $regionalId,
        $regionalName,
        $territorialId,
        $territorialName,
        $redId,
        $redName,
        $ranking,
        $cmpQuarterly1,
        $cmpQuarterly2,
        $cmpQuarterly3,
        $cmpQuarterly4,
        $cmpAnnual,
        $indexTopRanking,
        $isRedPlatinum
    ) {
        $this->historicOfficeAccumulatedId = $historicOfficeAccumulatedId;
        $this->officeId = $officeId;
        $this->officeName = $officeName;
        $this->regionalId = $regionalId;
        $this->regionalName = $regionalName;
        $this->territorialId = $territorialId;
        $this->territorialName = $territorialName;
        $this->redId = $redId;
        $this->redName = $redName;
        $this->ranking = $ranking;
        $this->cmpAnnual = $cmpAnnual;
        $this->cmpAnnualPercentage = $this->cmpAnnual * 100;
        $this->cmpQuarterly1 = $cmpQuarterly1;
        $this->cmpQuarterly2 = $cmpQuarterly2;
        $this->cmpQuarterly3 = $cmpQuarterly3;
        $this->cmpQuarterly4 = $cmpQuarterly4;
        $this->createdAt = new \DateTime('NOW');
        $this->updatedAt = new \DateTime('NOW');

        $this->setOfficeTransformAndMedal($indexTopRanking, $isRedPlatinum);
    }

    public function update(
        $historicOfficeAccumulatedId,
        $officeId,
        $officeName,
        $regionalId,
        $regionalName,
        $territorialId,
        $territorialName,
        $redId,
        $redName,
        $ranking,
        $cmpQuarterly1,
        $cmpQuarterly2,
        $cmpQuarterly3,
        $cmpQuarterly4,
        $cmpAnnual,
        $indexTopRanking,
        $isRedPlatinum
    ) {
        $this->historicOfficeAccumulatedId = $historicOfficeAccumulatedId;
        $this->officeId = $officeId;
        $this->officeName = $officeName;
        $this->regionalId = $regionalId;
        $this->regionalName = $regionalName;
        $this->territorialId = $territorialId;
        $this->territorialName = $territorialName;
        $this->redId = $redId;
        $this->redName = $redName;
        $this->ranking = $ranking;
        $this->cmpAnnual = $cmpAnnual;
        $this->cmpAnnualPercentage = $this->cmpAnnual * 100;
        $this->cmpQuarterly1 = $cmpQuarterly1;
        $this->cmpQuarterly2 = $cmpQuarterly2;
        $this->cmpQuarterly3 = $cmpQuarterly3;
        $this->cmpQuarterly4 = $cmpQuarterly4;
        $this->updatedAt = new \DateTime('NOW');

        $this->setOfficeTransformAndMedal($indexTopRanking, $isRedPlatinum);
    }

    public function getId()
    {
        return $this->getId();
    }

    public function getHistoricOfficeAccumulatedId()
    {
        return $this->historicOfficeAccumulatedId;
    }

    public function getOfficeId()
    {
        return $this->officeId;
    }

    public function getOfficeName()
    {
        return $this->officeName;
    }

    public function getTerritorialId()
    {
        return $this->territorialId;
    }

    public function getTerritorialName()
    {
        return $this->territorialName;
    }

    public function getRedId()
    {
        return $this->redId;
    }

    public function getRedName()
    {
        return $this->redName;
    }

    public function getRegionalId()
    {
        return $this->regionalId;
    }

    public function getRegionalName()
    {
        return $this->regionalName;
    }

    public function getRanking()
    {
        return $this->ranking;
    }

    public function getRankingLast()
    {
        return $this->rankingLast;
    }

    public function getMedal()
    {
        return $this->medal;
    }

    public function getOfficeTransform()
    {
        return $this->officeTransform;
    }

    public function getCmpQuarterly1()
    {
        return $this->cmpQuarterly1;
    }

    public function getCmpQuarterly2()
    {
        return $this->cmpQuarterly2;
    }

    public function getCmpQuarterly3()
    {
        return $this->cmpQuarterly3;
    }

    public function getCmpQuarterly4()
    {
        return $this->cmpQuarterly3;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getCmpAnnual()
    {
        return $this->cmpAnnual;
    }

    public function getCmpAnnualPercentage()
    {
        return $this->cmpAnnualPercentage;
    }

    public function getCmpAnnualLast()
    {
        return $this->cmpAnnualLast;
    }

    public function setCmpAnnualLast($last)
    {
        $this->cmpAnnualLast = $last;

        return $this;
    }

    public function setRankingLast($last)
    {
        $this->rankingLast = $last;

        return $this;
    }
}
