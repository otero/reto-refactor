<?php

namespace RetoApiBundle\Entity;

use PtiBundle\Entity\Traits\DatetimeTrait;
use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Traits\NameTrait;
use RetoApiBundle\Entity\Traits\OnficinaTrait;
use Doctrine\Common\Collections\ArrayCollection;
use RetoApiBundle\Entity\Traits\BancSabadellIdTrait;
use RetoApiBundle\Entity\Interfaces\RegionalInterface;
use RetoApiBundle\Entity\Interfaces\HeadquarterInterface;

class Regional implements HeadquarterInterface, RegionalInterface
{
    use IdentifyTrait;
    use BancSabadellIdTrait;
    use NameTrait;
    use OnficinaTrait;
    use DatetimeTrait;

    protected $red;

    protected $offices;

    protected $historicRegional;

    protected $historicRegionalAccumulated;

    protected $officereto;

    public function __construct()
    {
        $this->offices = new ArrayCollection();
    }

    public function create($bancSabadellId, $name, $red)
    {
        $this->name = $name;
        $this->bancsabadellId = $bancSabadellId;
        $this->red = $red;
    }

    public function getRed()
    {
        return $this->red;
    }

    public function addOffice(OfficeReto $officeReto)
    {
        $this->offices[] = $officeReto;
        return $this;
    }

    public function removeOffice(OfficeReto $officeReto)
    {
        $this->offices->removeElement($officeReto);
        return $this;
    }

    public function getOffices()
    {
        return $this->offices;
    }

    public function __toString()
    {
        return $this->getBancSabadellId() . ' :: ' . $this->getName();
    }
}
