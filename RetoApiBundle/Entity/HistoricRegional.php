<?php

namespace RetoApiBundle\Entity;

use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Interfaces\HistoricRegionalInterface;

class HistoricRegional implements HistoricRegionalInterface
{
    use IdentifyTrait;

    private $accomplishment;

    private $position;

    private $regional;

    private $target;

    private $historic;

    protected $createdAt;

    public function create($regional, $historic, $target, $accomplishment, $position)
    {
        $this->regional = $regional;
        $this->historic = $historic;
        $this->target = $target;
        $this->accomplishment = $accomplishment;
        $this->position = $position;
    }

    public function updateTimestamps()
    {
        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setUnit($regional)
    {
        $this->regional = $regional;
    }

    public function setHistoric($historic)
    {
        $this->historic = $historic;
    }

    public function setAccomplishment($accomplishment)
    {
        $this->accomplishment = $accomplishment;
    }

    public function getAccomplishment()
    {
        return $this->accomplishment;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function getTarget()
    {
        return $this->target;
    }

    public function setTarget(Target $target)
    {
        $this->target = $target;
        return $this;
    }
}
