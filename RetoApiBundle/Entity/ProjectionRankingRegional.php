<?php

namespace RetoApiBundle\Entity;


use RetoApiBundle\Entity\Interfaces\ProjectionRankingRegionalInterface;

class ProjectionRankingRegional implements ProjectionRankingRegionalInterface
{
    const MEDAL_PLATINUM = 0;

    const MEDAL_GOLDEN = 1;

    const MEDAL_SILVER = 2;

    const MEDAL_BRONZE = 3;

    const LAST_DOWN = 0;

    const LAST_EQUAL = 1;

    const LAST_UP = 2;

    protected $id;

    protected $historicRegionalAccumulatedId;

    protected $regionalId;

    protected $regionalName;

    protected $redId;

    protected $redName;

    protected $rankingOfficesTransform;

    protected $territorialId;

    protected $territorialName;

    protected $ranking;

    protected $last;

    protected $medals;

    protected $officeTransform;

    protected $cmpAnnual;

    protected $cmpQuarterly1;

    protected $cmpQuarterly2;

    protected $cmpQuarterly3;

    protected $cmpQuarterly4;

    protected $createdAt;

    protected $updatedAt;

    private function generateArrayMedals($medalPlatinum, $medalGolden, $medalSilver, $medalBronze)
    {
        return array(
            self::MEDAL_PLATINUM => $medalPlatinum,
            self::MEDAL_GOLDEN => $medalGolden,
            self::MEDAL_SILVER => $medalSilver,
            self::MEDAL_BRONZE => $medalBronze
        );
    }

    public function create(
        $historicRegionalAccumulatedId,
        $regionalId,
        $regionalName,
        $redId,
        $redName,
        $territorialId,
        $territorialName,
        $ranking,
        $medalPlatinum,
        $medalGolden,
        $medalSilver,
        $medalBronze,
        $cmpAnnual,
        $cmpQuarterly1,
        $cmpQuarterly2,
        $cmpQuarterly3,
        $cmpQuarterly4,
        $officesTransform
    ) {
        $this->historicRegionalAccumulatedId = $historicRegionalAccumulatedId;
        $this->regionalId = $regionalId;
        $this->regionalName = $regionalName;
        $this->redId = $redId;
        $this->redName = $redName;
        $this->territorialId = $territorialId;
        $this->territorialName = $territorialName;
        $this->ranking = $ranking;
        $this->medals = $this->generateArrayMedals($medalPlatinum, $medalGolden, $medalSilver, $medalBronze);
        $this->cmpAnnual = $cmpAnnual;
        $this->cmpQuarterly1 = $cmpQuarterly1;
        $this->cmpQuarterly2 = $cmpQuarterly2;
        $this->cmpQuarterly3 = $cmpQuarterly3;
        $this->cmpQuarterly4 = $cmpQuarterly4;
        $this->officeTransform = $officesTransform;
        $this->createdAt = new \DateTime('NOW');
        $this->updatedAt = new \DateTime('NOW');
    }

    public function updateRankingOfficesTransform($ranking, $position)
    {
        $this->rankingOfficesTransform = $ranking;
        $this->last = $position;
    }

    public function update(
        $historicRegionalAccumulatedId,
        $regionalId,
        $regionalName,
        $redId,
        $redName,
        $territorialId,
        $territorialName,
        $ranking,
        $medalPlatinum,
        $medalGolden,
        $medalSilver,
        $medalBronze,
        $cmpAnnual,
        $cmpQuarterly1,
        $cmpQuarterly2,
        $cmpQuarterly3,
        $cmpQuarterly4,
        $officesTransform
    ) {
        $this->historicRegionalAccumulatedId = $historicRegionalAccumulatedId;
        $this->regionalId = $regionalId;
        $this->regionalName = $regionalName;
        $this->redId = $redId;
        $this->redName = $redName;
        $this->territorialId = $territorialId;
        $this->territorialName = $territorialName;
        $this->ranking = $ranking;
        $this->medals = $this->generateArrayMedals($medalPlatinum, $medalGolden, $medalSilver, $medalBronze);
        $this->cmpAnnual = $cmpAnnual;
        $this->cmpQuarterly1 = $cmpQuarterly1;
        $this->cmpQuarterly2 = $cmpQuarterly2;
        $this->cmpQuarterly3 = $cmpQuarterly3;
        $this->cmpQuarterly4 = $cmpQuarterly4;
        $this->officeTransform = $officesTransform;
        $this->updatedAt = new \DateTime('NOW');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getHistoricRegionalAccumulatedId()
    {
        return $this->historicRegionalAccumulatedId;
    }

    public function getRegionalId()
    {
        return $this->regionalId;
    }

    public function getRegionalName()
    {
        return $this->regionalName;
    }

    public function getRedId()
    {
        return $this->redId;
    }

    public function getRedName()
    {
        return $this->redName;
    }

    public function getTerritorialId()
    {
        return $this->territorialId;
    }

    public function getTerritorialName()
    {
        return $this->territorialName;
    }

    public function getRanking()
    {
        return $this->ranking;
    }

    public function getRankingOfficesTransform()
    {
        return $this->rankingOfficesTransform;
    }

    public function getMedals()
    {
        return $this->medals;
    }

    public function getOfficeTransform()
    {
        return $this->officeTransform;
    }

    public function getCmpAnnual()
    {
        return $this->cmpAnnual;
    }

    public function getCmpQuarterly1()
    {
        return $this->cmpQuarterly1;
    }

    public function getCmpQuarterly2()
    {
        return $this->cmpQuarterly2;
    }

    public function getCmpQuarterly3()
    {
        return $this->cmpQuarterly3;
    }

    public function getCmpQuarterly4()
    {
        return $this->cmpQuarterly4;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
