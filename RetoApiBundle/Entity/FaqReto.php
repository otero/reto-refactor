<?php
/**
 * Created by PhpStorm.
 * User: Marc
 * Date: 18/07/2017
 * Time: 13:13
 */

namespace RetoApiBundle\Entity;


use PtiBundle\Entity\Traits\DatetimeTrait;
use PtiBundle\Entity\Traits\IdentifyTrait;

class FaqReto
{
    use IdentifyTrait;
    use DatetimeTrait;

    const UPLOAD_DIR = 'uploads/img/faqReto';

    /**
     * @var string
     */
    protected $question;

    /**
     * @var string
     */
    protected $answer;

    /**
     * @var string
     */
    protected $mediaUrl;

    /**
     * @var string
     */
    protected $imageUrl;

    /**
     * @var integer
     */
    protected $position;

    /**
     * @var boolean
     */
    protected $isHidden;

    /**
     * @var boolean
     */
    protected $isHighlight;

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     * @return FaqReto
     */
    public function setQuestion($question)
    {
        $this->question = $question;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     * @return FaqReto
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
        return $this;
    }

    /**
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->mediaUrl;
    }

    /**
     * @param string $media_url
     * @return FaqReto
     */
    public function setMediaUrl($media_url)
    {
        $this->mediaUrl = $media_url;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @param string $image_url
     * @return FaqReto
     */
    public function setImageUrl($image_url)
    {
        $this->imageUrl = $image_url;
        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return FaqReto
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return $this->isHidden;
    }

    /**
     * @param bool $isHidden
     * @return FaqReto
     */
    public function setIsHidden($isHidden)
    {
        $this->isHidden = $isHidden;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHighlight()
    {
        return $this->isHighlight;
    }

    /**
     * @param bool $isHighlight
     * @return FaqReto
     */
    public function setIsHighlight($isHighlight)
    {
        $this->isHighlight = $isHighlight;
        return $this;
    }
}