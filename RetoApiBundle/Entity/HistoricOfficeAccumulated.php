<?php

namespace RetoApiBundle\Entity;

use PtiBundle\Entity\Traits\DatetimeTrait;
use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Traits\BancSabadellIdTrait;
use RetoApiBundle\Entity\Interfaces\HistoricOfficeAccumulatedInterface;

class HistoricOfficeAccumulated implements HistoricOfficeAccumulatedInterface
{
    use IdentifyTrait;
    use DatetimeTrait;
    use BancSabadellIdTrait;

    private $officereto;

    private $challenge;

    private $anual;

    private $trim1;

    private $trim2;

    private $trim3;

    private $trim4;

    private $rankingRed;

    public function create(
        $office,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    ) {
        $this->officereto = $office;
        $this->challenge = $challenge;
        $this->bancsabadellId = $bankSabadellId;
        $this->trim1 = $trim1;
        $this->trim2 = $trim2;
        $this->trim3 = $trim3;
        $this->trim4 = $trim4;
        $this->anual = $anual;
    }

    public function update(
        $office,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    ) {
        $this->create(
            $office,
            $challenge,
            $bankSabadellId,
            $trim1,
            $trim2,
            $trim3,
            $trim4,
            $anual
        );
    }

    public function setUnit($office)
    {
        $this->officereto = $office;
    }

    public function getOffice()
    {
        return $this->officereto;
    }


    public function setChallenge($challenge)
    {
        $this->challenge = $challenge;
    }

    public function setAnual($anual)
    {
        $this->anual = $anual;
    }

    public function setTrim1($trim1)
    {
        $this->trim1 = $trim1;
    }

    public function setTrim2($trim2)
    {
        $this->trim2 = $trim2;
    }

    public function setTrim3($trim3)
    {
        $this->trim3 = $trim3;
    }

    public function setTrim4($trim4)
    {
        $this->trim4 = $trim4;
    }

    /**
     * Get anual
     *
     * @return float
     */
    public function getAnual()
    {
        return $this->anual;
    }

    /**
     * Get trim1
     *
     * @return float
     */
    public function getTrim1()
    {
        return $this->trim1;
    }

    /**
     * Get trim2
     *
     * @return float
     */
    public function getTrim2()
    {
        return $this->trim2;
    }

    /**
     * Get trim3
     *
     * @return float
     */
    public function getTrim3()
    {
        return $this->trim3;
    }

    /**
     * Get trim4
     *
     * @return float
     */
    public function getTrim4()
    {
        return $this->trim4;
    }

    /**
     * Set rankingRed
     *
     * @param \RetoApiBundle\Entity\RankingRed $rankingRed
     *
     * @return HistoricOfficeAccumulated
     */
    public function setRankingRed(\RetoApiBundle\Entity\RankingRed $rankingRed = null)
    {
        $this->rankingRed = $rankingRed;

        return $this;
    }

    /**
     * Get rankingRed
     *
     * @return \RetoApiBundle\Entity\RankingRed
     */
    public function getRankingRed()
    {
        return $this->rankingRed;
    }

    /**
     * Set officereto
     *
     * @param \RetoApiBundle\Entity\OfficeReto $officereto
     *
     * @return HistoricOfficeAccumulated
     */
    public function setOfficereto(\RetoApiBundle\Entity\OfficeReto $officereto = null)
    {
        $this->officereto = $officereto;

        return $this;
    }

    /**
     * Get officereto
     *
     * @return \RetoApiBundle\Entity\OfficeReto
     */
    public function getOfficereto()
    {
        return $this->officereto;
    }

    /**
     * Get challenge
     *
     * @return \RetoApiBundle\Entity\Challenge
     */
    public function getChallenge()
    {
        return $this->challenge;
    }
}
