<?php

namespace RetoApiBundle\Entity;


use RetoApiBundle\Entity\Interfaces\TargetTipInterface;

class TargetTip implements TargetTipInterface
{
    const DIR = 'uploads/targetTip';

    protected $target;

    protected $title;

    protected $description;

    private $id;

    private $fileTip;

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setFileTip($fileTip)
    {
        $this->fileTip = $fileTip;

        return $this;
    }

    public function getFileTip()
    {
        return $this->fileTip;
    }

    public function setTarget(\RetoApiBundle\Entity\Target $target = null)
    {
        $this->target = $target;

        return $this;
    }

    public function getTarget()
    {
        return $this->target;
    }
}
