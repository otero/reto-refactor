<?php

namespace RetoApiBundle\Entity;

use PtiBundle\Entity\Traits\DatetimeTrait;
use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Traits\NameTrait;
use RetoApiBundle\Entity\Traits\OnficinaTrait;
use Doctrine\Common\Collections\ArrayCollection;
use RetoApiBundle\Entity\Interfaces\RedInterface;
use RetoApiBundle\Entity\Traits\BancSabadellIdTrait;
use RetoApiBundle\Entity\Interfaces\HeadquarterInterface;

class Red implements HeadquarterInterface, RedInterface
{
    use IdentifyTrait;
    use BancSabadellIdTrait;
    use NameTrait;
    use OnficinaTrait;
    use DatetimeTrait;

    protected $territorial;

    protected $regionals;

    protected $historicRed;

    protected $historicRedAccumulated;

    public function __construct()
    {
        $this->regionals = new ArrayCollection();
    }

    public function create($bancSabadellId, $name, $territorial)
    {
        $this->name = $name;
        $this->bancsabadellId = $bancSabadellId;
        $this->territorial = $territorial;
    }

    public function getTerritorial()
    {
        return $this->territorial;
    }

    public function addRegional(Regional $regional)
    {
        $this->regionals[] = $regional;
        return $this;
    }

    public function removeRegional(Regional $regional)
    {
        $this->regionals->removeElement($regional);
        return $this;
    }

    public function getRegionals()
    {
        return $this->regionals;
    }

    public function __toString()
    {
        return $this->getBancSabadellId() . ' :: ' . $this->getName();
    }
}
