<?php

namespace RetoApiBundle\Entity;


use PtiBundle\Entity\Traits\DatetimeTrait;
use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Interfaces\HistoricTerritorialAccumulatedInterface;

class HistoricTerritorialAccumulated implements HistoricTerritorialAccumulatedInterface
{
    use IdentifyTrait;
    use DatetimeTrait;

    protected $bancsabadellId;

    private $territorial;

    private $challenge;

    private $anual;

    private $trim1;

    private $trim2;

    private $trim3;

    private $trim4;

    public function create(
        $territorial,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    ) {
        $this->territorial = $territorial;
        $this->challenge = $challenge;
        $this->bancsabadellId = $bankSabadellId;
        $this->trim1 = $trim1;
        $this->trim2 = $trim2;
        $this->trim3 = $trim3;
        $this->trim4 = $trim4;
        $this->anual = $anual;
    }

    public function update(
        $territorial,
        $challenge,
        $bankSabadellId,
        $trim1,
        $trim2,
        $trim3,
        $trim4,
        $anual
    ) {
        $this->create(
            $territorial,
            $challenge,
            $bankSabadellId,
            $trim1,
            $trim2,
            $trim3,
            $trim4,
            $anual
        );
    }

    public function getBancsabadellId()
    {
        return $this->bancsabadellId;
    }

    public function getTerritorial()
    {
        return $this->territorial;
    }

    public function getChallenge()
    {
        return $this->challenge;
    }

    public function getAnual()
    {
        return $this->anual;
    }

    public function getTrim1()
    {
        return $this->trim1;
    }

    public function getTrim2()
    {
        return $this->trim2;
    }

    public function getTrim3()
    {
        return $this->trim3;
    }

    public function getTrim4()
    {
        return $this->trim4;
    }
}
