<?php

namespace RetoApiBundle\Entity;


use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Interfaces\HistoricInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Historic implements HistoricInterface
{
    use IdentifyTrait;

    const UNIT_TERRITORIAL = 'Territorial';

    const UNIT_RED = 'Red';

    const UNIT_REGIONAL = 'Regional';

    const UNIT_OFFICERETO = 'OfficeReto';

    const ACRONYM_TERRITORIAL = 'TE';

    const ACRONYM_RED = 'RE';

    const ACRONYM_REGIONAL= 'ZO';

    const ACRONYM_OFFICE = 'HUCC';

    const ACRONYM_NOM_TERRITORIAL = 'NOMTE';

    const ACRONYM_NOM_RED = 'NOMRE';

    const ACRONYM_NOM_REGIONAL= 'NOMZO';

    const ACRONYM_NOM_OFFICE = 'NOMHUCC';

    const UNITS = array(
        self::ACRONYM_TERRITORIAL => self::UNIT_TERRITORIAL,
        self::ACRONYM_RED => self::UNIT_RED,
        self::ACRONYM_REGIONAL => self::UNIT_REGIONAL,
        self::ACRONYM_OFFICE => self::UNIT_OFFICERETO
    );

    const OFFICE_PREFIX = array('DMO', 'HUB');

    const HISTORIC_UNITS = array(
        HistoricOffice::class,
        HistoricRegional::class,
        HistoricTerritorial::class,
        HistoricRed::class
    );

    const MONTHS = array(
        1 => 'Enero',
        2 => 'Febrero',
        4 => 'Abril',
        5 => 'Mayo',
        6 => 'Junio',
        7 => 'Julio',
        8 => 'Agosto',
        9 => 'Septiembre',
        10 => 'Octubre',
        11 => 'Noviembre',
        12 => 'Diciembre'
    );

    const PREFIX_BANK_ID = '81';

    const ACC_ANUAL = 'anual';

    const ACC_TRIM1  = 'trim1';

    const ACC_TRIM2  = 'trim2';

    const ACC_TRIM3  = 'trim3';

    const ACC_TRIM4  = 'trim4';

    private $challenge;

    private $month;

    private $filename;

    private $trimester;

    private $createdAt;

    private $processed;

    private $historicLog;

    protected $historicOffice;

    protected $historicRegional;

    protected $historicRed;

    protected $historicTerritorial;

    protected $historicRedAccumulated;

    protected $historicOfficeAccumulated;

    protected $historicRegionalAccumulated;

    protected $historicTerritorialAccumulated;

    public function changeStatusToProcessed()
    {
        $this->processed = true;

        return $this;
    }

    public function getChallenge()
    {
        return $this->challenge;
    }

    public function setChallenge($challenge)
    {
        $this->challenge = $challenge;
    }

    public function getMonth()
    {
        return $this->month;
    }

    public function setMonth($month)
    {
        $this->month = $month;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    public function getAccumulatedester()
    {
        return $this->trimester;
    }

    public function setTrimester($trimester)
    {
        $this->trimester = $trimester;
    }

    private function getTrimesterFromMonth($monthNumber)
    {
        return floor(($monthNumber - 1) / 3) + 1;
    }

    public function getProcessed()
    {
        return $this->processed;
    }

    public function setProcessed($processed)
    {
        $this->processed = $processed;
    }

    public function updateTimestamps()
    {
        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function completeFromUpload(UploadedFile $file)
    {
        $this->setFilename($file->getClientOriginalName());

        $trimester = $this->getTrimesterFromMonth($this->getMonth());
        $this->setTrimester($trimester);
        $this->setProcessed(false);
    }
}
