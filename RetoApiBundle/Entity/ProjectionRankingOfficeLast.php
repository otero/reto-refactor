<?php

namespace RetoApiBundle\Entity;

class ProjectionRankingOfficeLast
{
    protected $id;

    protected $officeId;

    protected $rankingLast;

    protected $cmpAnnualLast;

    protected $createdAt;

    protected $updatedAt;

    public function create(
        $officeId,
        $rankingLast,
        $cmpAnnualLast
    ) {
        $this->officeId = $officeId;
        $this->rankingLast = $rankingLast;
        $this->cmpAnnualLast = $cmpAnnualLast;
        $this->createdAt = new \DateTime('NOW');
        $this->updatedAt = new \DateTime('NOW');
    }

    public function update(
        $officeId,
        $rankingLast,
        $cmpAnnualLast
    ) {
        $this->officeId = $officeId;
        $this->rankingLast = $rankingLast;
        $this->cmpAnnualLast = $cmpAnnualLast;
        $this->updatedAt = new \DateTime('NOW');
    }

    public function getId()
    {
        return $this->getId();
    }

    public function getOfficeId()
    {
        return $this->officeId;
    }

    public function getRankingLast()
    {
        return $this->rankingLast;
    }

    public function getCmpAnnualLast()
    {
        return $this->cmpAnnualLast;
    }
}
