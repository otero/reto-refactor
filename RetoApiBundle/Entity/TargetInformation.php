<?php

namespace RetoApiBundle\Entity;


class TargetInformation
{
    protected $target;

    protected $title;
    
    protected $subtitle;

    protected $description;

    protected $percentaje;

    protected $sub_percentaje;

    protected $features;


    private $id;

    public function __toString()
    {
       return $this->getTitle();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }


    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getSubtitle()
    {
        return $this->subtitle;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setPercentaje($percentaje)
    {
        $this->percentaje = $percentaje;

        return $this;
    }

    public function getPercentaje()
    {
        return $this->percentaje;
    }

    public function setSubPercentaje($subPercentaje)
    {
        $this->sub_percentaje = $subPercentaje;

        return $this;
    }

    public function getSubPercentaje()
    {
        return $this->sub_percentaje;
    }

    public function setFeatures($features)
    {
        $this->features = $features;

        return $this;
    }

    public function getFeatures()
    {
        return $this->features;
    }



    public function setTarget(\RetoApiBundle\Entity\Target $target = null)
    {
        $this->target = $target;

        return $this;
    }

    public function getTarget()
    {
        return $this->target;
    }
}

