<?php

namespace RetoApiBundle\Entity;

use PtiBundle\Entity\Traits\DatetimeTrait;
use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Traits\NameTrait;
use RetoApiBundle\Entity\Traits\BancSabadellIdTrait;
use RetoApiBundle\Entity\Interfaces\OfficeRetoInterface;
use RetoApiBundle\Entity\Interfaces\HeadquarterInterface;

class OfficeReto implements HeadquarterInterface, OfficeRetoInterface
{
    use IdentifyTrait;
    use BancSabadellIdTrait;
    use NameTrait;
    use DatetimeTrait;

    protected $regional;

    protected $historicOffice;

    protected $historicOfficeAccumulated;

    public function create($bancSabadellId, $name, $regional)
    {
        $this->name = $name;
        $this->bancsabadellId = $bancSabadellId;
        $this->regional = $regional;
    }

    public function getRegional()
    {
        return $this->regional;
    }

    public function __toString()
    {
        return $this->getBancSabadellId() . ' :: ' . $this->getName();
    }
}
