<?php

namespace RetoApiBundle\Entity;

use PtiBundle\Entity\Traits\IdentifyTrait;
use RetoApiBundle\Entity\Interfaces\HistoricRedInterface;


class HistoricRed implements HistoricRedInterface
{
    use IdentifyTrait;

    private $accomplishment;

    private $position;

    protected $targets;

    protected $createdAt;

    private $red;

    private $historic;

    private $target;

    public function create($red, $historic, $target, $accomplishment, $position)
    {
        $this->red = $red;
        $this->historic = $historic;
        $this->target = $target;
        $this->accomplishment = $accomplishment;
        $this->position = $position;
    }

    public function setUnit($red)
    {
        $this->red = $red;
    }

    public function setHistoric($historic)
    {
        $this->historic = $historic;
    }

    public function setTarget(Target $target)
    {
        $this->target = $target;
        return $this;
    }

    public function setAccomplishment($accomplishment)
    {
        $this->accomplishment = $accomplishment;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function updateTimestamps()
    {
        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getRed()
    {
        return $this->red;
    }
}
