<?php
/**
 * Created by PhpStorm.
 * User: Marc Ginovart <marc.ginovart@berepublic.es>
 * Date: 17/07/2017
 * Time: 15:15
 */

namespace RetoApiBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BankType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                [
                    'label'     =>  'Nombre',
                    'required'  =>  true,
                ]
            )
            ->add(
                'bancSabadellId',
                'integer',
                [
                    'label'     =>  'BancSabadell Id',
                    'required'  =>  true,
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' =>  'RetoApiBundle\Entity\Bank',
        ]);
    }

    public function getName()
    {
        return 'reto_bank';
    }
}