<?php

namespace RetoApiBundle\Form\Type;


use RetoApiBundle\Entity\Challenge;
use RetoApiBundle\Entity\Historic;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;


class HistoricType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $months = Historic::MONTHS;

        $builder->add('challenge', 'entity', [
                'class' => Challenge::class,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.year', 'DESC');
                },
            ])
            ->add('month', 'choice', [
                'choices' => $months,
                'required' => true
            ])
            ->add('filename', 'file', [
                'label' => 'Fichero estadísticas',
                'data_class' => null,
                'required' => true
            ]);
    }



    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RetoApiBundle\Entity\Historic'
        ]);
    }

    public function getName()
    {
        return 'stats';
    }
}