<?php

namespace RetoApiBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChallengeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', 'text', [
                    'label'     =>  'Nombre',
                    'required'  =>  true])
            ->add('active', 'checkbox', [
                    'label'     =>  'Activo',
                    'required'  =>  true])
            ->add('year', 'integer', [
                    'label'     =>  'Año',
                    'required'  =>  true
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RetoApiBundle\Entity\Challenge'
        ]);
    }

    public function getName()
    {
        return 'challenge';
    }
}