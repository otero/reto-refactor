<?php
/**
 * Created by PhpStorm.
 * User: Marc
 * Date: 20/07/2017
 * Time: 9:25
 */

namespace RetoApiBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegionalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                [
                    'label'     =>  'Nombre',
                    'required'  =>  true,
                ]
            )
            ->add(
                'bancSabadellId',
                'integer',
                [
                    'label'     =>  'BancSabadell Id',
                    'required'  =>  true
                ]
            )
            ->add(
                'red',
                'entity',
                [
                    'label'         =>  'Red Comercial',
                    'required'      =>  true,
                    'class'         =>  'RetoApiBundle\Entity\Red',
                    'choice_label'  =>  function($red){
                        return (string)$red;
                    },
                    'multiple'      =>  false,
                    'expanded'      =>  false,
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    =>  'RetoApiBundle\Entity\Regional'
        ]);
    }

    public function getName()
    {
        return 'reto_regional';
    }
}