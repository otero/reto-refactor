<?php

namespace RetoApiBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class TargetInformationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                'text',
                array(
                    'label'     =>  'Titulo extendido',
                    'required'  =>  true,
                    'constraints' => array(new Length(array('max' => 100), new NotBlank()))
                )
            )
            ->add(
                'subtitle',
                'text',
                array(
                    'label'     =>  'Subtítulo',
                    'required'  =>  false,
                    'constraints' => array(new Length(array('max' => 100)))
                )
            )
            ->add(
                'description',
                'textarea',
                array(
                    'label'     =>  'Descripcion',
                    'required'  =>  true,
                     'constraints' => array(new Length(array('max' => 10000), new NotBlank()))
                    )
            )
            ->add(
                'percentaje',
                'integer',
            array(
                'label'     =>  'Porcentaje (Provisional)',
                'required'  =>  false,

                )
            )
            ->add(
                'sub_percentaje',
                'textarea',
                array(
                    'label'     =>  ' Subtitulo Porcentaje (Provisional)',
                    'required'  =>  false,

                )
            )
            ->add(
                'features',
                'textarea',
                array(
                    'label'     =>  'Caracteristicas (Provisional)',
                    'required'  =>  false,

                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    =>  'RetoApiBundle\Entity\TargetInformation'
        ));
    }

    public function getName()
    {
        return 'target_information';
    }
}
