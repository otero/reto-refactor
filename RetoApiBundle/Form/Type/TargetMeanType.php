<?php

namespace RetoApiBundle\Form\Type;


use RetoApiBundle\Entity\TargetInformation;
use RetoApiBundle\Entity\TargetMean;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class TargetMeanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'typeMean',
                'choice',
                array(
                    'choices' => TargetMean::getTypes(),
                    'label'     =>  'Tipo de recurso',
                    'required'  =>  true
                )
            )
            ->add(
                'title',
                'text',
                array(
                    'label'     =>  'Titulo',
                    'required'  =>  true,
                    'constraints' => array(new Length(array('max' => 100), new NotBlank()))
                )
            )
            ->add(
                'description',
                'textarea',
                array(
                    'label'     =>  'Descripcion',
                    'required'  =>  true,
                     'constraints' => array(new NotBlank())
                    )
            )
            ->add(
                'fileMean',
                'file',
                array(
                    'data_class' => null,
                    'label'     =>  'Imagen',
                    'required'  =>  false,
                    'constraints' => array(new Image())
                )
            )
            ->add(
                'link',
                'text',
                array(
                    'label'     =>  'Link',
                    'required'  =>  true,
                    'constraints' => array(new Length(array('max' => 250), new NotBlank()))
                )
            )

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    =>  'RetoApiBundle\Entity\TargetMean'
        ));
    }

    public function getName()
    {
        return 'target_mean';
    }
}
