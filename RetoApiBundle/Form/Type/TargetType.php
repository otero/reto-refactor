<?php

namespace RetoApiBundle\Form\Type;


use RetoApiBundle\Entity\TargetInformation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class TargetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                array(
                    'label'     =>  'Titulo',
                    'required'  =>  true,
                    'constraints' => array(new Length(array('max' => 100), new NotBlank()))
                )
            )
            ->add(
                'bsIndicador',
                'integer',
                array(
                    'label'     =>  'Numero Indicador',
                    'required'  =>  true,
                     'constraints' => array(new Type('integer'), new NotBlank())
                    )
            )
            ->add('targetInformation', new TargetInformationType())
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    =>  'RetoApiBundle\Entity\Target'
        ));
    }

    public function getName()
    {
        return 'target';
    }
}
