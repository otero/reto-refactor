<?php
/**
 * Created by PhpStorm.
 * User: Marc
 * Date: 19/07/2017
 * Time: 12:38
 */

namespace RetoApiBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                [
                    'label'     =>  'Nombre',
                    'required'  =>  true,
                ]
            )
            ->add(
                'bancSabadellId',
                'integer',
                [
                    'label'     =>  'BancSabadell Id',
                    'required'  =>  true,
                ]
            )
            ->add(
                'territorial',
                'entity',
                [
                    'label'         =>  'Territorial',
                    'required'      =>  true,
                    'class'         =>  'RetoApiBundle\Entity\Territorial',
                    'choice_label'  =>  function($territorial){
                        return (string)$territorial;
                    },
                    'multiple'      =>  false,
                    'expanded'      =>  false,
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    =>  'RetoApiBundle\Entity\Red',
        ]);
    }

    public function getName()
    {
        return 'reto_red';
    }
}