<?php

namespace RetoApiBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TerritorialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                [
                    'label'     =>  'Nombre',
                    'required'  =>  true,
                ]
            )
            ->add(
                'bancSabadellId',
                'integer',
                [
                    'label'     =>  'BancSabadell Id',
                    'required'  =>  true,
                ]
            )
            ->add(
                'bank',
                'entity',
                [
                    'label'         =>  'Banco',
                    'required'      =>  true,
                    'class'         =>  'RetoApiBundle\Entity\Bank',
                    'choice_label'  =>  function($bank){
                        return (string)$bank;
                    },
                    'multiple'      =>  false,
                    'expanded'      =>  false,
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    =>  'RetoApiBundle\Entity\Territorial',
        ]);
    }

    public function getName()
    {
        return 'reto_territorial';
    }
}