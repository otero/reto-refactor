<?php
/**
 * Created by PhpStorm.
 * User: Marc
 * Date: 20/07/2017
 * Time: 9:25
 */

namespace RetoApiBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfficeRetoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                [
                    'label'     =>  'Nombre',
                    'required'  =>  true,
                ]
            )
            ->add(
                'bancSabadellId',
                'integer',
                [
                    'label'     =>  'BancSabadell Id',
                    'required'  =>  true
                ]
            )
            ->add(
                'regional',
                'entity',
                [
                    'label'         =>  'Regional',
                    'required'      =>  true,
                    'class'         =>  'RetoApiBundle\Entity\Regional',
                    'choice_label'  =>  function($regional){
                        return (string)$regional;
                    },
                    'multiple'      =>  false,
                    'expanded'      =>  false,
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    =>  'RetoApiBundle\Entity\OfficeReto'
        ]);
    }

    public function getName()
    {
        return 'reto_office';
    }
}