<?php
/**
 * Created by PhpStorm.
 * User: Marc
 * Date: 18/07/2017
 * Time: 18:43
 */

namespace RetoApiBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FaqRetoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'question',
                'textarea',
                [
                    'label'     => 'Pregunta',
                    'required'  =>  true,
                ]
            )
            ->add(
                'answer',
                'textarea',
                [
                    'label'     =>  'Respuesta',
                    'required'  =>  true,
                ]
            )
            ->add(
                'position',
                'integer',
                [
                    'label'     =>  'Posición',
                    'required'  =>  true,
                ]
            )
            ->add(
                'isHidden',
                'checkbox',
                [
                    'label'     =>  'FAQ Ocultada',
                    'required'  =>  false,
                ]
            )
            ->add(
                'isHighlight',
                'checkbox',
                [
                    'label'     =>  'FAQ Destacada',
                    'required'  =>  false,
                ]
            )
            ->add(
                'imageUrl',
                'file',
                [
                    'label'     => 'Imagen FAQ',
                    'required'  =>  false,
                    'mapped'    =>  false,
                ]
            )
            ->add(
                'mediaUrl',
                'file',
                [
                    'label'     => 'Documento FAQ',
                    'required'  =>  false,
                    'mapped'    =>  false,
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RetoApiBundle\Entity\FaqReto',
        ]);
    }

    public function getName()
    {
        return 'reto_faq';
    }
}