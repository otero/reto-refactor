<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Domain\Service\RankingOfficeByRedService;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Repository\Interfaces\RedRepositoryInterface;

class ProcessRankingRedGenerateUseCase
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    protected $redRepository;

    protected $rankingOfficeByRedService;

    public function __construct(
        RedRepositoryInterface $redRepository,
        RankingOfficeByRedService $rankingOfficeByRedService,
        LoggerInterface $logger
    ) {
        $this->redRepository = $redRepository;
        $this->rankingOfficeByRedService = $rankingOfficeByRedService;
        $this->logger = $logger;
    }

    public function execute()
    {
        $message = 'START - ProcessRankingRedGenerateUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));

        $reds = $this->redRepository->findAllReds();

        $this->rankingOfficeByRedService->generate($reds);

        $message = 'END - ProcessRankingRedGenerateUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));
    }
}

