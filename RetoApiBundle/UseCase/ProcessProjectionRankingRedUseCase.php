<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Domain\Service\ProjectionRankingRedService;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Repository\Interfaces\HistoricRedAccumulatedRepositoryInterface;
use RetoApiBundle\Domain\Service\ProjectionRankingOfficeTransformRedService;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingRedRepositoryInterface;

class ProcessProjectionRankingRedUseCase
{
    protected $logger;

    protected $historicRedAccumulatedRepository;

    protected $projectionRankingRedService;

    protected $projectionRankingOfficeTransformRedService;

    protected $projectionRankingRedRepository;

    public function __construct(
        HistoricRedAccumulatedRepositoryInterface $historicRedAccumulatedRepository,
        ProjectionRankingRedService $projectionRankingRedService,
        ProjectionRankingOfficeTransformRedService $projectionRankingOfficeTransformRedService,
        ProjectionRankingRedRepositoryInterface $projectionRankingRedRepository,
        LoggerInterface $logger
    ) {
        $this->historicRedAccumulatedRepository = $historicRedAccumulatedRepository;
        $this->projectionRankingRedService = $projectionRankingRedService;
        $this->projectionRankingOfficeTransformRedService = $projectionRankingOfficeTransformRedService;
        $this->projectionRankingRedRepository = $projectionRankingRedRepository;
        $this->logger = $logger;
    }

    public function execute()
    {
        $message = 'START - ProcessProjectionRankingRedUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));

        $this->projectionRankingRedRepository->deleteAll();

        $redRanking = $this->historicRedAccumulatedRepository->findRanking();

        $this->projectionRankingRedService->generate($redRanking);

        $officeTransformRedRanking = $this->projectionRankingRedRepository->findRankingByOfficeTransform();

        $this->projectionRankingOfficeTransformRedService
            ->generate($officeTransformRedRanking);

        $this->logger->info('The  RankingRedProjection was created Successfully!', array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_PROJECTION_RANKING_RED,
            'data' => array(

            )
        ));

        $message = 'END - ProcessProjectionRankingRedUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));
    }
}
