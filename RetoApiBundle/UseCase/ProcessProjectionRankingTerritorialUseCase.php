<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Domain\Service\ProjectionRankingTerritorialService;
use RetoApiBundle\Domain\Service\ProjectionRankingOfficeTransformTerritorialService;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Repository\Interfaces\HistoricTerritorialAccumulatedRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingTerritorialRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionHistoricRankingTerritorialRepositoryInterface;

class ProcessProjectionRankingTerritorialUseCase
{
    protected $logger;

    protected $historicTerritorialAccumulatedRepository;

    protected $projectionRankingTerritorialService;

    protected $projectionRankingOfficeTransformTerritorialService;

    protected $projectionRankingTerritorialRepository;
    protected $projectionHistoricRankingTerritorialRepository;

    public function __construct(
        HistoricTerritorialAccumulatedRepositoryInterface $historicTerritorialAccumulatedRepository,
        ProjectionRankingTerritorialService $projectionRankingTerritorialService,
        ProjectionRankingOfficeTransformTerritorialService $projectionRankingOfficeTransformTerritorialService,
        ProjectionRankingTerritorialRepositoryInterface $projectionRankingTerritorialRepository,
        ProjectionHistoricRankingTerritorialRepositoryInterface $projectionHistoricRankingTerritorialRepository,
        LoggerInterface $logger
    ) {
        $this->historicTerritorialAccumulatedRepository = $historicTerritorialAccumulatedRepository;
        $this->projectionRankingTerritorialService = $projectionRankingTerritorialService;
        $this->projectionRankingOfficeTransformTerritorialService = $projectionRankingOfficeTransformTerritorialService;
        $this->projectionRankingTerritorialRepository = $projectionRankingTerritorialRepository;
        $this->projectionHistoricRankingTerritorialRepository = $projectionHistoricRankingTerritorialRepository;
        $this->logger = $logger;
    }

    public function execute()
    {
        $message = 'START - ProcessProjectionRankingTerritorialUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));

        /*
        $this->projectionHistoricRankingTerritorialRepository->deleteAll();
        $dataBackUp = $this->projectionRankingTerritorialRepository->findByFilters();
        */
        $this->projectionRankingTerritorialRepository->deleteAll();

        $territorialRanking = $this->historicTerritorialAccumulatedRepository->findRanking();

        $this->projectionRankingTerritorialService->generate($territorialRanking);

        $officeTransformTerritorialRanking = $this->projectionRankingTerritorialRepository->findRankingByOfficeTransform();

        $this->projectionRankingOfficeTransformTerritorialService
            ->generate($officeTransformTerritorialRanking);

        $this->logger->info('The  RankingTerritorialProjection was created Successfully!', array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_PROJECTION_RANKING_TERRITORIAL,
            'data' => array(

            )
        ));

        $message = 'END - ProcessProjectionRankingTerritorialUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));
    }
}
