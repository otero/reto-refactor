<?php

namespace RetoApiBundle\UseCase;


use RetoApiBundle\Domain\Exception\OfficeReto\OfficeRetoNotFoundException;
use RetoApiBundle\Domain\Repository\UserAuthRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingRedRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\HistoricRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\OfficeRetoRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingOfficeRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\RankingRedRepositoryInterface;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

class ChallengeUserDataUseCase
{
    /**
     * @var RankingRedRepositoryInterface
     */
    protected $rankingRedRepository;

    /**
     * @var UserAuthRepositoryInterface
     */
    protected $userAuthRepository;

    /**
     * @var OfficeRetoRepositoryInterface
     */
    protected $officeRetoRepository;

    /**
     * @var HistoricRepositoryInterface
     */
    protected $historicRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var ProjectionRankingOfficeRepositoryInterface
     */
    protected $projectionRankingOfficeRepository;

    public function __construct(
        UserAuthRepositoryInterface $userAuthRepository,
        RankingRedRepositoryInterface $rankingRedRepository,
        ProjectionRankingRedRepositoryInterface $projectionRankingRedRepository,
        OfficeRetoRepositoryInterface $officeRetoRepository,
        HistoricRepositoryInterface $historicRepository,
        ProjectionRankingOfficeRepositoryInterface $projectionRankingOfficeRepository,
        LoggerInterface $logger
    ) {
        $this->userAuthRepository = $userAuthRepository;
        $this->rankingRedRepository = $rankingRedRepository;
        $this->historicRepository = $historicRepository;
        $this->projectionRankingRedRepository = $projectionRankingRedRepository;
        $this->logger = $logger;
        $this->officeRetoRepository = $officeRetoRepository;
        $this->projectionRankingOfficeRepository = $projectionRankingOfficeRepository;
    }

    public function execute()
    {
        try {
            $user = $this->userAuthRepository->findUserOrFail();

            $challengeUserData = $this->officeRetoRepository
                ->findOneByBankSabadellIdOrFail($user->getOffice()->getCpi());

            $ranking = $this->rankingRedRepository->findOneRankingByOffice(
                $challengeUserData->getId()
            );

            $rankingCommercial = $this->projectionRankingRedRepository->findOneByRed(
                $challengeUserData->getRegional()->getRed()->getId()
            );

            $officeReto = $this->officeRetoRepository
                ->findOneByBankSabadellIdOrFail($user->getOffice()->getCpi());

            $officeUser = $this->projectionRankingOfficeRepository
                ->findOneByOfficeReto($officeReto->getId());

            $userArray = array(
                'hasOffice' => true,
                'ranking' => $ranking,
                'office_id' => $challengeUserData->getId(),
                'office_name' => $challengeUserData->getName(),
                'office_medal' => $officeUser['medal'],
                'office_ranking' => $officeUser,
                'regional_id' => $challengeUserData->getRegional()->getId(),
                'regional_name' => $challengeUserData->getRegional()->getName(),
                'red_id' => $challengeUserData->getRegional()->getRed()->getId(),
                'red_name' => $challengeUserData->getRegional()->getRed()->getName(),
                'red_data' => $rankingCommercial,
                'territorial_id' => $challengeUserData->getRegional()->getRed()->getTerritorial()->getId(),
                'territorial_name' => $challengeUserData->getRegional()->getRed()->getTerritorial()->getName()
            );

        } catch (OfficeRetoNotFoundException $e) {
            $userArray = array('hasOffice' => false);
        }

        $historic = $this->historicRepository->findCurrent();

        return array(
            'user' => $userArray,
            'reto' => array(
                'last_update' => $historic->getCreatedAt()->format('Y-m-d H:i')
            )
        );
    }
}