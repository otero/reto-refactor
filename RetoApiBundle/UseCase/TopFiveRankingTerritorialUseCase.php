<?php

namespace RetoApiBundle\UseCase;


use Symfony\Component\HttpKernel\Log\LoggerInterface;
use RetoApiBundle\Domain\Repository\UserAuthRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingTerritorialRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionHistoricRankingTerritorialRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingOfficeRepositoryInterface;

class TopFiveRankingTerritorialUseCase
{
    /**
     * @var UserAuthRepositoryInterface
     */
    protected $userAuthRepository;

    /**
     * @var ProjectionRankingTerritorialRepositoryInterface
     */
    protected $projectionRankingTerritorialRepository;

    /**
     * @var ProjectionHistoricRankingTerritorialRepositoryInterface
     */
    protected $projectionHistoricRankingTerritorialRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ProjectionRankingOfficeRepositoryInterface
     */
    protected $projectionRankingOfficeRepository;

    public function __construct(
        UserAuthRepositoryInterface $userAuthRepository,
        ProjectionRankingTerritorialRepositoryInterface $projectionRankingTerritorialRepository,
        ProjectionHistoricRankingTerritorialRepositoryInterface $projectionHistoricRankingTerritorialRepository,
        ProjectionRankingOfficeRepositoryInterface $projectionRankingOfficeRepository,
        LoggerInterface $logger
    ) {
        $this->userAuthRepository = $userAuthRepository;
        $this->projectionRankingTerritorialRepository = $projectionRankingTerritorialRepository;
        $this->projectionHistoricRankingTerritorialRepository = $projectionHistoricRankingTerritorialRepository;
        $this->projectionRankingOfficeRepository = $projectionRankingOfficeRepository;
        $this->logger = $logger;
    }

    public function execute()
    {
        $this->userAuthRepository->findUserOrFail();

        $ranking = $this->projectionRankingTerritorialRepository->findByFilters();

        foreach ($ranking as $id=>$territorial)
        {
            $comparative = $this->projectionHistoricRankingTerritorialRepository->getComparative($territorial['territorialId'],$territorial['ranking'],$territorial['officesTransform'],$territorial['cmpAnnual']);
            foreach($comparative as $name=>$value)
            {
                $ranking[$id][$name] = $value;
            }

        }

        $platinumMedalOffice = $this->projectionRankingOfficeRepository->findByMedalType();
        $ranking['diamondMedal'] = $platinumMedalOffice[0]['officeName'];
        return $ranking;
    }
}