<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingOfficeRepositoryInterface;

/**
 * Class ChallengeRankingTopOfficeUseCase
 * @package RetoApiBundle\UseCase
 * @todo AG: write a unit test for this
 */
class ChallengeRankingTopOfficeUseCase
{
    protected $projectionRankingOfficeRepository;

    protected $logger;

    public function __construct(
        ProjectionRankingOfficeRepositoryInterface $projectionRankingOfficeRepository,
        LoggerInterface $logger
    ) {
        $this->projectionRankingOfficeRepository = $projectionRankingOfficeRepository;
        $this->logger = $logger;
    }

    public function execute()
    {
        return $this->projectionRankingOfficeRepository->findOneByCmpAnnual('DESC');
    }
}
