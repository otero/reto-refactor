<?php

namespace RetoApiBundle\UseCase;


use RetoApiBundle\Domain\Repository\UserAuthRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\OfficeRetoRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\RankingRedRepositoryInterface;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

class RankingMyOfficeUseCase
{
    /**
     * @var UserAuthRepositoryInterface
     */
    protected $userAuthRepository;

    /**
     * @var RankingRedRepositoryInterface
     */
    protected $rankingRedRepository;

    /**
     * @var OfficeRetoRepositoryInterface
     */
    protected $officeRetoRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(
        UserAuthRepositoryInterface $userAuthRepository,
        RankingRedRepositoryInterface $rankingRedRepository,
        OfficeRetoRepositoryInterface $officeRetoRepository,
        LoggerInterface $logger
    ) {
        $this->userAuthRepository = $userAuthRepository;
        $this->rankingRedRepository = $rankingRedRepository;
        $this->logger = $logger;
        $this->officeRetoRepository = $officeRetoRepository;
    }

    public function execute()
    {
        $user = $this->userAuthRepository->findUserOrFail();

        $officeReto = $this->officeRetoRepository
            ->findOneByBankSabadellIdOrFail($user->getOffice()->getCpi());

        $ranking = $this->rankingRedRepository->findOneRankingByOffice(
            $officeReto->getId()
        );

        return $ranking;
    }
}