<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Domain\Exception\Challenge\ChallengeNotFoundException;
use RetoApiBundle\Domain\Exception\Historic\HistoricNotFoundException;
use RetoApiBundle\Domain\Service\Interfaces\UnitsServiceInterface;
use RetoApiBundle\Domain\Service\UnitsService;
use RetoApiBundle\Domain\Repository\csvRepositoryInterface;
use RetoApiBundle\Domain\Repository\FileRepositoryInterface;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Repository\Interfaces\HistoricRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ChallengeRepositoryInterface;

class ProcessStatsRetoUseCase
{
    /**
     * @var HistoricRepositoryInterface
     */
    protected $historicRepository;

    /**
     * @var ChallengeRepositoryInterface
     */
    protected  $challengeRepository;

    /**
     * @var unitsService
     */
    protected $unitsService;

    /**
     * @var FileRepositoryInterface
     */
    protected $fileRepository;

    /**
     * @var csvRepositoryInterface
     */
    protected $csvRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * ProcessStatsRetoUseCase constructor.
     * @param HistoricRepositoryInterface $historicRepository
     * @param ChallengeRepositoryInterface $challengeRepository
     * @param UnitsServiceInterface $unitsService
     * @param FileRepositoryInterface $fileRepository
     * @param csvRepositoryInterface $csvRepository
     * @param LoggerInterface $logger
     */
    public function __construct
    (
        HistoricRepositoryInterface $historicRepository,
        ChallengeRepositoryInterface $challengeRepository,
        UnitsServiceInterface $unitsService,
        FileRepositoryInterface $fileRepository,
        csvRepositoryInterface $csvRepository,
        LoggerInterface $logger
    ) {
        $this->historicRepository = $historicRepository;
        $this->challengeRepository = $challengeRepository;
        $this->fileRepository = $fileRepository;
        $this->unitsService = $unitsService;
        $this->csvRepository = $csvRepository;
        $this->logger = $logger;
    }

    public function execute()
    {
        $message = 'START - ProcessStatsRetoUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));

        try {
            $historic = $this->historicRepository->findOneByProcessedOrFail();
            $file = $this->fileRepository->get($historic->getFilename());
            $csv = $this->csvRepository->get($file);
            $challenge = $this->challengeRepository->findOneByActiveOrFail();
            $this->logger->info('CSV  proccess Successfully', array(
                'bundle' => HistoricLog::BUNDLE_RETO,
                'channel' => HistoricLog::CHANNEL_CSV,
                'data' => array(
                    'historic_id' => $historic->getId(),
                    'challenge_id' => $challenge->getId()
                )
            ));
            $challenge = $this->challengeRepository->findOneByActiveOrFail();
            $this->unitsService->process($historic, $challenge, $csv);
            $challenge = $this->challengeRepository->findOneByActiveOrFail();
            $this->historicRepository->createOrUpdate($historic->changeStatusToProcessed());

            $this->logger->info('Historics Tables update', array(
                'bundle' => HistoricLog::BUNDLE_RETO,
                'channel' => HistoricLog::CHANNEL_CSV,
                'data' => array(
                    'historic_id' => $historic->getId(),
                    'challenge_id' => $challenge->getId()
                )
            ));
        } catch (\Exception $e) {

            $this->logger->info('Something was wrong!', array(
                'bundle' => HistoricLog::BUNDLE_RETO,
                'channel' => HistoricLog::CHANNEL_CSV,
                'data' => array(
                    'message' => $e->getMessage(),
                    'code' => $e->getCode()
                )
            ));
        }

        $message = 'END - ProcessStatsRetoUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));
    }
}
