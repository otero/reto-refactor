<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Application\DTO\GetRed;
use RetoApiBundle\Repository\Interfaces\RedRepositoryInterface;

class GetRedUseCase
{
    protected $redRepository;

    protected $logger;

    public function __construct(
        RedRepositoryInterface $redRepository,
        LoggerInterface $logger
    ) {
        $this->redRepository = $redRepository;
        $this->logger = $logger;
    }

    public function execute(GetRed $request)
    {
        return $this->redRepository->getReds($request->getTerritorial());
    }
}
