<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Domain\Service\ProjectionRankingOfficeService;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingOfficeRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\RedRepositoryInterface;

class ProcessProjectionRankingOfficeUseCase
{
    protected $logger;

    protected $redRepository;

    protected $projectionRankingOfficeService;

    protected $projectionRankingOfficeRepository;

    public function __construct(
        RedRepositoryInterface $redRepository,
        ProjectionRankingOfficeService $projectionRankingOfficeService,
        ProjectionRankingOfficeRepositoryInterface $projectionRankingOfficeRepository,
        LoggerInterface $logger
    ) {
        $this->redRepository = $redRepository;
        $this->projectionRankingOfficeService = $projectionRankingOfficeService;
        $this->projectionRankingOfficeRepository = $projectionRankingOfficeRepository;
        $this->logger = $logger;
    }

    public function execute()
    {
        $message = 'START - ProcessProjectionRankingOfficeUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));

        $this->projectionRankingOfficeRepository->deleteAll();

        $reds = $this->redRepository->findAllReds();

        $this->projectionRankingOfficeService->generate($reds);

        $this->logger->info('The  RankingOfficeProjection was created Successfully!', array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_PROJECTION_RANKING_OFFICE,
            'data' => array(

            )
        ));

        $message = 'END - ProcessProjectionRankingOfficeUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));
    }
}