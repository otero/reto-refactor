<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Application\DTO\GetChallengeRankingRegional;
use RetoApiBundle\Domain\Exception\OfficeReto\OfficeRetoNotFoundException;
use RetoApiBundle\Domain\Repository\UserAuthRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\OfficeRetoRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingRegionalRepositoryInterface;

class ChallengeRankingRegionalUseCase
{
    protected $projectionRankingRegionalRepository;

    protected $userAuthRepository;

    protected $officeRetoRepository;

    protected $logger;

    public function __construct(
        ProjectionRankingRegionalRepositoryInterface $projectionRankingRegionalRepository,
        UserAuthRepositoryInterface $userAuthRepository,
        OfficeRetoRepositoryInterface $officeRetoRepository,
        LoggerInterface $logger
    ) {
        $this->userAuthRepository = $userAuthRepository;
        $this->projectionRankingRegionalRepository = $projectionRankingRegionalRepository;
        $this->officeRetoRepository = $officeRetoRepository;
        $this->logger = $logger;
    }

    public function execute(GetChallengeRankingRegional $request)
    {
        try {
            $user = $this->userAuthRepository->findUserOrFail();

            $officeReto = $this->officeRetoRepository
                ->findOneByBankSabadellIdOrFail($user->getOffice()->getCpi());

            $regionalUser = $this->projectionRankingRegionalRepository
                ->findOneByRegional($officeReto->getRegional()->getId());

        } catch (OfficeRetoNotFoundException $e) {
            $regionalUser = array();
        }

        $projection = $this->projectionRankingRegionalRepository
            ->findByFilters($request->getRed(), $request->getTerritorial());



        return array(
            'projection' => $projection,
            'regionalUser' => $regionalUser
        );
    }
}
