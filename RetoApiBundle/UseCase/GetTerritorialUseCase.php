<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Repository\Interfaces\TerritorialRepositoryInterface;

class GetTerritorialUseCase
{
    protected $territorialRepository;

    protected $logger;

    public function __construct(
        TerritorialRepositoryInterface $territorialRepository,
        LoggerInterface $logger
    ) {
        $this->territorialRepository = $territorialRepository;
        $this->logger = $logger;
    }

    public function execute()
    {
        return $this->territorialRepository->getTerritorials();
    }
}
