<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Domain\Exception\OfficeReto\OfficeRetoNotFoundException;
use RetoApiBundle\Domain\Repository\UserAuthRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\OfficeRetoRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingTerritorialRepositoryInterface;

class ChallengeRankingTerritorialUseCase
{
    protected $projectionRankingTerritorialRepository;

    protected $userAuthRepository;

    protected $officeRetoRepository;

    protected $logger;

    public function __construct(
        ProjectionRankingTerritorialRepositoryInterface $projectionRankingTerritorialRepository,
        UserAuthRepositoryInterface $userAuthRepository,
        OfficeRetoRepositoryInterface $officeRetoRepository,
        LoggerInterface $logger
    ) {
        $this->userAuthRepository = $userAuthRepository;
        $this->projectionRankingTerritorialRepository = $projectionRankingTerritorialRepository;
        $this->officeRetoRepository = $officeRetoRepository;
        $this->logger = $logger;
    }

    public function execute()
    {
        try {
            $user = $this->userAuthRepository->findUserOrFail();

            $officeReto = $this->officeRetoRepository
                ->findOneByBankSabadellIdOrFail($user->getOffice()->getCpi());

            $territorialUser = $this->projectionRankingTerritorialRepository
                ->findOneByRed($officeReto->getRegional()->getRed()->getTerritorial()->getId());

        } catch (OfficeRetoNotFoundException $e) {
            $territorialUser = array();
        }

        $projection = $this->projectionRankingTerritorialRepository
            ->findByFilters();

        return array(
            'projection' => $projection,
            'territorialUser' => $territorialUser
        );
    }
}
