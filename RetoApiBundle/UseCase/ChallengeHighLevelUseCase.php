<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Domain\Service\MyChallengeService;
use RetoApiBundle\Repository\HistoricOfficeRepository;
use RetoApiBundle\Domain\Repository\UserAuthRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\TargetRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\HistoricRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ChallengeRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\OfficeRetoRepositoryInterface;
use RetoApiBundle\Domain\Exception\Historic\HistoricNotFoundException;
use RetoApiBundle\Repository\Interfaces\HistoricOfficeRepositoryInterface;

class ChallengeHighLevelUseCase
{
    /**
     * @var ChallengeRepositoryInterface
     */
    protected $challengeRepository;

    /**
     * @var HistoricRepositoryInterface
     */
    protected $historicRepository;

    /**
     * @var TargetRepositoryInterface
     */
    protected $targetRepository;

    /**
     * @var OfficeRetoRepositoryInterface
     */
    protected $officeRetoRepository;

    /**
     * @var HistoricOfficeRepository
     */
    protected $historicOfficeRepository;

    /**
     * @var MyChallengeService
     */
    protected $myChallengeService;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * ChallengeUserUseCase constructor.
     * @param ChallengeRepositoryInterface $challengeRepository
     * @param HistoricRepositoryInterface $historicRepository
     * @param TargetRepositoryInterface $targetRepository
     * @param OfficeRetoRepositoryInterface $officeRetoRepository
     * @param HistoricOfficeRepositoryInterface $historicOfficeRepository
     * @param MyChallengeService $myChallengeService
     * @param LoggerInterface $logger
     */
    public function __construct(
        ChallengeRepositoryInterface $challengeRepository,
        HistoricRepositoryInterface $historicRepository,
        TargetRepositoryInterface $targetRepository,
        OfficeRetoRepositoryInterface $officeRetoRepository,
        HistoricOfficeRepositoryInterface $historicOfficeRepository,
        MyChallengeService $myChallengeService,
        LoggerInterface $logger
    ) {
        $this->challengeRepository = $challengeRepository;
        $this->historicRepository = $historicRepository;
        $this->targetRepository = $targetRepository;
        $this->officeRetoRepository = $officeRetoRepository;
        $this->historicOfficeRepository = $historicOfficeRepository;
        $this->myChallengeService = $myChallengeService;
        $this->logger = $logger;
    }

    public function execute()
    {
        $now = new \DateTime('NOW');
        $challenge = $this->challengeRepository
            ->findOneByYearAndActiveOrFail($now->format('Y'));

        $historic = $this->historicRepository->findOneByYearAndMonth(
            $challenge->getId(),
            $now->format('Y'),
            $now->format('m')
        );

        try {
            $historicLastMonth['exist'] = true;
            $now->modify('-1 month');

            $historicLastMonth['historic'] = $this->historicRepository->findOneByYearAndMonth(
                $challenge->getId(),
                $now->format('Y'),
                $now->format('m')
            );
        } catch (HistoricNotFoundException $e) {
            $historicLastMonth['exist'] = false;
            $historicLastMonth['historic'] = null;
        }

        $targets = $this->targetRepository->findAllOrderByBsIndicator();

        $historicOfficeReto = array();
        foreach ($targets as $target) {
            $historicTarget = $this->historicOfficeRepository
                ->findByHistoricHighLevel($historic['id'], $target['id'], $target['bsIndicador'], $target['name']);

            array_push($historicOfficeReto, $historicTarget);
        }

        $historicOfficeRetoLast = false;

        if ($historicLastMonth['exist']) {
            $historicOfficeRetoLast = array();
            foreach ($targets as $target) {
                $historicTarget = $this->historicOfficeRepository
                    ->findByHistoricHighLevel($historicLastMonth['historic']['id'], $target['id'], $target['bsIndicador'], $target['name']);

                array_push($historicOfficeRetoLast, $historicTarget);
            }
        }

        return $this->myChallengeService->generate($targets, $historicOfficeReto, $historicOfficeRetoLast);
    }
}
