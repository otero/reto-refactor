<?php

namespace RetoApiBundle\UseCase;


use RetoApiBundle\Application\DTO\GetTarget;
use RetoApiBundle\Repository\Interfaces\TargetRepositoryInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

class ChallengeTargetUseCase
{
    protected $targetRepository;

    protected $logger;

    protected $rootDir;

    public function __construct(
        TargetRepositoryInterface $targetRepository,
        $rootDir,
        LoggerInterface $logger
    ) {
        $this->targetRepository = $targetRepository;
        $this->rootDir = $rootDir;
        $this->logger = $logger;
    }

    public function execute(GetTarget $request, Request $sfRequest)
    {
        $target = $this->targetRepository->findOneByBsIndicatorOrFail($request->getTarget());

        $targetMean = array();
        $targetTip = array();

        foreach ($target->getTargetMeans() as $tm) {

            $output = array(
                'title' => $tm->getTitle(),
                'description' => $tm->getDescription(),
                'type' => $tm->getTypeMean(),
                'link' => $tm->getLink()
            );

            if(!empty($tm->getFileMean())){
                $output['image'] = $sfRequest->getUriForPath(sprintf('%s/%s', '/' . $tm::DIR, $tm->getFileMean()));
            }

            array_push($targetMean, $output);
        }

        foreach ($target->getTargetTips() as $tp) {
            $filename = sprintf('%s/../web/%s/%s', $this->rootDir, $tp::DIR, $tp->getFileTip());

            $filesize = 0;

            if (file_exists($filename)){
                $filesize = filesize($filename);
            };

            array_push($targetTip, array(
                'title' => $tp->getTitle(),
                'description' => $tp->getDescription(),
                'document' => array(
                    'dir' => sprintf('%s/%s', $tp::DIR, $tp->getFileTip()),
                    'size' => $filesize
                )
            ));
        }
        return array(
            'id' => $target->getId(),
            'name' => $target->getName(),
            'title' => $target->getTargetInformation()->getTitle(),
            'subtitle' => $target->getTargetInformation()->getSubtitle(),
            'description' => $target->getTargetInformation()->getDescription(),
            'means' => $targetMean,
            'tips' => $targetTip
        );
    }
}