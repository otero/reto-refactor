<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Application\DTO\GetChallengeRankingOffice;
use RetoApiBundle\Domain\Exception\OfficeReto\OfficeRetoNotFoundException;
use RetoApiBundle\Domain\Repository\UserAuthRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\OfficeRetoRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingOfficeRepositoryInterface;

class ChallengeRankingOfficeUseCase
{
    protected $projectionRankingOfficeRepository;

    protected $userAuthRepository;

    protected $officeRetoRepository;

    protected $logger;

    public function __construct(
        ProjectionRankingOfficeRepositoryInterface $projectionRankingOfficeRepository,
        UserAuthRepositoryInterface $userAuthRepository,
        OfficeRetoRepositoryInterface $officeRetoRepository,
        LoggerInterface $logger
    ) {
        $this->userAuthRepository = $userAuthRepository;
        $this->projectionRankingOfficeRepository = $projectionRankingOfficeRepository;
        $this->officeRetoRepository = $officeRetoRepository;
        $this->logger = $logger;
    }

    public function execute(GetChallengeRankingOffice $request)
    {
        try {
            $user = $this->userAuthRepository->findUserOrFail();

            $officeReto = $this->officeRetoRepository
                ->findOneByBankSabadellIdOrFail($user->getOffice()->getCpi());

            $officeUser = $this->projectionRankingOfficeRepository
                ->findOneByOfficeReto($officeReto->getId());

        } catch (OfficeRetoNotFoundException $e) {
            $officeUser = array();
        }

        $projection = $this->projectionRankingOfficeRepository
            ->findByFilters($request->getRegional(), $request->getRed(), $request->getTerritorial(), $request->getLimit());

        return array(
            'projection' => $projection,
            'officeUser' => $officeUser
        );
    }
}
