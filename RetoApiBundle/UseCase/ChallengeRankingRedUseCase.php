<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Application\DTO\GetChallengeRankingRed;
use RetoApiBundle\Domain\Exception\OfficeReto\OfficeRetoNotFoundException;
use RetoApiBundle\Domain\Repository\UserAuthRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\OfficeRetoRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingRedRepositoryInterface;

class ChallengeRankingRedUseCase
{
    protected $projectionRankingRedRepository;

    protected $userAuthRepository;

    protected $officeRetoRepository;

    protected $logger;

    public function __construct(
        ProjectionRankingRedRepositoryInterface $projectionRankingRedRepository,
        UserAuthRepositoryInterface $userAuthRepository,
        OfficeRetoRepositoryInterface $officeRetoRepository,
        LoggerInterface $logger
    ) {
        $this->userAuthRepository = $userAuthRepository;
        $this->projectionRankingRedRepository = $projectionRankingRedRepository;
        $this->officeRetoRepository = $officeRetoRepository;
        $this->logger = $logger;
    }

    public function execute(GetChallengeRankingRed $request)
    {
        try {
            $user = $this->userAuthRepository->findUserOrFail();

            $officeReto = $this->officeRetoRepository
                ->findOneByBankSabadellIdOrFail($user->getOffice()->getCpi());

            $regionalUser = $this->projectionRankingRedRepository
                ->findOneByRed($officeReto->getRegional()->getRed()->getId());

        } catch (OfficeRetoNotFoundException $e) {
            $regionalUser = array();
        }

        $projection = $this->projectionRankingRedRepository
            ->findByFilters($request->getTerritorial());

        return array(
            'projection' => $projection,
            'redUser' => $regionalUser
        );
    }
}
