<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Application\DTO\GetRegional;
use RetoApiBundle\Repository\Interfaces\RegionalRepositoryInterface;

class GetRegionalUseCase
{
    protected $regionalRepository;

    protected $logger;

    public function __construct(
        RegionalRepositoryInterface $regionalRepository,
        LoggerInterface $logger
    ) {
        $this->regionalRepository = $regionalRepository;
        $this->logger = $logger;/**/
    }

    public function execute(GetRegional $request)
    {
        return $this->regionalRepository->getRegional($request->getRegional());
    }

    public function estructure()
    {
        $res = array();
        $regionals = $this->regionalRepository->getTerritorialRedRegionalEstructure();
        foreach ($regionals as $regional){
           $res[$regional['idTerritorial']]['nameTerritorial'] = $regional['nameTerritorial'];
           $res[$regional['idTerritorial']]['redList'][$regional['idRed']]['nameRed'] = $regional['nameRed'];
            $res[$regional['idTerritorial']]['redList'][$regional['idRed']]['regionalList'][$regional['idRegional']]['nameRegional'] = $regional['nameRegional'];
        }
        return $res;
    }

    public function estructureOrderbyName()
    {
        $res = array();
        $regionals = $this->regionalRepository->getTerritorialRedRegionalEstructure();
        foreach ($regionals as $regional){
            $res[$regional['nameTerritorial']]['idTerritorial'] = $regional['idTerritorial'];
            $res[$regional['nameTerritorial']]['redList'][$regional['nameRed']]['idRed'] = $regional['idRed'];
            $res[$regional['nameTerritorial']]['redList'][$regional['nameRed']]['regionalList'][$regional['nameRegional']]['idRegional'] = $regional['idRegional'];
        }
        return $res;
    }

}
