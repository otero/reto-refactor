<?php

namespace RetoApiBundle\UseCase;


use Doctrine\ORM\EntityManager;
use PtiBundle\Entity\Office;
use PtiBundle\Entity\User;


class ChangeUserOfficeRetoUseCase
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function execute()
    {
        $user = $this->em->getRepository(User::class)->find(21837);
        $office = $this->em->getRepository(Office::class)->find(3683);

        $user->setOffice($office);

        $user->setName('Office_User');

        $this->em->persist($user);
        $this->em->flush();
    }
}
