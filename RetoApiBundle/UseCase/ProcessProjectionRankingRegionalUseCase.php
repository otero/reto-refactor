<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Domain\Service\ProjectionRankingRegionalService;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Repository\Interfaces\HistoricRegionalAccumulatedRepositoryInterface;
use RetoApiBundle\Domain\Service\ProjectionRankingOfficeTransformRegionalService;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingRegionalRepositoryInterface;

class ProcessProjectionRankingRegionalUseCase
{
    protected $logger;

    protected $historicRegionalAccumulatedRepository;

    protected $projectionRankingRegionalService;

    protected $projectionRankingOfficeTransformRegionalService;

    protected $projectionRankingRegionalRepository;

    public function __construct(
        HistoricRegionalAccumulatedRepositoryInterface $historicRegionalAccumulatedRepository,
        ProjectionRankingRegionalService $projectionRankingRegionalService,
        ProjectionRankingOfficeTransformRegionalService $projectionRankingOfficeTransformRegionalService,
        ProjectionRankingRegionalRepositoryInterface $projectionRankingRegionalRepository,
        LoggerInterface $logger
    ) {
        $this->historicRegionalAccumulatedRepository = $historicRegionalAccumulatedRepository;
        $this->projectionRankingRegionalService = $projectionRankingRegionalService;
        $this->projectionRankingOfficeTransformRegionalService = $projectionRankingOfficeTransformRegionalService;
        $this->projectionRankingRegionalRepository = $projectionRankingRegionalRepository;
        $this->logger = $logger;
    }

    public function execute()
    {
        $message = 'START - ProcessProjectionRankingRegionalUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));

        $this->projectionRankingRegionalRepository->deleteAll();

        $regionalRanking = $this->historicRegionalAccumulatedRepository->findRankingByRegional();

        $this->projectionRankingRegionalService->generate($regionalRanking);

        $officeTransformRegionalRanking = $this->projectionRankingRegionalRepository->findRankingByOfficeTransform();

        $this->projectionRankingOfficeTransformRegionalService
            ->generate($officeTransformRegionalRanking);

        $this->logger->info('The  RankingRegionalProjection was created Successfully!', array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_PROJECTION_RANKING_REGIONAL,
            'data' => array(

            )
        ));

        $message = 'END - ProcessProjectionRankingRegionalUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));
    }
}