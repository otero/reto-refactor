<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Repository\Interfaces\OfficeRetoRepositoryInterface;

class GetOfficesUseCase
{
    protected $officeRetoRepository;

    protected $logger;

    public function __construct(
        OfficeRetoRepositoryInterface $officeRetoRepository,
        LoggerInterface $logger
    ) {
        $this->officeRetoRepository = $officeRetoRepository;
        $this->logger = $logger;
    }

    public function execute()
    {
        return $this->officeRetoRepository->getOffices();
    }



}
