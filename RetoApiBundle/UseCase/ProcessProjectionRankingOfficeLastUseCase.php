<?php

namespace RetoApiBundle\UseCase;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Domain\Service\ProjectionRankingOfficeService;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingOfficeRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\RedRepositoryInterface;

class ProcessProjectionRankingOfficeLastUseCase
{
    protected $logger;

    protected $projectionRankingOfficeService;

    public function __construct(
        ProjectionRankingOfficeService $projectionRankingOfficeService,
        LoggerInterface $logger
    ) {
        $this->projectionRankingOfficeService = $projectionRankingOfficeService;
        $this->logger = $logger;
    }

    public function execute()
    {
        $message = 'START - ProcessProjectionRankingOfficeLastUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));

        $this->projectionRankingOfficeService->generateLast();

        $this->logger->info('The  RankingOfficeLastProjection was created Successfully!', array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_PROJECTION_RANKING_OFFICE,
            'data' => array(

            )
        ));

        $message = 'END - ProcessProjectionRankingOfficeLastUseCase::execute()';
        $this->logger->debug($message, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array('message' => $message)
        ));
    }
}