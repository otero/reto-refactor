<?php

namespace RetoApiBundle\UseCase;


use Doctrine\ORM\EntityManager;
use PtiBundle\Entity\Office;
use PtiBundle\Entity\User;


class ChangeUserSSCCRetoUseCase
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function execute()
    {
        $user = $this->em->getRepository(User::class)->find(21837);
        $office = $this->em->getRepository(Office::class)->find(4610);

        $user->setOffice($office);

        $user->setName('JOAQUÍN');

        $this->em->persist($user);
        $this->em->flush();
    }
}
