<?php

namespace RetoApiBundle\Infraestructure\Doctrine\DQL;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class YearFunction extends FunctionNode
{
    public $fecha;

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return "YEAR(" . $this->fecha->dispatch($sqlWalker) . ")";
    }

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $lexer = $parser->getLexer();

        $parser->match(Lexer::T_IDENTIFIER);       //Nombre de la Funcion
        $parser->match(Lexer::T_OPEN_PARENTHESIS); //Parantesis abierto

        $this->fecha = $parser->ArithmeticPrimary(); //Dato

        $parser->match(Lexer::T_CLOSE_PARENTHESIS); //Parentesis Cerrado
    }
}