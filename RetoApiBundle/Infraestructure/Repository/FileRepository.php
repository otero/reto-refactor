<?php

namespace RetoApiBundle\Infraestructure\Repository;

use Symfony\Component\HttpFoundation\File\File;
use RetoApiBundle\Domain\Repository\FileRepositoryInterface;

class FileRepository implements FileRepositoryInterface
{
    /**
     * @var string
     */
    protected $uploadRetoDir;

    /**
     * FileService constructor.
     * @param string $uploadRetoDir
     */
    public function __construct($uploadRetoDir)
    {
        $this->uploadRetoDir = $uploadRetoDir;
    }

    public function upload($pathFile)
    {
        //TODO: implement
    }

    public function get($pathFile)
    {
        return new File($this->uploadRetoDir . DIRECTORY_SEPARATOR . $pathFile);
    }
}