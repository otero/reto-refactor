<?php

namespace RetoApiBundle\Infraestructure\Repository;

use RetoApiBundle\Domain\Exception\Historic\UserAuthNotFoundException;
use RetoApiBundle\Domain\Repository\UserAuthRepositoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class UserAuthRepository implements UserAuthRepositoryInterface
{
    public $token;

    public function __construct(TokenStorage $token)
    {
        $this->token = $token;
    }

    public function findUser()
    {
        if (null === $token = $this->token->getToken()) {

            return false;
        }

        if (!is_object($user = $token->getUser())) {

            return false;
        }

        return $user;
    }

    public function findUserOrFail()
    {
        if (!$user =$this->findUser()) {

            throw new UserAuthNotFoundException();
        }

        return $this->findUser();
    }
}
