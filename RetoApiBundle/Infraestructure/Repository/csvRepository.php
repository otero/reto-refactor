<?php

namespace RetoApiBundle\Infraestructure\Repository;


use RetoApiBundle\Entity\CSV;
use RetoApiBundle\Entity\Historic;
use Symfony\Component\HttpFoundation\File\File;
use RetoApiBundle\Domain\Repository\CSVRepositoryInterface;

class csvRepository implements CSVRepositoryInterface
{
    public function get($file)
    {
        $territorial = array();
        $regional = array();
        $red = array();
        $officeReto = array();

        /**
         * @var $file File
         */
        $handle = fopen($file->getRealPath(), "r");
        fgets($handle);

        while (($row = fgetcsv($handle, null, CSV::DELIMITER)) !== FALSE) {
            $row = array_map('utf8_encode', $row);

            if ($row[CSV::POS_TARGET_ACCMP] === '') { continue; }

            switch ($row[CSV::POS_UNIT]) {
                case Historic::ACRONYM_OFFICE:
                    array_push($officeReto, $row);
                    break;
                case Historic::ACRONYM_RED:
                    array_push($red, $row);
                    break;
                case Historic::ACRONYM_REGIONAL:
                    array_push($regional, $row);
                    break;
                case Historic::ACRONYM_TERRITORIAL:
                    array_push($territorial, $row);
                    break;
            }

        }

        fclose($handle);

        return array(
            Historic::UNIT_TERRITORIAL => $territorial,
            Historic::UNIT_RED => $red,
            Historic::UNIT_REGIONAL => $regional,
            Historic::UNIT_OFFICERETO => $officeReto
        );
    }
}