<?php

namespace RetoApiBundle\Command;

use RetoApiBundle\Util\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class RankingRedCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('reto:ranking-red')
            ->setDescription('Generate ranking red by office');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = Utils::microTimeFloat();

        $processStatsRetoUseCase = $this->getContainer()->get('reto.top_ranking.use_case');
        $processStatsRetoUseCase->execute();

        $timeEnd = Utils::microTimeFloat();
        $output->writeln('Finished, time: ' . ($timeEnd-$timeStart));
    }
}
