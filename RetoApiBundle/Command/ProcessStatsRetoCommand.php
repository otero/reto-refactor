<?php

namespace RetoApiBundle\Command;

use Psr\Log\LoggerInterface;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Util\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class ProcessStatsRetoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('reto:process-stats')
            ->setDescription('Process stats reto (csv file)');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = Utils::microTimeFloat();

        $output->writeln('Started, time: ' . date('m/d/Y H:i:s'));

        $processStatsRetoUseCase = $this->getContainer()->get('reto.process_stats_reto.use_case');
        $processStatsRetoUseCase->execute();

        $processStatsRetoUseCase = $this->getContainer()->get('reto.top_ranking.use_case');
        $processStatsRetoUseCase->execute();

        $processStatsRetoUseCase = $this->getContainer()->get('reto.process_projection_ranking_office.use_case');
        $processStatsRetoUseCase->execute();

        $processProjectionRankingRegional = $this->getContainer()->get('reto.process_projection_ranking_regional.use_case');
        $processProjectionRankingRegional->execute();

        $processProjectionRankingRed = $this->getContainer()->get('reto.process_projection_ranking_red.use_case');
        $processProjectionRankingRed->execute();

        $processProjectionRankingTerritorial = $this->getContainer()->get('reto.process_projection_ranking_territorial.use_case');
        $processProjectionRankingTerritorial->execute();

        $processStatsRetoUseCase = $this->getContainer()->get('reto.process_projection_ranking_office_last.use_case');
        $processStatsRetoUseCase->execute();


        $timeEnd = Utils::microTimeFloat();
        $output->writeln('Finished, time: ' . date('m/d/Y H:i:s'));
        $output->writeln('Elapsed: ' . ($timeEnd-$timeStart));
    }
}
