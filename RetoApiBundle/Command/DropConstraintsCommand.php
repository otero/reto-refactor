<?php

namespace RetoApiBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Thanks to
 * http://www.archonsystems.com/devblog/2012/05/25/how-to-drop-a-column-with-a-default-value-constraint-in-sql-server/
 *
 *
 * Class DropConstraintsCommand
 * @package RetoApiBundle\Command
 */

class DropConstraintsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('reto:drop-columns-default-value')
            ->setDescription('Drop columns with default value (constraints)');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tables = $this->getTables();
        $dbal = $this->getContainer()->get('doctrine.dbal.default_connection');
        $columns = $this->getColumns();


        try {

            foreach ($tables as $table) {

                foreach ($columns as $c) {

                    $output->writeln("Eliminando CONSTRAINT para $table .... ");

                    $sql = $this->getSqlDropColumnConstraint($table, $c);
                    $result = $dbal->query($sql)->execute();

                    $output->writeln($result ."\n");
                }
            }

            $output->writeln('Finished OK');

        } catch (\Doctrine\DBAL\DBALException $DBALException) {
            $output->writeln($DBALException->getMessage());

        } catch (\Exception $ex) {
            $output->writeln($ex->getMessage());
        }
    }




    /**
     *
     * @param $table
     * @param $column
     * @return string
     */
    private function getSqlDropColumnConstraint($table, $column)
    {
        $sql = "DECLARE @ConstraintName nvarchar(200)
SELECT @ConstraintName = Name FROM SYS.DEFAULT_CONSTRAINTS WHERE PARENT_OBJECT_ID = OBJECT_ID('$table') AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns WHERE NAME = N'$column' AND object_id = OBJECT_ID(N'$table'))
IF @ConstraintName IS NOT NULL
EXEC('ALTER TABLE $table DROP CONSTRAINT ' + @ConstraintName)
IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('$table') AND name='$column')
EXEC('ALTER TABLE $table DROP COLUMN $column')";

        return $sql;
    }



    private function getTables()
    {
        return ['office_reto', 'red', 'regional', 'territorial'];
    }


    private function getColumns()
    {
        return ['position',
                'points',
                'current_annual_position',
                'last_annual_position',
                'current_punctual_position',
                'last_punctual_position'
        ];
    }
}