<?php

namespace RetoApiBundle\Command;

use RetoApiBundle\Util\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class ProjectionRankingRedCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('reto:projection-ranking-red')
            ->setDescription('Generate a projection for red ranking');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = Utils::microTimeFloat();

        $processProjectionRankingRed = $this->getContainer()->get('reto.process_projection_ranking_red.use_case');
        $processProjectionRankingRed->execute();

        $timeEnd = Utils::microTimeFloat();
        $output->writeln('Finished, time: ' . ($timeEnd-$timeStart));
    }
}
