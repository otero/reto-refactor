<?php

namespace RetoApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CountRowsRetoTablesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reto:count-rows-reto-tables')
            ->setDescription('Count the rows of each table reto!');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');

        $rows = $entityManager->getRepository('RetoApiBundle:ProjectionRankingOffice')->countAll();
        $output->writeln(sprintf('%s: %s', 'PROJECTION RANKING OFFICE', $rows['count_rows']));

        $rows = $entityManager->getRepository('RetoApiBundle:ProjectionRankingRegional')->countAll();
        $output->writeln(sprintf('%s: %s', 'PROJECTION RANKING REGIONAL', $rows['count_rows']));

        $rows = $entityManager->getRepository('RetoApiBundle:ProjectionRankingRed')->countAll();
        $output->writeln(sprintf('%s: %s', 'PROJECTION RANKING RED', $rows['count_rows']));

        $rows = $entityManager->getRepository('RetoApiBundle:ProjectionRankingTerritorial')->countAll();
        $output->writeln(sprintf('%s: %s', 'PROJECTION RANKING TERRITORIAL', $rows['count_rows']));

        $rows = $entityManager->getRepository('RetoApiBundle:RankingRed')->countAll();
        $output->writeln(sprintf('%s: %s', 'PROJECTION RANKING RED', $rows['count_rows']));

        $rows = $entityManager->getRepository('RetoApiBundle:HistoricOffice')->countAll();
        $output->writeln(sprintf('%s: %s', 'HISTORIC OFFICE', $rows['count_rows']));

        $rows = $entityManager->getRepository('RetoApiBundle:HistoricOfficeAccumulated')->countAll();
        $output->writeln(sprintf('%s: %s', 'HISTORIC OFFICE ACCUMULATED', $rows['count_rows']));

        $rows = $entityManager->getRepository('RetoApiBundle:HistoricRegional')->countAll();
        $output->writeln(sprintf('%s: %s', 'HISTORIC REGIONAL', $rows['count_rows']));

        $rows = $entityManager->getRepository('RetoApiBundle:HistoricRegionalAccumulated')->countAll();
        $output->writeln(sprintf('%s: %s', 'HISTORIC REGIONAL ACCUMULATED', $rows['count_rows']));

        $rows = $entityManager->getRepository('RetoApiBundle:HistoricRed')->countAll();
        $output->writeln(sprintf('%s: %s', 'HISTORIC RED', $rows['count_rows']));

        $rows = $entityManager->getRepository('RetoApiBundle:HistoricRedAccumulated')->countAll();
        $output->writeln(sprintf('%s: %s', 'HISTORIC RED ACCUMULATED', $rows['count_rows']));

        $rows = $entityManager->getRepository('RetoApiBundle:HistoricTerritorial')->countAll();
        $output->writeln(sprintf('%s: %s', 'HISTORIC TERRITORIAL', $rows['count_rows']));

        $rows = $entityManager->getRepository('RetoApiBundle:HistoricTerritorialAccumulated')->countAll();
        $output->writeln(sprintf('%s: %s', 'HISTORIC TERRITORIAL ACCUMULATED', $rows['count_rows']));
    }
}