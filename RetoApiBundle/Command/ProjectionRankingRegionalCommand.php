<?php

namespace RetoApiBundle\Command;

use RetoApiBundle\Util\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class ProjectionRankingRegionalCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('reto:projection-ranking-regional')
            ->setDescription('Generate a projection for regional ranking');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = Utils::microTimeFloat();

        $processProjectionRankingRegional = $this->getContainer()->get('reto.process_projection_ranking_regional.use_case');
        $processProjectionRankingRegional->execute();

        $timeEnd = Utils::microTimeFloat();
        $output->writeln('Finished, time: ' . ($timeEnd-$timeStart));
    }
}
