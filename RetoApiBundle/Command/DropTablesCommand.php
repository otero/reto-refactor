<?php

namespace RetoApiBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DropTablesCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('reto:drop-old-tables')
            ->setDescription('Drop old tables (last reto)');
    }



    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dbal = $this->getContainer()->get('doctrine.dbal.default_connection');
        $tables = $this->getTables();

        try {

            foreach ($tables as $table) {

                $output->writeln("Eliminando $table ... \n") ;

                $sql = "DROP TABLE $table";
                $result = $dbal->executeQuery($sql);

                $output->writeln($result->columnCount() ."\n");
            }

        } catch (\Doctrine\DBAL\DBALException $DBALException) {
            $output->writeln($DBALException->getMessage());

        } catch (\Exception $ex) {
            $output->writeln($ex->getMessage());
        }

        $output->writeln('Finished OK');
    }



    private function getTables()
    {
        // tiene que se este orden por las FK
        return [
            'bank_challenge',
            'office_reto_challenge',
            'red_challenge',
            'regional_challenge',
            'territorial_challenge',
            'calculations',
            'current_bank_data',
            'current_office_data',
            'current_red_data',
            'current_regional_data',
            'current_territorial_data',
            'historic_bank_data',
            'historic_csv_bank',
            'historic_csv_office',
            'historic_csv_red',
            'historic_csv_regional',
            'historic_csv_territorial',
            'historic_office_data',
            'historic_red_data',
            'historic_regional_data',
            'historic_territorial_data',
            'challenge_field',
            'challenge'


        ];
    }

}