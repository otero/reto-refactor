<?php

namespace RetoApiBundle\Command;

use RetoApiBundle\Util\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class ProjectionRankingTerritorialCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('reto:projection-ranking-territorial')
            ->setDescription('Generate a projection for territorial ranking');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = Utils::microTimeFloat();

        $processProjectionRankingTerritorial = $this->getContainer()->get('reto.process_projection_ranking_territorial.use_case');
        $processProjectionRankingTerritorial->execute();

        $timeEnd = Utils::microTimeFloat();
        $output->writeln('Finished, time: ' . ($timeEnd-$timeStart));
    }
}
