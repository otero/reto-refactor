<?php

namespace RetoApiBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ChangeUserForHasOfficeRetoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reto:change-user-for-has-office-reto')
            ->setDescription('Change the office reto of the user');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $changeUserForHasOfficeRetoUseCase = $this->getContainer()->get('reto.change_user_office_reto.use_case');
        $changeUserForHasOfficeRetoUseCase->execute();

        $output->writeln('Finished');
    }
}
