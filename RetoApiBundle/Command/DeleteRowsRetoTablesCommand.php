<?php

namespace RetoApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class DeleteRowsRetoTablesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reto:delete-rows-reto-tables')
            ->setDescription('WARNING: DELETE all rows!');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');

        $entityManager->getRepository('RetoApiBundle:ProjectionRankingOffice')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:ProjectionRankingRegional')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:ProjectionRankingRed')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:ProjectionRankingTerritorial')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:RankingRed')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:HistoricOffice')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:HistoricOfficeAccumulated')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:HistoricRegional')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:HistoricRegionalAccumulated')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:HistoricRed')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:HistoricRedAccumulated')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:HistoricTerritorial')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:HistoricTerritorialAccumulated')->deleteAll();
        $entityManager->getRepository('RetoApiBundle:Historic')->deleteAll();

        $output->writeln('Finished');
    }
}