<?php

namespace RetoApiBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Entity\Interfaces\ProjectionRankingOfficeInterface;
use RetoApiBundle\Entity\ProjectionRankingOffice;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingOfficeRepositoryInterface;
use RetoApiBundle\Domain\Exception\ProjectionRankingOffice\ProjectionRankingOfficeException;

const PLATINUM_MEDAL = 0;
const GOLD_MEDAL = 1;
const SILVER_MEDAL = 2;
const CUPPER_MEDAL = 3;

class ProjectionRankingOfficeRepository extends EntityRepository implements ProjectionRankingOfficeRepositoryInterface
{
    public function findMedalsByRegional($regional)
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
            SELECT TOP 1
                   (SELECT COUNT(*) FROM projection_ranking_office p1 WHERE p1.regional_id = :regional_platinum AND p1.medal = 0) as count_medals_platinum,
                   (SELECT COUNT(*) FROM projection_ranking_office p1 WHERE p1.regional_id = :regional_golden AND p1.medal = 1) as count_medals_golden,
                   (SELECT COUNT(*) FROM projection_ranking_office p1 WHERE p1.regional_id = :regional_silver AND p1.medal = 2) as count_medals_silver,
                   (SELECT COUNT(*) FROM projection_ranking_office p1 WHERE p1.regional_id = :regional_bronze AND p1.medal = 3) as count_medals_bronze,
                   (SELECT COUNT(*) FROM projection_ranking_office p1 WHERE p1.regional_id = :regional_offices_transform AND p1.cmp_annual >= 1) as count_offices_transform,
                   (SELECT COUNT(*) FROM projection_ranking_office p1 WHERE p1.regional_id = :regional_offices) as count_total_offices
          FROM projection_ranking_office p
            WHERE p.regional_id = :regional
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute(array(
            'regional' => $regional,
            'regional_platinum' => $regional,
            'regional_golden' => $regional,
            'regional_silver' => $regional,
            'regional_bronze' => $regional,
            'regional_offices_transform' => $regional,
            'regional_offices' => $regional
        ));

        return $stmt->fetchAll();
    }

    public function findOfficesTransformByRed($red)
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
            SELECT TOP 1
                   (SELECT COUNT(*) FROM projection_ranking_office p1 WHERE p1.red_id = :red_offices_transform AND p1.cmp_annual >= 1) as count_offices_transform,
                   (SELECT COUNT(*) FROM projection_ranking_office p1 WHERE p1.red_id = :red_offices) as count_total_offices
          FROM projection_ranking_office p
            WHERE p.red_id = :red
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute(array(
            'red' => $red,
            'red_offices_transform' => $red,
            'red_offices' => $red
        ));

        return $stmt->fetchAll();
    }

    public function findOfficesTransformByTerritorial($territorial)
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
            SELECT TOP 1
                   (SELECT COUNT(*) FROM projection_ranking_office p1 WHERE p1.territorial_id = :territorial_offices_transform AND p1.cmp_annual >= 1) as count_offices_transform,
                   (SELECT COUNT(*) FROM projection_ranking_office p1 WHERE p1.territorial_id = :territorial_offices) as count_total_offices
          FROM projection_ranking_office p
            WHERE p.territorial_id = :territorial
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute(array(
            'territorial' => $territorial,
            'territorial_offices_transform' => $territorial,
            'territorial_offices' => $territorial
        ));

        return $stmt->fetchAll();
    }

    public function findByHistoricOfficeAccumulated($officeId)
    {
        return $this->findOneBy(array(
            'officeId' => $officeId
        ));
    }

    public function findByHistoricOfficeAccumulatedOrFail($officeId)
    {
        $rankingRed = $this->findByHistoricOfficeAccumulated($officeId);

        if (!$rankingRed instanceof ProjectionRankingOfficeInterface) {

            throw new ProjectionRankingOfficeException();
        }

        return $rankingRed;
    }

    public function findByFilters($regional, $red, $territorial, $limit)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('p');
        $qb->from('RetoApiBundle:ProjectionRankingOffice', 'p');

        if (count($red) > 0) {
            $qb->andWhere('p.redId IN (:red)');
            $qb->setParameter('red', $red);
        }

        if (count($regional) > 0) {
            $qb->andWhere('p.regionalId IN (:regional)');
            $qb->setParameter('regional', $regional);
        }

        if (count($territorial) > 0) {
            $qb->andWhere('p.territorialId IN (:territorial)');
            $qb->setParameter('territorial', $territorial);
        }

        if (isset($limit) && $limit > 0) {
            $qb->setMaxResults($limit);
        }

        $qb->orderBy('p.ranking', 'ASC');

        return $qb->getQuery()->getArrayResult();
    }

    public function findOneByOfficeReto($officeReto)
    {
        return $this->getEntityManager()
            ->createQuery(
                "
                    SELECT p FROM RetoApiBundle:ProjectionRankingOffice p 
                              WHERE p.officeId = :office
                "
            )
            ->setParameter('office', $officeReto)
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }


    public function findOneByOfficeId($officeId)
    {
        return $this->getEntityManager()
            ->createQuery(
                "
                    SELECT p FROM RetoApiBundle:ProjectionRankingOffice p 
                              WHERE p.officeId = :office
                "
            )
            ->setParameter('office', $officeId)
            ->getOneOrNullResult(Query::HYDRATE_OBJECT);
    }

    public function deleteAll()
    {
        $this->getEntityManager()->createQuery('DELETE RetoApiBundle:ProjectionRankingOffice p')->execute();
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:ProjectionRankingOffice p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function createOrUpdate($projectionRankingOffice)
    {
        $this->getEntityManager()->persist($projectionRankingOffice);
        // $this->getEntityManager()->flush();
    }

    public function findOneByCmpAnnual($sortOrder)
    {
        $office = $this->getEntityManager()
            ->createQuery(
                "SELECT p FROM RetoApiBundle:ProjectionRankingOffice p ORDER BY p.cmpAnnual $sortOrder"
            )
            ->setMaxResults(1)
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);

        return $office;
    }

    public function findByMedalType($medalType = 0)
    {
        $office = $this->getEntityManager()
            ->createQuery(
                "SELECT p FROM RetoApiBundle:ProjectionRankingOffice p where p.medal = $medalType "
            )
            ->getResult(Query::HYDRATE_ARRAY);

        return $office;

    }

}
