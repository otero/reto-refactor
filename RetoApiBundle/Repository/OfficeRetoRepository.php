<?php
namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RetoApiBundle\Domain\Exception\OfficeReto\OfficeRetoNotFoundException;
use RetoApiBundle\Entity\Interfaces\OfficeRetoInterface;
use RetoApiBundle\Repository\Interfaces\OfficeRetoRepositoryInterface;

class OfficeRetoRepository extends EntityRepository implements OfficeRetoRepositoryInterface
{
    public function getDataTableInfo($dataTableParams)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select('o')
            ->from('RetoApiBundle:OfficeReto', 'o');
        $cloneQuery = clone $query;
        $recordsFiltered = $cloneQuery->select('count(o.id)')->getQuery()->getSingleScalarResult();

         $query
            ->setFirstResult($dataTableParams['start'])
            ->setMaxResults($dataTableParams['length']);

        $cols = array('o.id', 'o.name', 'o.bancsabadellId', 'o.regional', 'o.red', 'o.territorial', 'o.bank');
        if ($dataTableParams['order']) {
            $idxCol = $dataTableParams['order'][0]['column'];
            $dirOrder = $dataTableParams['order'][0]['dir'];

            $col = $cols[$idxCol];

            $query
                ->orderBy($col, $dirOrder);
        }

        $records = $query->getQuery()->getResult();
       $outputData = array();

        foreach($records as $idx=>$elem) {
            $outputData[] = array(
                $elem->getId(),
                $elem->getName(),
                $elem->getBancSabadellId(),
                (string)$elem->getRegional(),
                (string)$elem->getRegional()->getRed(),
                (string)$elem->getRegional()->getRed()->getTerritorial(),
                (string)$elem->getRegional()->getRed()->getTerritorial()->getBank(),
            );
        }

        $response['draw'] = $dataTableParams['draw'];
        $response['recordsTotal'] = count($this->findAll());
        $response['recordsFiltered'] = $recordsFiltered;
        $response['data'] = $outputData;

        return $response;
    }

    public function findOneByBankSabadellId($id)
    {
        return $this->findOneBy(['bancsabadellId' => $id]);
    }

    public function findOneByBankSabadellIdOrFail($id)
    {
        $officeReto = $this->findOneByBankSabadellId($id);

        if (!$officeReto instanceof OfficeRetoInterface) {

            throw new OfficeRetoNotFoundException();
        }

        return $officeReto;
    }

    public function getOffices()
    {
        $offices = $this->getEntityManager()
            ->createQuery(
                "SELECT o.id, o.name FROM RetoApiBundle:OfficeReto o ORDER BY o.name ASC"
            )
            ->getArrayResult();

        return $offices;
    }

    public function createOrUpdate($officeReto)
    {
        $this->getEntityManager()->persist($officeReto);
        $this->getEntityManager()->flush();
    }
}
