<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Entity\Interfaces\RankingRedInterface;
use RetoApiBundle\Repository\Interfaces\RankingRedRepositoryInterface;
use RetoApiBundle\Domain\Exception\RankingRed\RankingRedNotFoundException;

class RankingRedRepository extends EntityRepository implements RankingRedRepositoryInterface
{

    public function findByHistoricOfficeAccumulated($historicOfficeAccumulated)
    {
        return $this->findOneBy(array(
            'historicOfficeAccumulated' => $historicOfficeAccumulated
        ));
    }

    public function findByHistoricOfficeAccumulatedOrFail($historicOfficeAccumulated)
    {
        $rankingRed = $this->findByHistoricOfficeAccumulated($historicOfficeAccumulated);

        if (!$rankingRed instanceof RankingRedInterface) {

            throw new RankingRedNotFoundException();
        }

        return $rankingRed;
    }

    public function findTopFiveByRed($red)
    {
        $topFive = $this->getEntityManager()
            ->createQuery(
                "SELECT r.id, r.ranking, o.name, r.last, r.cmpAccumulated as cmp_accumulated, r.cmpAccumulated * 100 as  cmp_accumulated_anual
                        FROM RetoApiBundle:RankingRed r 
                        LEFT JOIN r.historicOfficeAccumulated h
                        LEFT JOIN h.officereto o
                        LEFT JOIN o.regional re
                        LEFT JOIN re.red red 
                        WHERE red.id = :red AND r.ranking <= 5
                        ORDER BY r.ranking ASC
                "
            )
            ->setParameter('red', $red)
            ->getArrayResult();

       return $topFive;
    }

    public function findOneRankingByOffice($office)
    {
        $ranking = $this->getEntityManager()
            ->createQuery(
                "SELECT r.id, r.ranking, o.name, r.last, r.cmpAccumulated as cmp_accumulated, r.cmpAccumulated * 100 as  cmp_accumulated_anual
                        FROM RetoApiBundle:RankingRed r 
                        LEFT JOIN r.historicOfficeAccumulated h
                        LEFT JOIN h.officereto o
                        WHERE o.id = :office
                "
            )
            ->setParameter('office', $office)
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);

        return $ranking;
    }

    public function deleteAll()
    {
        $query = $this->getEntityManager()->createQuery('DELETE RetoApiBundle:RankingRed rr');
        $query->execute();
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:RankingRed p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }


    public function createOrUpdate($rankingRed)
    {
        $this->getEntityManager()->persist($rankingRed);
        //$this->getEntityManager()->flush();
    }
}
