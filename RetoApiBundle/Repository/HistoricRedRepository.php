<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Repository\Interfaces\HistoricRedRepositoryInterface;

class HistoricRedRepository extends EntityRepository implements HistoricRedRepositoryInterface
{
    public function findOneByHistoricAndTarget($historic, $target, $red)
    {
        $historicRed = $this->getEntityManager()
            ->createQuery(
                "SELECT p FROM RetoApiBundle:HistoricRed p 
                    LEFT JOIN p.historic h
                    LEFT JOIN p.target t
                    LEFT JOIN p.red red
                    WHERE h.id = :historic_id AND t.id = :target_id AND red.id = :red_id
                "
            )
            ->setParameter('historic_id', $historic->getId())
            ->setParameter('target_id', $target->getId())
            ->setParameter('red_id', $red->getId())
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);

        return $historicRed;
    }

    public function deleteByHistoric($historicId)
    {
        $qb = $this->createQueryBuilder('h');
        $qb->delete();
        $qb->where('h.historic = :historic_id');
        $qb->setParameter('historic_id', $historicId);

        return $qb->getQuery()->execute();
    }

    public function deleteAll()
    {
        $query = $this->getEntityManager()->createQuery('DELETE RetoApiBundle:HistoricRed hr');
        $query->execute();
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:HistoricRed p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function createOrUpdate($historicRed)
    {
        $this->getEntityManager()->persist($historicRed);
        //$this->getEntityManager()->flush();
    }
}
