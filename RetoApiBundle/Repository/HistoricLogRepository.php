<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RetoApiBundle\Repository\Interfaces\HistoricLogRepositoryInterface;

class HistoricLogRepository extends EntityRepository implements HistoricLogRepositoryInterface
{
    protected  $entityManager;

    public function createOrUpdate($historicLog)
    {
        $this->entityManager = $this->getEntityManager();

        if (!$this->entityManager->isOpen()) {
            $this->entityManager = $this->entityManager->create(
                $this->entityManager->getConnection(),
                $this->entityManager->getConfiguration()
            );
        }

        $this->entityManager->persist($historicLog);
        $this->entityManager->flush();
    }
}