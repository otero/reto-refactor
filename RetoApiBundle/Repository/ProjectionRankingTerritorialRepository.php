<?php

namespace RetoApiBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Entity\Interfaces\ProjectionRankingTerritorialInterface;
use RetoApiBundle\Entity\ProjectionRankingTerritorial;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingTerritorialRepositoryInterface;
use RetoApiBundle\Domain\Exception\ProjectionRankingTerritorial\ProjectionRankingTerritorialException;

class ProjectionRankingTerritorialRepository extends EntityRepository implements ProjectionRankingTerritorialRepositoryInterface
{

    public function findOneByHistoricTerritorialAccumulated($historicTerritorialAccumulated)
    {
        return $this->findOneBy(array(
            'historicTerritorialAccumulatedId' => $historicTerritorialAccumulated
        ));
    }

    public function findOneByHistoricTerritorialAccumulatedOrFail($historicTerritorialAccumulated)
    {
        $projectionRankingTerritorial = $this->findOneByHistoricTerritorialAccumulated($historicTerritorialAccumulated);

        if (!$projectionRankingTerritorial instanceof ProjectionRankingTerritorialInterface) {

            throw new ProjectionRankingTerritorialException();
        }

        return $projectionRankingTerritorial;
    }

    public function findByFilters()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('p');
        $qb->from('RetoApiBundle:ProjectionRankingTerritorial', 'p');

        $qb->orderBy('p.rankingOfficesTransform', 'ASC');

        return $qb->getQuery()->getArrayResult();
    }

    public function findOneByRed($territorial)
    {
        return $this->getEntityManager()
            ->createQuery(
                "
                    SELECT p FROM RetoApiBundle:ProjectionRankingTerritorial p 
                              WHERE p.territorialId = :territorial
                "
            )
            ->setParameter('territorial', $territorial)
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function findRankingByOfficeTransform()
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
           SELECT p.id,
                   p.historic_territorial_accumulated_id,
                   p.offices_transform,
                   p.ranking_offices_transform,
                   DENSE_RANK() OVER (order by  p.offices_transform DESC, p.cmp_annual DESC) AS ranking
            FROM projection_ranking_territorial p
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function deleteAll()
    {
        $this->getEntityManager()->createQuery('DELETE RetoApiBundle:ProjectionRankingTerritorial p')->execute();
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:ProjectionRankingTerritorial p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function createOrUpdate($projectionRankingTerritorial)
    {
        $this->getEntityManager()->persist($projectionRankingTerritorial);
        //$this->getEntityManager()->flush();
    }
}
