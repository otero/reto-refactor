<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;
use RetoApiBundle\Repository\Interfaces\HistoricOfficeRepositoryInterface;

class HistoricOfficeRepository extends EntityRepository implements HistoricOfficeRepositoryInterface
{
    private $pageSize;

    public function findOneByHistoricAndTarget($historic, $target, $office)
    {
        $historicOffice = $this->getEntityManager()
            ->createQuery(
                "SELECT p FROM RetoApiBundle:HistoricOffice p 
                    LEFT JOIN p.historic h
                    LEFT JOIN p.target t
                    LEFT JOIN p.officereto r
                    WHERE h.id = :historic_id AND t.id = :target_id AND r.id = :office_id
                "
            )
            ->setParameter('historic_id', $historic->getId())
            ->setParameter('target_id', $target->getId())
            ->setParameter('office_id', $office->getId())
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);

        return $historicOffice;
    }

    public function findOrderedByTargetAccomplishment($targetId, $historicId, $page)
    {
        $this->pageSize = 10;

        $query = $this->createQueryBuilder('p')
                    ->where('p.historic = ' .$historicId)
                    ->andWhere('p.target = ' . $targetId)
                    ->orderBy('p.accomplishment', 'DESC');

        $paginator = $this->paginate($query, $page);

        return $paginator->getIterator();
    }

    public function getOfficeTargetAccomplishments($historicId, $officeId)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "select t.name, accomplishment
                    from historic_office ho
                      join historic h on ho.historic_id = h.id
                      join target t on ho.target_id = t.id
                    where
                      h.id = :historic_id
                      and ho.office_id = :office_id";

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            array(
                'historic_id' => $historicId,
                'office_id' => $officeId
            )
        );

        return $stmt->fetchAll();
    }

    public function paginate($dql, $page)
    {
        $paginator = new Paginator($dql);
        $paginator->getQuery()
            ->setFirstResult($this->pageSize * ($page - 1))
            ->setMaxResults($this->pageSize);

        return $paginator;
    }

    public function deleteByHistoric($historicId)
    {
        $qb = $this->createQueryBuilder('h');
        $qb->delete();
        $qb->where('h.historic = :historic_id');
        $qb->setParameter('historic_id', $historicId);

        return $qb->getQuery()->execute();
    }

    public function findByHistoricAndOffice($historic, $office)
    {
        $historic = $this->getEntityManager()
            ->createQuery(
                "SELECT h.id as historic_office_id,
                        h.accomplishment as historic_accomplishment,
                        h.createdAt as historic_created_at,
                        t.id as target_id,
                        t.name as target_name 
                            FROM RetoApiBundle:HistoricOffice h
                            LEFT JOIN h.target t
                            LEFT JOIN h.officereto o
                            WHERE h.historic = :historic
                            AND h.officereto = :office
                            ORDER BY t.bsIndicador ASC
                    "
            )
            ->setParameter('historic', $historic)
            ->setParameter('office', $office)
            ->getResult(Query::HYDRATE_ARRAY);

        return $historic;
    }

    public function findByHistoricHighLevel($historic, $target_id, $target_bs_indicator, $target_name)
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
            SELECT 1 as historic_office_id,
                    AVG(h.accomplishment) as historic_accomplishment,
                   null as historic_created_at
            FROM historic_office h
              LEFT JOIN target t on h.target_id = t.id
            WHERE h.historic_id = :historic and t.bs_indicador = :target_indicator
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute(array('target_indicator' => $target_bs_indicator, 'historic' => $historic));

        $result =  $stmt->fetchAll();
        $result = $result[0];

        $result['target_id'] = $target_id;
        $result['target_name'] = $target_name;

        return $result;
    }

    public function deleteAll()
    {
        $query = $this->getEntityManager()->createQuery('DELETE RetoApiBundle:HistoricOffice ho');
        $query->execute();
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:HistoricOffice p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function createOrUpdate($historicOffice)
    {
        $this->getEntityManager()->persist($historicOffice);
        //$this->getEntityManager()->flush();
    }
}