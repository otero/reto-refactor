<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RetoApiBundle\Entity\Interfaces\TargetMeanInterface;
use RetoApiBundle\Domain\Exception\Target\TargetMeanNotFoundException;
use RetoApiBundle\Repository\Interfaces\TargetMeanRepositoryInterface;

class TargetMeanRepository extends EntityRepository implements TargetMeanRepositoryInterface
{
    public function findAllByTarget($id)
    {
        return $this->findBy(array('target' => $id));
    }

    public function findOneByTargetMean($id)
    {
        return $this->find($id);
    }

    public function findOneByTargetMeanOrFail($id)
    {
        $targetMean = $this->findOneByTargetMean($id);

        if (!$targetMean instanceof TargetMeanInterface) {
            throw new TargetMeanNotFoundException();
        }

        return $targetMean;
    }
}
