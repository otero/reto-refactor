<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Entity\Interfaces\TargetInterface;
use RetoApiBundle\Repository\Interfaces\TargetRepositoryInterface;
use RetoApiBundle\Domain\Exception\Target\TargetNotFoundException;

class TargetRepository extends EntityRepository implements TargetRepositoryInterface
{
    public function findOneByBsIndicator($bsIndicator)
    {
        return $this->findOneBy(
            array('bsIndicador' => $bsIndicator)
        );
    }

    public function findOneByBsIndicatorOrFail($bsIndicator)
    {
        $target = $this->findOneByBsIndicator($bsIndicator);

        if (!$target instanceof TargetInterface) {
            throw new TargetNotFoundException();
        }

        return $target;
    }

    public function findAllOrderByBsIndicator()
    {
        $targets = $this->getEntityManager()
            ->createQuery(
                "SELECT t.id, t.name, t.bsIndicador 
                        FROM RetoApiBundle:Target t 
                        ORDER BY t.bsIndicador ASC"
            )
            ->getResult(Query::HYDRATE_ARRAY);

        return $targets;
    }

    public function createOrUpdate($target)
    {
        $this->getEntityManager()->persist($target);
        $this->getEntityManager()->flush();
    }
}
