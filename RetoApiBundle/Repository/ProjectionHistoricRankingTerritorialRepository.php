<?php

namespace RetoApiBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Entity\Interfaces\ProjectionRankingTerritorialInterface;
use RetoApiBundle\Entity\ProjectionHistoricRankingTerritorial;
use RetoApiBundle\Repository\Interfaces\ProjectionHistoricRankingTerritorialRepositoryInterface;
use RetoApiBundle\Domain\Exception\ProjectionRankingTerritorial\ProjectionRankingTerritorialException;

class ProjectionHistoricRankingTerritorialRepository extends EntityRepository implements ProjectionHistoricRankingTerritorialRepositoryInterface

{
    public function getComparative($territorial,$ranking,$officesTransformed,$cmp)
    {
        $historic = $this->findOneBy(array(
            'territorialId' => $territorial
        ));


        if($historic)
        {
            $historicRanking = $historic->getRanking();
            $historicOfficesTransformed = $historic->getOfficesTransform();
            $historicCMP = $historic->getCmpAnnual();

            if($ranking > $historicRanking)
            {
                $comparative["comparativeRanking"] = "increment";
            }
            else if ($ranking == $historicRanking)
            {
                $comparative["comparativeRanking"] = "equal";
            }
            else
            {
                $comparative["comparativeRanking"] = "decrement";
            }


            if($officesTransformed > $historicOfficesTransformed)
            {
                $comparative["comparativeOfficesTransform"] = "increment";
            }
            else if ($officesTransformed == $historicOfficesTransformed)
            {
                $comparative["comparativeOfficesTransform"] = "equal";
            }
            else
            {
                $comparative["comparativeOfficesTransform"] = "decrement";
            }

            if($cmp > $historicCMP)
            {
                $comparative["comparativeCmpAnnual"] = "increment";
            }
            else if ($cmp == $historicCMP)
            {
                $comparative["comparativeCmpAnnual"] = "equal";
            }
            else
            {
                $comparative["comparativeCmpAnnual"] = "decrement";
            }

        }

        else
        {
            $comparative["comparativeRanking"] = "equal";
            $comparative["comparativeOfficesTransformed"] = "equal";
            $comparative["comparativeCMP"] = "equal";

        }


        return($comparative);
    }

    public function deleteAll()
    {
        $this->getEntityManager()->createQuery('DELETE RetoApiBundle:ProjectionHistoricRankingTerritorial p')->execute();
    }

    public function backUp($data)
    {


    }

}
