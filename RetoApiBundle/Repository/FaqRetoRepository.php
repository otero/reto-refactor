<?php
/**
 * Created by PhpStorm.
 * User: Marc
 * Date: 18/07/2017
 * Time: 13:16
 */

namespace RetoApiBundle\Repository;


use Doctrine\ORM\EntityRepository;
use RetoApiBundle\Entity\FaqReto;

class FaqRetoRepository extends EntityRepository
{
    /**
     * ADMIN
     * @param $dataTableParams
     *
     * @return array
     */
    public function getDataTableInfo($dataTableParams)
    {
        // DQL Base
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select('f')
            ->from('RetoApiBundle:FaqReto', 'f');

        // data length, filtered or not, and before paging
        $cloneQuery = clone $query;
        $recordsFiltered = $cloneQuery->select('count(f.id)')->getQuery()->getSingleScalarResult();

        // paging
        $query
            ->setFirstResult($dataTableParams['start'])
            ->setMaxResults($dataTableParams['length']);

        $cols = array('f.id', 'f.question', 'f.position', 'f.hidden', 'f.highlight');
        if ($dataTableParams['order']) {
            $idxCol = $dataTableParams['order'][0]['column'];
            $dirOrder = $dataTableParams['order'][0]['dir'];

            $col = $cols[$idxCol];

            $query
                ->orderBy($col, $dirOrder);
        }

        $records = $query->getQuery()->getResult();

        // prepare output data
        $outputData = array();
        /** @var FaqReto $elem */
        foreach($records as $idx=>$elem) {
            $outputData[] = array(
                $elem->getId(),
                $elem->getQuestion(),
                $elem->getPosition(),
                $elem->isHidden(),
                $elem->isHighlight(),
            );
        }

        // output
        $response = array();

        $response['draw'] = $dataTableParams['draw'];
        $response['recordsTotal'] = count($this->findAll());
        $response['recordsFiltered'] = $recordsFiltered;
        $response['data'] = $outputData;

        return $response;
    }

    /**
     * Get all FAQs available to show on sidebar at home
     * @return array
     */
    public function findHomeFaqs()
    {
        // DQL Base
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select('f.id', 'f.question', 'f.position')
            ->from('RetoApiBundle:FaqReto', 'f')
            ->where('f.isHighlight = :highlight')
            ->andWhere('f.isHidden = :hidden')
            ->orderBy('f.position', 'ASC')
            ->setParameters(['highlight'=>true, 'hidden'=>false]);

        $records = $query->getQuery()->getResult();

        return $records;
    }

    /**
     * Get all FAQs available to show
     * @param bool $json_encode
     * @return string
     */
    public function findAllFaqs($json_encode = false)
    {
        // DQL Base
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select('f.id', 'f.question', 'f.answer', 'f.imageUrl', 'f.mediaUrl', 'f.position')
            ->from('RetoApiBundle:FaqReto', 'f')
            ->where('f.isHidden = :hidden')
            ->orderBy('f.position', 'ASC')
            ->setParameter('hidden', false);

        $records = $query->getQuery()->getResult();

        return $records;
    }
}