<?php

namespace RetoApiBundle\Repository;


use Doctrine\ORM\EntityRepository;
use RetoApiBundle\Entity\Territorial;
use RetoApiBundle\Entity\Interfaces\TerritorialInterface;
use RetoApiBundle\Repository\Interfaces\TerritorialRepositoryInterface;
use RetoApiBundle\Domain\Exception\Territorial\TerritorialNotFoundException;

class TerritorialRepository extends EntityRepository implements TerritorialRepositoryInterface
{
    public function getDataTableInfo($dataTableParams)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select('t')
            ->from('RetoApiBundle:Territorial', 't');

        $cloneQuery = clone $query;
        $recordsFiltered = $cloneQuery->select('count(t.id)')->getQuery()->getSingleScalarResult();

        $query
            ->setFirstResult($dataTableParams['start'])
            ->setMaxResults($dataTableParams['length']);

        $cols = array('t.id', 't.name', 't.bancsabadellId', 't.bank');
        if ($dataTableParams['order']) {
            $idxCol = $dataTableParams['order'][0]['column'];
            $dirOrder = $dataTableParams['order'][0]['dir'];

            $col = $cols[$idxCol];

            $query
                ->orderBy($col, $dirOrder);
        }

        $records = $query->getQuery()->getResult();

        $outputData = array();
        /** @var Territorial $elem */
        foreach($records as $idx=>$elem) {
            $outputData[] = array(
                $elem->getId(),
                $elem->getName(),
                $elem->getBancSabadellId(),
                (string)$elem->getBank()
            );
        }

        $response['draw'] = $dataTableParams['draw'];
        $response['recordsTotal'] = count($this->findAll());
        $response['recordsFiltered'] = $recordsFiltered;
        $response['data'] = $outputData;

        return $response;
    }

    public function findOneByBankSabadellId($id)
    {
        return $this->findOneBy(['bancsabadellId' => $id]);
    }

    public function findOneByBankSabadellIdOrFail($id)
    {
        $territorial = $this->findOneByBankSabadellId($id);

        if (!$territorial instanceof TerritorialInterface) {
            throw new TerritorialNotFoundException();
        }

        return $territorial;
    }

    public function getTerritorials()
    {
        $territorials = $this->getEntityManager()
            ->createQuery(
                "SELECT t.id, t.name FROM RetoApiBundle:Territorial t ORDER BY t.name ASC"
            )
            ->getArrayResult();

        return $territorials;
    }

    public function createOrUpdate($territorial)
    {
        $this->getEntityManager()->persist($territorial);
        $this->getEntityManager()->flush();
    }
}
