<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RetoApiBundle\Domain\Exception\Regional\RegionalNotFoundException;
use RetoApiBundle\Entity\Interfaces\RegionalInterface;
use RetoApiBundle\Repository\Interfaces\RegionalRepositoryInterface;

class RegionalRepository extends EntityRepository implements RegionalRepositoryInterface
{
    public function getDataTableInfo($dataTableParams)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select('r')
            ->from('RetoApiBundle:Regional', 'r');

        $cloneQuery = clone $query;
        $recordsFiltered = $cloneQuery->select('count(r.id)')->getQuery()->getSingleScalarResult();

        $query
            ->setFirstResult($dataTableParams['start'])
            ->setMaxResults($dataTableParams['length']);

        $cols = array('r.id', 'r.name', 'r.bancsabadellId', 'r.red', 'r.territorial', 'r.bank');
        if ($dataTableParams['order']) {
            $idxCol = $dataTableParams['order'][0]['column'];
            $dirOrder = $dataTableParams['order'][0]['dir'];

            $col = $cols[$idxCol];

            $query
                ->orderBy($col, $dirOrder);
        }

        $records = $query->getQuery()->getResult();

        $outputData = array();

        foreach($records as $idx=>$elem) {
            $outputData[] = array(
                $elem->getId(),
                $elem->getName(),
                $elem->getBancSabadellId(),
                (string)$elem->getRed(),
                (string)$elem->getRed()->getTerritorial(),
                (string)$elem->getRed()->getTerritorial()->getBank(),
            );
        }
        $response['draw'] = $dataTableParams['draw'];
        $response['recordsTotal'] = count($this->findAll());
        $response['recordsFiltered'] = $recordsFiltered;
        $response['data'] = $outputData;

        return $response;
    }

    public function findOneByBankSabadellId($id)
    {
        return $this->findOneBy(['bancsabadellId' => $id]);
    }

    public function findOneByBankSabadellIdOrFail($id)
    {
        $regional = $this->findOneByBankSabadellId($id);

        if (!$regional instanceof RegionalInterface) {

            throw new RegionalNotFoundException();
        }

        return $regional;
    }

    public function findAllRegional()
    {
        $regional = $this->getEntityManager()
            ->createQuery(
                "SELECT r FROM RetoApiBundle:Regional r"
            )
            ->getArrayResult();

        return $regional;
    }

    public function getRegional($red)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('r.id, r.name');
        $qb->from('RetoApiBundle:Regional', 'r');
        $qb->leftJoin('r.red', 're');
        $qb->orderBy('r.name', 'ASC');

        if (count($red) > 0) {
            $qb->andWhere('re.id IN (:red)');
            $qb->setParameter('red', $red);
        }

        return $qb->getQuery()->getArrayResult();
    }

    public function getTerritorialRedRegionalEstructure()
    {
        $estructure = $this->getEntityManager()
            ->createQuery(
                "SELECT t.id as idTerritorial, t.name as nameTerritorial,r.id as idRed,r.name as nameRed,reg.id as idRegional,reg.name as nameRegional FROM RetoApiBundle:Regional reg JOIN reg.red r JOIN r.territorial t  ORDER BY t.name,r.name,reg.name ASC"
            )
            ->getArrayResult();
        //var_dump($estructure);die;
        return $estructure;
    }


    public function createOrUpdate($regional)
    {
        $this->getEntityManager()->persist($regional);
        $this->getEntityManager()->flush();
    }
}
