<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Entity\Interfaces\HistoricTerritorialAccumulatedInterface;
use RetoApiBundle\Repository\Interfaces\HistoricTerritorialAccumulatedRepositoryInterface;
use RetoApiBundle\Domain\Exception\HistoricTerritorialAccumulated\HistoricTerritorialAccumulatedNotFoundException;

class HistoricTerritorialAccumulatedRepository extends EntityRepository implements HistoricTerritorialAccumulatedRepositoryInterface
{
    public function findOneByBankSabadellIdAndChallenge($bankSabadellId, $challenge)
    {
       return $this->findOneBy(array(
        'bancsabadellId' => $bankSabadellId,
        'challenge' => $challenge
        ));
    }

    public function findOneByBankSabadellIdAndChallengeOrFail($bankSabadellId, $challenge)
    {
        $historicTerritorialAccumulated = $this
            ->findOneByBankSabadellIdAndChallenge($bankSabadellId, $challenge);

        if (!$historicTerritorialAccumulated instanceof HistoricTerritorialAccumulatedInterface) {
            throw new HistoricTerritorialAccumulatedNotFoundException();
        }

        return $historicTerritorialAccumulated;
    }

    public function findRanking()
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
            SELECT h.id as historic_territorial_accumulated_id,
                   h.anual as historic_territorial_accumulated_annual,
                   h.trim1 as historic_territorial_accumulated_trim1,
                   h.trim2 as historic_territorial_accumulated_trim2,
                   h.trim3 as historic_territorial_accumulated_trim3,
                   h.trim4 as historic_territorial_accumulated_trim4,
                   t.id as territorial_id,
                   t.name as territorial_name,
                   DENSE_RANK() OVER (order by h.anual DESC) AS ranking
            FROM historic_territorial_accumulated as h
                   LEFT JOIN territorial t on t.id = h.territorial_id
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:HistoricTerritorialAccumulated p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function deleteAll()
    {
        $query = $this->getEntityManager()->createQuery('DELETE RetoApiBundle:HistoricTerritorialAccumulated hr');
        $query->execute();
    }

    public function createOrUpdate($historicTerritorialAccumulated)
    {
        $this->getEntityManager()->persist($historicTerritorialAccumulated);
        //$this->getEntityManager()->flush();
    }
}
