<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RetoApiBundle\Entity\Bank;

class BankRepository extends EntityRepository
{
    public function getDataTableInfo($dataTableParams)
    {
        // DQL Base
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select('b')
            ->from('RetoApiBundle:Bank', 'b');

        // data length, filtered or not, and before paging
        $cloneQuery = clone $query;
        $recordsFiltered = $cloneQuery->select('count(b.id)')->getQuery()->getSingleScalarResult();

        // paging
        $query
            ->setFirstResult($dataTableParams['start'])
            ->setMaxResults($dataTableParams['length']);

        $cols = array('b.id', 'b.name', 'b.bancsabadellId');
        if ($dataTableParams['order']) {
            $idxCol = $dataTableParams['order'][0]['column'];
            $dirOrder = $dataTableParams['order'][0]['dir'];

            $col = $cols[$idxCol];

            $query
                ->orderBy($col, $dirOrder);
        }

        $records = $query->getQuery()->getResult();

        // prepare output data
        $outputData = array();
        /** @var Bank $elem */
        foreach($records as $idx=>$elem) {
            $outputData[] = array(
                $elem->getId(),
                $elem->getName(),
                $elem->getBancSabadellId(),
            );
        }

        // output
        $response = array();

        $response['draw'] = $dataTableParams['draw'];
        $response['recordsTotal'] = count($this->findAll());
        $response['recordsFiltered'] = $recordsFiltered;
        $response['data'] = $outputData;

        return $response;
    }
}
