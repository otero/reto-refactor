<?php

namespace RetoApiBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Entity\Interfaces\ProjectionRankingRedInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingRedRepositoryInterface;
use RetoApiBundle\Domain\Exception\ProjectionRankingRed\ProjectionRankingRedException;

class ProjectionRankingRedRepository extends EntityRepository implements ProjectionRankingRedRepositoryInterface
{

    public function findOneByHistoricRedAccumulated($historicRedAccumulated)
    {
        return $this->findOneBy(array(
            'historicRedAccumulatedId' => $historicRedAccumulated
        ));
    }

    public function findOneByHistoricRedAccumulatedOrFail($historicRedAccumulated)
    {
        $projectionRankingRed = $this->findOneByHistoricRedAccumulated($historicRedAccumulated);

        if (!$projectionRankingRed instanceof ProjectionRankingRedInterface) {

            throw new ProjectionRankingRedException();
        }

        return $projectionRankingRed;
    }

    public function findByFilters($territorial)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('p');
        $qb->from('RetoApiBundle:ProjectionRankingRed', 'p');

        if (count($territorial) > 0) {
            $qb->andWhere('p.territorialId IN (:territorial)');
            $qb->setParameter('territorial', $territorial);
        }

        $qb->orderBy('p.rankingOfficesTransform', 'ASC');

        return $qb->getQuery()->getArrayResult();
    }

    public function findOneByRed($red)
    {
        return $this->getEntityManager()
            ->createQuery(
                "
                    SELECT p FROM RetoApiBundle:ProjectionRankingRed p 
                              WHERE p.redId = :red
                "
            )
            ->setParameter('red', $red)
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function findRankingByOfficeTransform()
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
           SELECT p.id,
                   p.historic_red_accumulated_id,
                   p.offices_transform,
                   p.ranking_offices_transform,
                   DENSE_RANK() OVER (order by  p.offices_transform DESC, p.cmp_annual DESC) AS ranking
            FROM projection_ranking_red p
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function deleteAll()
    {
        $this->getEntityManager()->createQuery('DELETE RetoApiBundle:ProjectionRankingRed p')->execute();
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:ProjectionRankingRed p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function createOrUpdate($projectionRankingRed)
    {
        $this->getEntityManager()->persist($projectionRankingRed);
        //$this->getEntityManager()->flush();
    }
}
