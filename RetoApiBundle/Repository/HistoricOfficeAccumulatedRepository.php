<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Entity\Interfaces\HistoricOfficeAccumulatedInterface;
use RetoApiBundle\Repository\Interfaces\HistoricOfficeAccumulatedRepositoryInterface;
use RetoApiBundle\Domain\Exception\HistoricOfficeRetoAccumulated\HistoricOfficeRetoAccumulatedNotFoundException;

class HistoricOfficeAccumulatedRepository extends EntityRepository implements HistoricOfficeAccumulatedRepositoryInterface
{
    public function findOneById($id)
    {
        return $this->find($id);
    }

    public function findOneByBankSabadellIdAndChallenge($bankSabadellId, $challenge)
    {
        return $this->findOneBy(array(
            'bancsabadellId' =>$bankSabadellId,
            'challenge' => $challenge
        ));
    }

    public function findOneByBankSabadellIdAndChallengeOrFail($bankSabadellId, $challenge)
    {
        $historicOfficeAccumulated = $this
            ->findOneByBankSabadellIdAndChallenge($bankSabadellId, $challenge);

        if (!$historicOfficeAccumulated instanceof HistoricOfficeAccumulatedInterface) {
            throw new HistoricOfficeRetoAccumulatedNotFoundException();
        }

        return $historicOfficeAccumulated;
    }

    public function findRankingByRed($red)
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
            SELECT h.id, h.anual,
                   DENSE_RANK() OVER (order by h.anual DESC) AS ranking
            FROM historic_office_accumulated as h
            LEFT JOIN office_reto o on h.office_id = o.id
            LEFT JOIN regional r on o.regional_id = r.id
            LEFT JOIN red r2 on r.red_id = r2.id
            WHERE r2.id = :red
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute(array('red' => $red));

        return $stmt->fetchAll();
    }

    public function findTop1RankingAllReds($red)
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
            SELECT h.id, h.anual,
                   DENSE_RANK() OVER (order by h.anual DESC) AS ranking
            FROM historic_office_accumulated as h
            LEFT JOIN office_reto o on h.office_id = o.id
            LEFT JOIN regional r on o.regional_id = r.id
            LEFT JOIN red r2 on r.red_id = r2.id
            WHERE r2.id = :red
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute(array('red' => $red));

        return $stmt->fetchAll();
    }

    public function findTop1Ranking()
    {
        $conn = $this->getEntityManager()
            ->getConnection();

        $sql = "
            SELECT TOP 1
                  h.id as historic_office_accumulated_id,
                   h.anual as historic_office_accumulated_annual,
                   h.trim1 as historic_office_accumulated_trim1,
                   h.trim2 as historic_office_accumulated_trim2,
                   h.trim3 as historic_office_accumulated_trim3,
                   h.trim4 as historic_office_accumulated_trim4,
                   o.id as office_id,
                   o.name as office_name,
                   r.id as regional_id,
                   r.name as regional_name,
                   r2.id as red_id,
                   r2.name as red_name,
                   t.id as territorial_id,
                   t.name as territorial_name,
                   DENSE_RANK() OVER (order by h.anual DESC) AS ranking
            
            FROM historic_office_accumulated as h
                   LEFT JOIN office_reto o on h.office_id = o.id
                   LEFT JOIN regional r on o.regional_id = r.id
                   LEFT JOIN red r2 on r.red_id = r2.id
                   LEFT JOIN territorial t on t.id = r2.territorial_id
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function findRankingByRedLessPlatinum($red)
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
            SELECT h.id as historic_office_accumulated_id,
                   h.anual as historic_office_accumulated_annual,
                   h.trim1 as historic_office_accumulated_trim1,
                   h.trim2 as historic_office_accumulated_trim2,
                   h.trim3 as historic_office_accumulated_trim3,
                   h.trim4 as historic_office_accumulated_trim4,
                   o.id as office_id,
                   o.name as office_name,
                   r.id as regional_id,
                   r.name as regional_name,
                   r2.id as red_id,
                   r2.name as red_name,
                   t.id as territorial_id,
                   t.name as territorial_name,
                   DENSE_RANK() OVER (order by h.anual DESC) AS ranking
            
            FROM historic_office_accumulated as h
                   LEFT JOIN office_reto o on h.office_id = o.id
                   LEFT JOIN regional r on o.regional_id = r.id
                   LEFT JOIN red r2 on r.red_id = r2.id
                   LEFT JOIN territorial t on t.id = r2.territorial_id
            WHERE r2.id = :red
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute(array('red' => $red));

        return $stmt->fetchAll();
    }

    public function deleteAll()
    {
        $query = $this->getEntityManager()->createQuery('DELETE RetoApiBundle:HistoricOfficeAccumulated hoa');
        $query->execute();
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:HistoricOfficeAccumulated p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function createOrUpdate($historicOfficeAccumulated)
    {
        $this->getEntityManager()->persist($historicOfficeAccumulated);
        //$this->getEntityManager()->flush();
    }
}
