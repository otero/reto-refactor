<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\Interfaces\HistoricInterface;
use RetoApiBundle\Entity\Interfaces\RegionalInterface;
use RetoApiBundle\Entity\Regional;
use RetoApiBundle\Repository\Interfaces\HistoricRegionalRepositoryInterface;

class HistoricRegionalRepository extends EntityRepository implements HistoricRegionalRepositoryInterface
{
    public function findOneByHistoricAndTarget($historic, $target, $regional)
    {
        $historicRegional = $this->getEntityManager()
            ->createQuery(
                "SELECT p FROM RetoApiBundle:HistoricRegional p 
                    LEFT JOIN p.historic h
                    LEFT JOIN p.target t
                    LEFT JOIN p.regional r
                    WHERE h.id = :historic_id AND t.id = :target_id AND r.id = :regional_id
                "
            )
            ->setParameter('historic_id', $historic->getId())
            ->setParameter('target_id', $target->getId())
            ->setParameter('regional_id', $regional->getId())
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);

        return $historicRegional;
    }

    public function getAccomplishments(HistoricInterface $historic, RegionalInterface $regional)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "select t.name, accomplishment
                    from historic_regional hr
                      join historic h on hr.historic_id = h.id
                      join target t on hr.target_id = t.id
                    where
                      h.id = :historic_id
                      and hr.regional_id = :regional_id";

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            array(
                'historic_id' => $historic->getId(),
                'regional_id' => $regional->getId()
            )
        );

        return $stmt->fetchAll();
    }

    public function getAccomplishmentTrim(HistoricInterface $historic, RegionalInterface $regional)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "select accomplishment
                    from historic_regional_trim hr
                    where
                      hr.historic_id = :historic_id
                      and hr.regional_id = :regional_id";

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            array(
                'historic_id' => $historic->getId(),
                        'regional_id' => $regional->getId()
            )
        );
        return $stmt->fetchColumn(0);
    }

    public function deleteByHistoric($historicId)
    {
        $qb = $this->createQueryBuilder('h');
        $qb->delete();
        $qb->where('h.historic = :historic_id');
        $qb->setParameter('historic_id', $historicId);

        return $qb->getQuery()->execute();
    }

    public function deleteAll()
    {
        $query = $this->getEntityManager()->createQuery('DELETE RetoApiBundle:HistoricRegional hr');
        $query->execute();
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:HistoricRegional p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function createOrUpdate($historicRegional)
    {
        $this->getEntityManager()->persist($historicRegional);
        //$this->getEntityManager()->flush();
    }
}
