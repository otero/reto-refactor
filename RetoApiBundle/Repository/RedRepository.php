<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RetoApiBundle\Entity\Interfaces\RedInterface;
use RetoApiBundle\Domain\Exception\Red\RedNotFoundException;
use RetoApiBundle\Repository\Interfaces\RedRepositoryInterface;

class RedRepository extends EntityRepository implements RedRepositoryInterface
{
    public function getDataTableInfo($dataTableParams)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select('r')
            ->from('RetoApiBundle:Red', 'r');

        $cloneQuery = clone $query;
        $recordsFiltered = $cloneQuery->select('count(r.id)')->getQuery()->getSingleScalarResult();

        $query
            ->setFirstResult($dataTableParams['start'])
            ->setMaxResults($dataTableParams['length']);

        $cols = array('r.id', 'r.name', 'r.bancsabadellId', 'r.territorial', 'r.bank');
        if ($dataTableParams['order']) {
            $idxCol = $dataTableParams['order'][0]['column'];
            $dirOrder = $dataTableParams['order'][0]['dir'];

            $col = $cols[$idxCol];

            $query
                ->orderBy($col, $dirOrder);
        }

        $records = $query->getQuery()->getResult();

        $outputData = array();

        foreach($records as $idx=>$elem) {
            $outputData[] = array(
                $elem->getId(),
                $elem->getName(),
                $elem->getBancSabadellId(),
                (string)$elem->getTerritorial(),
                (string)$elem->getTerritorial()->getBank(),
            );
        }

        $response['draw'] = $dataTableParams['draw'];
        $response['recordsTotal'] = count($this->findAll());
        $response['recordsFiltered'] = $recordsFiltered;
        $response['data'] = $outputData;

        return $response;
    }

    public function findOneByBankSabadellId($id)
    {
        return $this->findOneBy(['bancsabadellId' => $id]);
    }

    public function findAllReds()
    {
        $reds = $this->getEntityManager()
            ->createQuery(
                "SELECT r FROM RetoApiBundle:Red r"
            )
            ->getArrayResult();

        return $reds;
    }

    public function findOneByBankSabadellIdOrFail($id)
    {
        $red = $this->findOneByBankSabadellId($id);

        if (!$red instanceof RedInterface) {

            throw new RedNotFoundException();
        }

        return $red;
    }

    public function getReds($territorial)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('r.id, r.name');
        $qb->from('RetoApiBundle:Red', 'r');
        $qb->leftJoin('r.territorial', 't');
        $qb->orderBy('r.name', 'ASC');

        if (count($territorial) > 0) {
            $qb->andWhere('t.id IN (:territorial)');
            $qb->setParameter('territorial', $territorial);
        }

        return $qb->getQuery()->getArrayResult();
    }

    public function createOrUpdate($red)
    {
        $this->getEntityManager()->persist($red);
        $this->getEntityManager()->flush();
    }
}
