<?php

namespace RetoApiBundle\Repository\Interfaces;

interface ProjectionRankingRegionalRepositoryInterface
{
    public function findByFilters($red, $territorial);

    public function findOneByHistoricRegionalAccumulated($historicRegionalAccumulated);

    public function findOneByHistoricRegionalAccumulatedOrFail($historicRegionalAccumulated);

    public function findOneByRegional($regional);

    public function findRankingByOfficeTransform();

    public function deleteAll();

    public function countAll();

    public function createOrUpdate($projectionRankingRegional);
}