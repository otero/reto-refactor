<?php

namespace RetoApiBundle\Repository\Interfaces;


interface HistoricRepositoryInterface
{
    public function findCurrent();

    public function findPrevious();

    public function findOneByProcessedOrFail();

    public function findOneByYearAndMonth($challenge, $year, $month);

    public function deleteAll();

    public function createOrUpdate($historic);
}