<?php

namespace RetoApiBundle\Repository\Interfaces;


use RetoApiBundle\Entity\Interfaces\HistoricInterface;
use RetoApiBundle\Entity\Interfaces\RegionalInterface;

interface HistoricRegionalRepositoryInterface
{
    public function findOneByHistoricAndTarget($historic, $target, $regional);

    public function getAccomplishmentTrim(HistoricInterface $historic, RegionalInterface $regional);

    public function getAccomplishments(HistoricInterface $historic, RegionalInterface $regional);

    public function deleteByHistoric($historicId);

    public function deleteAll();

    public function countAll();

    public function createOrUpdate($historicTerritorial);
}