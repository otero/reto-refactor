<?php

namespace RetoApiBundle\Repository\Interfaces;

interface ProjectionRankingRedRepositoryInterface
{
    public function findOneByHistoricRedAccumulated($historicRedAccumulated);

    public function findOneByHistoricRedAccumulatedOrFail($historicRedAccumulated);

    public function findByFilters($territorial);

    public function findOneByRed($red);

    public function findRankingByOfficeTransform();

    public function deleteAll();

    public function countAll();

    public function createOrUpdate($projectionRankingRed);
}