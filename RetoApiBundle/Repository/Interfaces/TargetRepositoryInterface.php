<?php

namespace RetoApiBundle\Repository\Interfaces;

interface TargetRepositoryInterface
{
    public function findOneByBsIndicator($bsIndicator);

    public function findOneByBsIndicatorOrFail($bsIndicator);

    public function findAllOrderByBsIndicator();

    public function createOrUpdate($target);
}

