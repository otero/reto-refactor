<?php

namespace RetoApiBundle\Repository\Interfaces;

interface OfficeRetoRepositoryInterface
{
    public function findOneByBankSabadellId($id);

    public function getDataTableInfo($dataTableParams);

    public function createOrUpdate($territorial);

    public function getOffices();

    public function findOneByBankSabadellIdOrFail($id);
}