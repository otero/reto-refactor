<?php

namespace RetoApiBundle\Repository\Interfaces;

interface HistoricRedAccumulatedRepositoryInterface
{
    public function findOneByBankSabadellIdAndChallenge($bankSabadellId, $challenge);

    public function findOneByBankSabadellIdAndChallengeOrFail($bankSabadellId, $challenge);

    public function findRanking();

    public function deleteAll();

    public function countAll();

    public function createOrUpdate($historicTerritorialAccumulated);
}