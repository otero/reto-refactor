<?php

namespace RetoApiBundle\Repository\Interfaces;


interface HistoricRedRepositoryInterface
{
    public function findOneByHistoricAndTarget($historic, $target, $red);

    public function deleteByHistoric($historicId);

    public function deleteAll();

    public function countAll();

    public function createOrUpdate($historicTerritorial);
}