<?php

namespace RetoApiBundle\Repository\Interfaces;

interface ProjectionRankingTerritorialRepositoryInterface
{
    public function findOneByHistoricTerritorialAccumulated($historicTerritorialAccumulated);

    public function findOneByHistoricTerritorialAccumulatedOrFail($historicTerritorialAccumulated);

    public function findByFilters();

    public function findOneByRed($territorial);

    public function findRankingByOfficeTransform();

    public function deleteAll();

    public function countAll();

    public function createOrUpdate($projectionRankingTerritorial);
}