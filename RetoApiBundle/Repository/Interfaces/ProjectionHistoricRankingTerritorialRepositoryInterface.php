<?php

namespace RetoApiBundle\Repository\Interfaces;

interface ProjectionHistoricRankingTerritorialRepositoryInterface
{
    public function getComparative($territorial,$ranking,$officesTransformed,$cmp);
    public function deleteAll();
    public function backUp($data);


}