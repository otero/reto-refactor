<?php

namespace RetoApiBundle\Repository\Interfaces;


interface HistoricLogRepositoryInterface
{
    public function createOrUpdate($historicLog);
}
