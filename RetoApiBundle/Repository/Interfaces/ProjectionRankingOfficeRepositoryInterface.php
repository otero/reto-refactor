<?php

namespace RetoApiBundle\Repository\Interfaces;

interface ProjectionRankingOfficeRepositoryInterface
{
    public function findByHistoricOfficeAccumulated($historicOfficeAccumulated);

    public function findByHistoricOfficeAccumulatedOrFail($historicOfficeAccumulated);

    public function createOrUpdate($projectionRankingOffice);

    public function findOfficesTransformByRed($red);

    public function findOfficesTransformByTerritorial($territorial);

    public function findByFilters($regional, $red, $territorial, $limit);

    public function deleteAll();

    public function countAll();

    public function findMedalsByRegional($regional);

    public function findOneByCmpAnnual($sortOrder);
}