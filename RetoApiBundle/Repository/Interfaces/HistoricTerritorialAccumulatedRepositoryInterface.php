<?php

namespace RetoApiBundle\Repository\Interfaces;

interface HistoricTerritorialAccumulatedRepositoryInterface
{
    public function findRanking();

    public function findOneByBankSabadellIdAndChallenge($bankSabadellId, $challenge);

    public function findOneByBankSabadellIdAndChallengeOrFail($bankSabadellId, $challenge);

    public function deleteAll();

    public function countAll();

    public function createOrUpdate($historicTerritorialAccumulated);
}