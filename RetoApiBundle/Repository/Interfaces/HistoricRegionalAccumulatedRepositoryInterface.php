<?php

namespace RetoApiBundle\Repository\Interfaces;

interface HistoricRegionalAccumulatedRepositoryInterface
{
    public function findOneByRegional($regional);

    public function findOneByBankSabadellIdAndChallenge($bankSabadellId, $challenge);

    public function findOneByBankSabadellIdAndChallengeOrFail($bankSabadellId, $challenge);

    public function createOrUpdate($historicRegionalAccumulated);

    public function deleteAll();

    public function countAll();

    public function findRankingByRegional();
}