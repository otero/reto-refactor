<?php

namespace RetoApiBundle\Repository\Interfaces;

interface HistoricOfficeAccumulatedRepositoryInterface
{
    public function findOneById($id);

    public function findOneByBankSabadellIdAndChallenge($bankSabadellId, $challenge);

    public function findOneByBankSabadellIdAndChallengeOrFail($bankSabadellId, $challenge);

    public function findRankingByRed($red);

    public function findTop1Ranking();

    public function findRankingByRedLessPlatinum($red);

    public function deleteAll();

    public function countAll();

    public function createOrUpdate($historicOfficeAccumulated);
}