<?php

namespace RetoApiBundle\Repository\Interfaces;


interface HistoricTerritorialRepositoryInterface
{
    public function findOneByHistoricAndTarget($historic, $target, $office);

    public function deleteByHistoric($historicId);

    public function deleteAll();

    public function countAll();

    public function createOrUpdate($historicTerritorial);
}