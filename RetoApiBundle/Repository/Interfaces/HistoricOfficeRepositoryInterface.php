<?php

namespace RetoApiBundle\Repository\Interfaces;


interface HistoricOfficeRepositoryInterface
{
    public function findOneByHistoricAndTarget($historic, $target, $office);

    public function findOrderedByTargetAccomplishment($targetId, $historicId, $page);

    public function getOfficeTargetAccomplishments($historicId, $officeId);

    public function paginate($dql, $page);

    public function findByHistoricHighLevel($historic, $target_id, $target_bs_indicator, $target_name);

    public function deleteByHistoric($historicId);

    public function findByHistoricAndOffice($historic, $office);

    public function createOrUpdate($historicTerritorial);

    public function countAll();

    public function deleteAll();
}