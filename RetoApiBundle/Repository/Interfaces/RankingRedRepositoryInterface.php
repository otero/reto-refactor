<?php

namespace RetoApiBundle\Repository\Interfaces;

interface RankingRedRepositoryInterface
{
    public function findByHistoricOfficeAccumulated($historicOfficeAccumulated);

    public function findByHistoricOfficeAccumulatedOrFail($historicOfficeAccumulated);

    public function createOrUpdate($rankingRed);

    public function findOneRankingByOffice($office);

    public function deleteAll();

    public function countAll();

    public function findTopFiveByRed($red);
}