<?php

namespace RetoApiBundle\Repository\Interfaces;


interface ChallengeRepositoryInterface
{
    public function findCurrent();

    public function findOneByActiveOrFail();

    public function findOneByYearAndActive($year);

    public function findOneByYearAndActiveOrFail($year);
}