<?php

namespace RetoApiBundle\Repository\Interfaces;

interface TargetMeanRepositoryInterface
{
    public function findAllByTarget($id);

    public function findOneByTargetMean($id);

    public function findOneByTargetMeanOrFail($id);
}

