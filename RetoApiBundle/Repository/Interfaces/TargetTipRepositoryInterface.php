<?php

namespace RetoApiBundle\Repository\Interfaces;

interface TargetTipRepositoryInterface
{
    public function findAllByTarget($id);

    public function findOneByTargetTip($id);

    public function findOneByTargetTipOrFail($id);
}

