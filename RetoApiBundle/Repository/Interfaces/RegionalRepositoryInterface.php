<?php

namespace RetoApiBundle\Repository\Interfaces;

interface RegionalRepositoryInterface
{
    public function findOneByBankSabadellId($id);

    public function getDataTableInfo($dataTableParams);

    public function createOrUpdate($territorial);

    public function findOneByBankSabadellIdOrFail($id);

    public function getRegional($red);

    public function findAllRegional();
}