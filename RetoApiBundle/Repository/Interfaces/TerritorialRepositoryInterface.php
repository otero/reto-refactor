<?php

namespace RetoApiBundle\Repository\Interfaces;

interface TerritorialRepositoryInterface
{
    public function findOneByBankSabadellId($id);

    public function getDataTableInfo($dataTableParams);

    public function createOrUpdate($territorial);

    public function getTerritorials();

    public function findOneByBankSabadellIdOrFail($id);
}