<?php

namespace RetoApiBundle\Repository\Interfaces;

interface RedRepositoryInterface
{
    public function findOneByBankSabadellId($id);

    public function getDataTableInfo($dataTableParams);

    public function createOrUpdate($territorial);

    public function findOneByBankSabadellIdOrFail($id);

    public function getReds($territorial);

    public function findAllReds();
}

