<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Repository\Interfaces\HistoricTerritorialRepositoryInterface;


class HistoricTerritorialRepository extends EntityRepository implements HistoricTerritorialRepositoryInterface
{

    public function findOneByHistoricAndTarget($historic, $target, $territorial)
    {
        $historicTerritorial = $this->getEntityManager()
            ->createQuery(
                "SELECT p FROM RetoApiBundle:HistoricTerritorial p 
                    LEFT JOIN p.historic h
                    LEFT JOIN p.target t
                    LEFT JOIN p.territorial te
                    WHERE h.id = :historic_id AND t.id = :target_id AND te.id = :territorial_id
                "
            )
            ->setParameter('historic_id', $historic->getId())
            ->setParameter('target_id', $target->getId())
            ->setParameter('territorial_id', $territorial->getId())
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);

        return $historicTerritorial;
    }

    public function deleteByHistoric($historicId)
    {
        $qb = $this->createQueryBuilder('h');
        $qb->delete();
        $qb->where('h.historic = :historic_id');
        $qb->setParameter('historic_id', $historicId);

        return $qb->getQuery()->execute();
    }

    public function deleteAll()
    {
        $query = $this->getEntityManager()->createQuery('DELETE RetoApiBundle:HistoricTerritorial hr');
        $query->execute();
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:HistoricTerritorial p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function createOrUpdate($historicTerritorial)
    {
        $this->getEntityManager()->persist($historicTerritorial);
        //$this->getEntityManager()->flush();
    }
}
