<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Domain\Exception\HistoricRedAccumulated\HistoricRedAccumulatedNotFoundException;
use RetoApiBundle\Entity\Interfaces\HistoricRedAccumulatedInterface;
use RetoApiBundle\Repository\Interfaces\HistoricRedAccumulatedRepositoryInterface;

class HistoricRedAccumulatedRepository extends EntityRepository implements HistoricRedAccumulatedRepositoryInterface
{
    public function findOneByBankSabadellIdAndChallenge($bankSabadellId, $challenge)
    {
        return $this->findOneBy(array(
            'bancsabadellId' =>$bankSabadellId,
            'challenge' => $challenge
        ));
    }

    public function findOneByBankSabadellIdAndChallengeOrFail($bankSabadellId, $challenge)
    {
        $historicRedAccumulated = $this
            ->findOneByBankSabadellIdAndChallenge($bankSabadellId, $challenge);

        if (!$historicRedAccumulated instanceof HistoricRedAccumulatedInterface) {
            throw new HistoricRedAccumulatedNotFoundException();
        }

        return $historicRedAccumulated;
    }

    public function findRanking()
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
            SELECT h.id as historic_red_accumulated_id,
                   h.anual as historic_red_accumulated_annual,
                   h.trim1 as historic_red_accumulated_trim1,
                   h.trim2 as historic_red_accumulated_trim2,
                   h.trim3 as historic_red_accumulated_trim3,
                   h.trim4 as historic_red_accumulated_trim4,
                   r2.id as red_id,
                   r2.name as red_name,
                   t.id as territorial_id,
                   t.name as territorial_name,
                   DENSE_RANK() OVER (order by h.anual DESC) AS ranking
            
            FROM historic_red_accumulated as h
                   LEFT JOIN red r2 on h.red_id = r2.id
                   LEFT JOIN territorial t on t.id = r2.territorial_id
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function deleteAll()
    {
        $query = $this->getEntityManager()->createQuery('DELETE RetoApiBundle:HistoricRedAccumulated hr');
        $query->execute();
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:HistoricRed p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function createOrUpdate($historicRedAccumulated)
    {
        $this->getEntityManager()->persist($historicRedAccumulated);
        //$this->getEntityManager()->flush();
    }
}
