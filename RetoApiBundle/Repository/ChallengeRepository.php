<?php

namespace RetoApiBundle\Repository;


use Doctrine\ORM\EntityRepository;
use RetoApiBundle\Domain\Exception\Challenge\ChallengeNotFoundException;
use RetoApiBundle\Entity\Interfaces\ChallengeInterface;
use RetoApiBundle\Repository\Interfaces\ChallengeRepositoryInterface;

class ChallengeRepository extends EntityRepository implements ChallengeRepositoryInterface
{
    public function findCurrent()
    {
        return $this->findOneBy(['active' => true]);
    }

    public function findOneByActiveOrFail()
    {
        $challenge = $this->findCurrent();

        if (!$challenge instanceof ChallengeInterface) {

            throw new ChallengeNotFoundException();
        }

        return $challenge;
    }

    public function findOneByYearAndActive($year)
    {
        return $this->findOneBy(['active' => true, 'year' => $year]);
    }

    public function findOneByYearAndActiveOrFail($year)
    {
        $challenge = $this->findOneByYearAndActive($year);

        if (!$challenge instanceof ChallengeInterface) {

            throw new ChallengeNotFoundException();
        }

        return $challenge;
    }
}
