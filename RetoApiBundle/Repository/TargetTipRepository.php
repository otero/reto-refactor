<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RetoApiBundle\Domain\Exception\Target\TargetTipNotFoundException;
use RetoApiBundle\Entity\Interfaces\TargetTipInterface;
use RetoApiBundle\Entity\TargetTip;
use RetoApiBundle\Repository\Interfaces\TargetTipRepositoryInterface;


class TargetTipRepository extends EntityRepository implements TargetTipRepositoryInterface
{
    public function findAllByTarget($id)
    {
        return $this->findBy(array('target' => $id));
    }

    public function findOneByTargetTip($id)
    {
        return $this->find($id);
    }

    public function findOneByTargetTipOrFail($id)
    {
        $targetTip = $this->findOneByTargetTip($id);

        if (!$targetTip instanceof TargetTipInterface) {
            throw new TargetTipNotFoundException();
        }

        return $targetTip;
    }
}
