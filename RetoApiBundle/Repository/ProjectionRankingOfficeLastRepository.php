<?php

namespace RetoApiBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class ProjectionRankingOfficeLastRepository extends EntityRepository
{
    public function findOneByOfficeId($officeId)
    {
        return $this->getEntityManager()
            ->createQuery(
                "
                    SELECT p FROM RetoApiBundle:ProjectionRankingOfficeLast p 
                              WHERE p.officeId = :office
                "
            )
            ->setParameter('office', $officeId)
            ->getOneOrNullResult(Query::HYDRATE_OBJECT);
    }

    public function createOrUpdate($projectionRankingOfficeLast)
    {
        $this->getEntityManager()->persist($projectionRankingOfficeLast);
        //$this->getEntityManager()->flush();
    }

}
