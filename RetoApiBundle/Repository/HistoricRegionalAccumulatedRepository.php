<?php

namespace RetoApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Domain\Exception\HistoricRegionalAccumulated\HistoricRegionalAccumulatedNotFoundException;
use RetoApiBundle\Domain\Exception\Regional\RegionalNotFoundException;
use RetoApiBundle\Entity\Interfaces\HistoricRegionalAccumulatedInterface;
use RetoApiBundle\Repository\Interfaces\HistoricRegionalAccumulatedRepositoryInterface;

class HistoricRegionalAccumulatedRepository extends EntityRepository implements HistoricRegionalAccumulatedRepositoryInterface
{
    public function findOneByRegional($regional)
    {
        return $this->findOneBy(
            array(
                'regional' => $regional
            )
        );
    }

    public function findRankingByRegional()
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
            SELECT h.id as historic_regional_accumulated_id,
                   h.anual as historic_regional_accumulated_annual,
                   h.trim1 as historic_regional_accumulated_trim1,
                   h.trim2 as historic_regional_accumulated_trim2,
                   h.trim3 as historic_regional_accumulated_trim3,
                   h.trim4 as historic_regional_accumulated_trim4,
                   r.id as regional_id,
                   r.name as regional_name,
                   r2.id as red_id,
                   r2.name as red_name,
                   t.id as territorial_id,
                   t.name as territorial_name,
                   DENSE_RANK() OVER (order by h.anual DESC) AS ranking
            
            FROM historic_regional_accumulated as h
                   LEFT JOIN regional r on h.regional_id = r.id
                   LEFT JOIN red r2 on r.red_id = r2.id
                   LEFT JOIN territorial t on t.id = r2.territorial_id
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function findOneByBankSabadellIdAndChallenge($bankSabadellId, $challenge)
    {
        return $this->findOneBy(array(
            'bancsabadellId' => $bankSabadellId,
            'challenge' => $challenge
        ));
    }

    public function findOneByBankSabadellIdAndChallengeOrFail($bankSabadellId, $challenge)
    {
        $historicRegionalAccumulated = $this
            ->findOneByBankSabadellIdAndChallenge($bankSabadellId, $challenge);

        if (!$historicRegionalAccumulated instanceof HistoricRegionalAccumulatedInterface) {
            throw new HistoricRegionalAccumulatedNotFoundException();
        }

        return $historicRegionalAccumulated;
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:HistoricRegionalAccumulated p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function deleteAll()
    {
        $query = $this->getEntityManager()->createQuery('DELETE RetoApiBundle:HistoricRegionalAccumulated hr');
        $query->execute();
    }

    public function createOrUpdate($historicRegionalAccumulated)
    {
        $this->getEntityManager()->persist($historicRegionalAccumulated);
        //$this->getEntityManager()->flush();
    }
}

