<?php

namespace RetoApiBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Entity\Interfaces\ProjectionRankingRegionalInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingRegionalRepositoryInterface;
use RetoApiBundle\Domain\Exception\ProjectionRankingRegional\ProjectionRankingRegionalException;

class ProjectionRankingRegionalRepository extends EntityRepository implements ProjectionRankingRegionalRepositoryInterface
{
    public function findOneByHistoricRegionalAccumulated($historicRegionalAccumulated)
    {
        return $this->findOneBy(array(
            'historicRegionalAccumulatedId' => $historicRegionalAccumulated
        ));
    }

    public function findByFilters($red, $territorial)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('p');
        $qb->from('RetoApiBundle:ProjectionRankingRegional', 'p');

        if (count($red) > 0) {
            $qb->andWhere('p.redId IN (:red)');
            $qb->setParameter('red', $red);
        }

        if (count($territorial) > 0) {
            $qb->andWhere('p.territorialId IN (:territorial)');
            $qb->setParameter('territorial', $territorial);
        }

        $qb->orderBy('p.rankingOfficesTransform', 'ASC');

        return $qb->getQuery()->getArrayResult();
    }

    public function findOneByRegional($regional)
    {
        return $this->getEntityManager()
            ->createQuery(
                "
                    SELECT p FROM RetoApiBundle:ProjectionRankingRegional p 
                              WHERE p.regionalId = :regional
                "
            )
            ->setParameter('regional', $regional)
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function findOneByHistoricRegionalAccumulatedOrFail($historicRegionalAccumulated)
    {
        $projectionRankingRegional = $this->findOneByHistoricRegionalAccumulated($historicRegionalAccumulated);

        if (!$projectionRankingRegional instanceof ProjectionRankingRegionalInterface) {

            throw new ProjectionRankingRegionalException();
        }

        return $projectionRankingRegional;
    }

    public function findRankingByOfficeTransform()
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "
           SELECT p.id,
                   p.historic_regional_accumulated_id,
                   p.office_transform,
                   p.ranking_offices_transform,
                   DENSE_RANK() OVER (order by  p.office_transform DESC, p.cmp_annual DESC) AS ranking
            FROM projection_ranking_regional p
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function deleteAll()
    {
        $this->getEntityManager()->createQuery('DELETE RetoApiBundle:ProjectionRankingRegional p')->execute();
    }

    public function countAll()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT count(p) as count_rows FROM RetoApiBundle:ProjectionRankingRegional p')
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function createOrUpdate($projectionRankingRegional)
    {
        $this->getEntityManager()->persist($projectionRankingRegional);
        //$this->getEntityManager()->flush();
    }
}
