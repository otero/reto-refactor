<?php

namespace RetoApiBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RetoApiBundle\Entity\Historic;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use RetoApiBundle\Entity\Interfaces\HistoricInterface;
use RetoApiBundle\Repository\Interfaces\HistoricRepositoryInterface;
use RetoApiBundle\Domain\Exception\Historic\HistoricNotFoundException;

class HistoricRepository extends EntityRepository implements HistoricRepositoryInterface
{
    public function findOneByProcessedOrFail()
    {
        $historic = $this->findOneBy(['processed' => 0]);

        if (!$historic instanceof HistoricInterface) {

            throw new HistoricNotFoundException();
        }

        return $historic;
    }

    public function findCurrent()
    {
        return $this->createQueryBuilder(Historic::class)
            ->select('h')
            ->from(Historic::class, 'h')
            ->orderBy('h.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findPrevious()
    {
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata(Historic::class, 'h');

        $sql = "SELECT TOP 1 h.* FROM (SELECT TOP 2 * FROM historic  ORDER BY id DESC) h ORDER BY h.id ASC";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getOneOrNullResult();
    }

    /**
     * @param $challenge integer
     * @param $year integer
     * @param $month integer
     * @return mixed
     * @throws HistoricNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @todo AG: add historic.year column so we do not rely on created_at column for year filter
     */
    public function findOneByYearAndMonth($challenge, $year, $month)
    {
        $query = $this->getEntityManager()
            ->createQuery(
                "SELECT h FROM RetoApiBundle:Historic h
                        LEFT JOIN h.challenge c 
                        WHERE c.id = :challenge 
                        AND h.processed = :processed
                        AND YEAR(h.createdAt) = :year
                        AND h.month = :month
            ")
            ->setParameter('challenge', $challenge)
            ->setParameter('year', $year)
            ->setParameter('month', $month)
            ->setParameter('processed', true);

        $historic = $query->getOneOrNullResult(Query::HYDRATE_ARRAY);

        if ($historic == null) {

            throw new HistoricNotFoundException();
        }

        return $historic;
    }

    public function deleteAll()
    {
        $query = $this->getEntityManager()->createQuery('DELETE RetoApiBundle:Historic hr');
        $query->execute();
    }

    public function createOrUpdate($historic)
    {
        $this->getEntityManager()->persist($historic);
        //$this->getEntityManager()->flush();
    }
}

