<?php

namespace RetoApiBundle\Tests\Infrastructure;


use PHPUnit\Framework\TestCase;
use RetoApiBundle\Infraestructure\Repository\FileRepository;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;

class FileRepositoryTest extends TestCase
{
    public function testFileCsv()
    {
        $retoDir = __DIR__.'/../Files/csv';

        $fileRepository = new FileRepository($retoDir);

        $file = $fileRepository->get('export_reto_02072018_v2.csv');

        $this->assertEquals(get_class($file), File::class);
    }

    public function testFileCsvFail()
    {
        $this->expectException(FileNotFoundException::class);
        $retoDir = __DIR__.'/../Files/csv/ERROR_PATH';

        $fileRepository = new FileRepository($retoDir);

        $file = $fileRepository->get('export_reto_02072018_v2.csv');
    }
}