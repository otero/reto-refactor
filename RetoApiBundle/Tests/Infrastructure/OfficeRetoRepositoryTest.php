<?php

namespace RetoApiBundle\Tests\Infrastructure;


use RetoApiBundle\Entity\OfficeReto;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use RetoApiBundle\Domain\Exception\OfficeReto\OfficeRetoNotFoundException;

class OfficeRetoRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();
        $this->entityManager = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    public function testFindOneByBankSabadellIdOrFail()
    {
        $officeReto = $this->entityManager
            ->getRepository(OfficeReto::class)
            ->findOneByBankSabadellIdOrFail(144);

        $this->assertInstanceOf(OfficeReto::class, $officeReto);

        $this->expectException(OfficeRetoNotFoundException::class);
        $this->entityManager
            ->getRepository(OfficeReto::class)
            ->findOneByBankSabadellIdOrFail(100000);

    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}
