<?php

namespace RetoApiBundle\Tests\Infrastructure;


use PHPUnit\Framework\TestCase;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Infraestructure\Repository\csvRepository;
use RetoApiBundle\Infraestructure\Repository\FileRepository;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;

class CSVRepositoryTest extends TestCase
{
    public function testCsv()
    {
        $retoDir = __DIR__.'/../Files/csv';

        $fileRepository = new FileRepository($retoDir);

        $file = $fileRepository->get('export_reto_02072018_v2.csv');

        $csvRepository = new csvRepository();

        $file = $csvRepository->get($file);

        $this->assertEquals(count($file), 4);
        $this->assertEquals(count($file[Historic::UNIT_TERRITORIAL]), 36);
        $this->assertEquals(count($file[Historic::UNIT_RED]), 48);
        $this->assertEquals(count($file[Historic::UNIT_REGIONAL]), 336);
        $this->assertEquals(count($file[Historic::UNIT_OFFICERETO]), 8310);
    }

    public function testFileCsvFail()
    {
        $this->expectException(FileNotFoundException::class);
        $retoDir = __DIR__.'/../Files/csv/ERROR_PATH';

        $fileRepository = new FileRepository($retoDir);

        $file = $fileRepository->get('export_reto_02072018_v2.csv');
    }
}