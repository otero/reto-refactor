<?php

namespace RetoApiBundle\Tests\Infrastructure;


use RetoApiBundle\Domain\Exception\Red\RedNotFoundException;
use RetoApiBundle\Domain\Exception\Territorial\TerritorialNotFoundException;
use RetoApiBundle\Entity\Red;
use RetoApiBundle\Entity\Territorial;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RedRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();
        $this->entityManager = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    public function testFindOneByBankSabadellIdOrFail()
    {
        $territorial = $this->entityManager
            ->getRepository(Red::class)
            ->findOneByBankSabadellIdOrFail(748);

        $this->assertInstanceOf(Red::class, $territorial);

        $this->expectException(RedNotFoundException::class);
        $this->entityManager
            ->getRepository(Red::class)
            ->findOneByBankSabadellIdOrFail(1);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}
