<?php

namespace RetoApiBundle\Tests\Infrastructure;


use RetoApiBundle\Domain\Exception\Territorial\TerritorialNotFoundException;
use RetoApiBundle\Entity\Territorial;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TerritorialRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();
        $this->entityManager = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    public function testFindOneByBankSabadellIdOrFail()
    {
        $territorial = $this->entityManager
            ->getRepository(Territorial::class)
            ->findOneByBankSabadellIdOrFail(711);

        $this->assertInstanceOf(Territorial::class, $territorial);

        $this->expectException(TerritorialNotFoundException::class);
        $this->entityManager
            ->getRepository(Territorial::class)
            ->findOneByBankSabadellIdOrFail(1);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}
