<?php

namespace RetoApiBundle\Tests\Infrastructure;


use RetoApiBundle\Domain\Exception\Red\RedNotFoundException;
use RetoApiBundle\Domain\Exception\Regional\RegionalNotFoundException;
use RetoApiBundle\Domain\Exception\Territorial\TerritorialNotFoundException;
use RetoApiBundle\Entity\Red;
use RetoApiBundle\Entity\Regional;
use RetoApiBundle\Entity\Territorial;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RegionalRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();
        $this->entityManager = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    public function testFindOneByBankSabadellIdOrFail()
    {
        $regional = $this->entityManager
            ->getRepository(Regional::class)
            ->findOneByBankSabadellIdOrFail(724);

        $this->assertInstanceOf(Regional::class, $regional);

        $this->expectException(RegionalNotFoundException::class);
        $this->entityManager
            ->getRepository(Regional::class)
            ->findOneByBankSabadellIdOrFail(1);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}
