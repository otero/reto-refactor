<?php

namespace RetoApiBundle\Tests\Domain\Entity;


use PHPUnit\Framework\TestCase;
use RetoApiBundle\Entity\Challenge;
use RetoApiBundle\Entity\HistoricTerritorialAccumulated;
use RetoApiBundle\Entity\Territorial;

class HistoricTerritorialAccumulatedTest extends TestCase
{
    public function testCreateAccumulated()
    {
        $historicTerritorialAccumulated = new HistoricTerritorialAccumulated();
        $historicTerritorialAccumulated->create(
          new Territorial(),
          new Challenge(),
          10,
            1,
            2,
            3,
            4,
            5
        );

        $this->assertInstanceOf(get_class(new Territorial()), $historicTerritorialAccumulated->getTerritorial());
        $this->assertInstanceOf(get_class(new Challenge()), $historicTerritorialAccumulated->getChallenge());
        $this->assertEquals(10, $historicTerritorialAccumulated->getBancsabadellId());
        $this->assertEquals(1, $historicTerritorialAccumulated->getTrim1());
        $this->assertEquals(2, $historicTerritorialAccumulated->getTrim2());
        $this->assertEquals(3, $historicTerritorialAccumulated->getTrim3());
        $this->assertEquals(4, $historicTerritorialAccumulated->getTrim4());
        $this->assertEquals(5, $historicTerritorialAccumulated->getAnual());
    }

    public function testUpdateAccumulated()
    {
        $historicTerritorialAccumulated = new HistoricTerritorialAccumulated();
        $historicTerritorialAccumulated->update(
            new Territorial(),
            new Challenge(),
            10,
            1,
            2,
            3,
            4,
            5
        );

        $this->assertInstanceOf(get_class(new Territorial()), $historicTerritorialAccumulated->getTerritorial());
        $this->assertInstanceOf(get_class(new Challenge()), $historicTerritorialAccumulated->getChallenge());
        $this->assertEquals(10, $historicTerritorialAccumulated->getBancsabadellId());
        $this->assertEquals(1, $historicTerritorialAccumulated->getTrim1());
        $this->assertEquals(2, $historicTerritorialAccumulated->getTrim2());
        $this->assertEquals(3, $historicTerritorialAccumulated->getTrim3());
        $this->assertEquals(4, $historicTerritorialAccumulated->getTrim4());
        $this->assertEquals(5, $historicTerritorialAccumulated->getAnual());
    }
}