<?php

namespace RetoApiBundle\Tests\Domain\Entity;


use PHPUnit\Framework\TestCase;
use RetoApiBundle\Entity\Territorial;

class TerritorialTest extends TestCase
{
    public function testCreate()
    {
        $territorial = new Territorial();
        $territorial->create(1, 'Territorial1');

        $this->assertEquals(1, $territorial->getBancSabadellId());
        $this->assertEquals('Territorial1', $territorial->getName());
    }
}