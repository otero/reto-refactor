<?php

namespace RetoApiBundle\Tests\Domain\Entity;


use PHPUnit\Framework\TestCase;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\HistoricTerritorial;
use RetoApiBundle\Entity\Target;
use RetoApiBundle\Entity\Territorial;

class HistoricTerritorialTest extends TestCase
{
    public function testCreate()
    {
        $historicTerritorial = new HistoricTerritorial();
        $historicTerritorial->create(
          new Territorial(),
          new Historic(),
          new Target(),
          1,
          2
        );

        $this->assertInstanceOf(get_class(new Territorial()), $historicTerritorial->getTerritorial());
        $this->assertInstanceOf(get_class(new Historic()), $historicTerritorial->getHistoric());
        $this->assertInstanceOf(get_class(new Target()), $historicTerritorial->getTarget());
        $this->assertEquals(1, $historicTerritorial->getAccomplishment());
        $this->assertEquals(2, $historicTerritorial->getPosition());
    }
}
