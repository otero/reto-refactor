<?php

namespace RetoApiBundle\Tests\Domain\Common;


use PHPUnit\Framework\TestCase;
use RetoApiBundle\Domain\Common\Utils;

class UtilsTest extends TestCase
{
    public function testTransformStringToFloat()
    {
        $number = Utils::transformStringToFloat('1,3');

        $this->assertEquals(1.3, $number);
    }
}