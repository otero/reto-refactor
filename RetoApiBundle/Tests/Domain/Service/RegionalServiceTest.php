<?php

namespace RetoApiBundle\Tests\Domain\Service;


use PHPUnit\Framework\TestCase;
use RetoApiBundle\Domain\Exception\Regional\RegionalNotFoundException;
use RetoApiBundle\Domain\Service\RegionalService;
use RetoApiBundle\Domain\Service\TerritorialService;
use RetoApiBundle\Entity\Challenge;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Infraestructure\Repository\csvRepository;
use RetoApiBundle\Infraestructure\Repository\FileRepository;
use RetoApiBundle\Repository\RedRepository;
use RetoApiBundle\Repository\RegionalRepository;
use RetoApiBundle\Repository\TerritorialRepository;

class RegionalServiceTest extends TestCase
{
    private $regionalService;

    private $challenge;

    private $historic;

    private $historicService;

    private $regionalRepository;

    private $redRepository;

    public function testProcess()
    {
        $this->historicService->expects($this->exactly(6))
            ->method('createOrUpdateAccumulated');

        $this->redRepository->expects($this->exactly(6))
            ->method('findOneByBankSabadellIdOrFail');

        $file = $this->getFile();
        $this->regionalService->process(
            $file['Regional'],
            $this->challenge,
            $this->historic
        );
    }

    public function testExtractBSId()
    {
        $this->assertEquals(728, $this->getRegionalId());
    }

    public function testCreateRegionalId()
    {
        $regional = $this->regionalService->create(
            $this->getRegionalId(),
            $this->getRowCsvRegional()
        );

        $this->assertEquals('DIR REG MARESME - BADALONA', $regional->getName());
        $this->assertEquals(get_class($regional), 'RetoApiBundle\Entity\Regional');
    }

    private function getFile()
    {
        $retoDir = __DIR__.'/../../Files/csv';
        $fileRepository = new FileRepository($retoDir);
        $file = $fileRepository->get('csv_reto_test.csv');

        $csvRepository = new csvRepository();
        return $csvRepository->get($file);
    }

    private function getRowCsvRegional()
    {
        $file = $this->getFile();

        return $file['Regional'][0];
    }
    private function getRegionalId()
    {
        return $this->regionalService->extractBSId($this->getRowCsvRegional());
    }

    public function setUp()
    {
        $this->regionalRepository = $this->getMockBuilder(RegionalRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array('findOneByBankSabadellIdOrFail', 'createOrUpdate'))
            ->getMock();
        $this->regionalRepository->method('createOrUpdate')->withAnyParameters()->willReturn(true);
        $this->regionalRepository->method('findOneByBankSabadellIdOrFail')->willThrowException(new RegionalNotFoundException());

        $this->redRepository = $this->getMockBuilder(RedRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array('findOneByBankSabadellIdOrFail', 'createOrUpdate'))
            ->getMock();

        $targetService = $this->getMockBuilder(\RetoApiBundle\Domain\Service\TargetService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->historicService = $this->getMockBuilder(\RetoApiBundle\Domain\Service\HistoricService::class)
            ->disableOriginalConstructor()
            ->setMethods(array('createOrUpdateAccumulated', 'create'))
            ->getMock();

        $this->challenge = $this->getMockBuilder(Challenge::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->historic = $this->getMockBuilder(Historic::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->regionalService = new RegionalService(
            $this->regionalRepository,
            $this->historicService,
            $targetService,
            $this->redRepository
        );
    }
}


