<?php

namespace RetoApiBundle\Tests\Domain\Service;


use PHPUnit\Framework\TestCase;
use RetoApiBundle\Domain\Service\HistoricOfficeRetoService;
use RetoApiBundle\Domain\Service\HistoricRedService;
use RetoApiBundle\Domain\Service\HistoricRegionalService;
use RetoApiBundle\Domain\Service\HistoricService;
use RetoApiBundle\Domain\Service\HistoricTerritorialService;
use RetoApiBundle\Entity\Challenge;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\OfficeReto;
use RetoApiBundle\Entity\Red;
use RetoApiBundle\Entity\Regional;
use RetoApiBundle\Entity\Target;
use RetoApiBundle\Entity\Territorial;
use RetoApiBundle\Infraestructure\Repository\csvRepository;
use RetoApiBundle\Infraestructure\Repository\FileRepository;

class HistoricServiceTest extends TestCase
{
    private $historicTerritorialService;

    private $historicRedService;

    private $historicRegionalService;

    private $historicOfficeRetoService;

    private $historicService;

    public function setUp()
    {
        $this->historicTerritorialService = $this->getMockBuilder(HistoricTerritorialService::class)
            ->disableOriginalConstructor()
            ->setMethods(array('createOrUpdateAccumulated', 'create'))
            ->getMock();

        $this->historicRedService = $this->getMockBuilder(HistoricRedService::class)
            ->disableOriginalConstructor()
            ->setMethods(array('createOrUpdateAccumulated', 'create'))
            ->getMock();

        $this->historicRegionalService = $this->getMockBuilder(HistoricRegionalService::class)
            ->disableOriginalConstructor()
            ->setMethods(array('createOrUpdateAccumulated', 'create'))
            ->getMock();

        $this->historicOfficeRetoService = $this->getMockBuilder(HistoricOfficeRetoService::class)
            ->disableOriginalConstructor()
            ->setMethods(array('createOrUpdateAccumulated', 'create'))
            ->getMock();

        $this->historicService = new HistoricService(
            $this->historicTerritorialService,
            $this->historicRedService,
            $this->historicRegionalService,
            $this->historicOfficeRetoService
        );
    }

    private function getFile()
    {
        $retoDir = __DIR__.'/../../Files/csv';
        $fileRepository = new FileRepository($retoDir);
        $file = $fileRepository->get('csv_reto_test.csv');

        $csvRepository = new csvRepository();
        return $csvRepository->get($file);
    }

    private function getRowCsvTerritorial()
    {
        $file = $this->getFile();

        return $file['Territorial'];
    }

    private function getRowCsvRed()
    {
        $file = $this->getFile();

        return $file['Red'];
    }

    private function getRowCsvRegional()
    {
        $file = $this->getFile();

        return $file['Regional'];
    }

    private function getRowCsvOfficeReto()
    {
        $file = $this->getFile();

        return $file['OfficeReto'];
    }

    public function testCreateOrUpdateAccumulated()
    {
        $csv = $this->getFile();

        $this->historicTerritorialService->expects($this->once())
            ->method('createOrUpdateAccumulated');
        $this->historicRedService->expects($this->once())
            ->method('createOrUpdateAccumulated');
        $this->historicRegionalService->expects($this->once())
            ->method('createOrUpdateAccumulated');
        $this->historicOfficeRetoService->expects($this->once())
            ->method('createOrUpdateAccumulated');

        $this->historicService->createOrUpdateAccumulated($csv['Territorial'][0], new Territorial(), new Challenge());
        $this->historicService->createOrUpdateAccumulated($csv['Red'][0], new Red(), new Challenge());
        $this->historicService->createOrUpdateAccumulated($csv['Regional'][0], new Regional(), new Challenge());
        $this->historicService->createOrUpdateAccumulated($csv['OfficeReto'][0], new OfficeReto(), new Challenge());
    }

    public function testCreate()
    {
        $csv = $this->getFile();

        $this->historicTerritorialService->expects($this->once())
            ->method('create');
        $this->historicRedService->expects($this->once())
            ->method('create');
        $this->historicRegionalService->expects($this->once())
            ->method('create');
        $this->historicOfficeRetoService->expects($this->once())
            ->method('create');

        $this->historicService->create(new Territorial(), new Historic(), new Target(), $csv['Territorial'][1]);
        $this->historicService->create(new Red(), new Historic(), new Target(), $csv['Red'][1]);
        $this->historicService->create(new Regional(), new Historic(), new Target(), $csv['Regional'][1]);
        $this->historicService->create(new OfficeReto(), new Historic(), new Target(), $csv['OfficeReto'][1]);
    }

    public function testGenerateData()
    {
        $csv = $this->getRowCsvTerritorial();

        $array = $this->historicService->generateData($csv[0]);

        $this->assertEquals(array(
            'anual' => 1.2039666666667,
            'trim1' => 1.2147,
            'trim2' => 1.2357,
            'trim3' => 1.1615,
            'trim4' => 0
        ), $array);
    }
    public function testCreateOrUpdateAccumulated2()
    {
        $csv = $this->getRowCsvTerritorial();

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[0], new Territorial(), new Challenge());
        $this->assertTrue($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[1], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[2], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[3], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[4], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[5], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $csv = $this->getRowCsvRed();

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[0], new Territorial(), new Challenge());
        $this->assertTrue($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[1], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[2], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[3], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[4], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[5], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $csv = $this->getRowCsvRegional();

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[0], new Territorial(), new Challenge());
        $this->assertTrue($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[1], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[2], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[3], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[4], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[5], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $csv = $this->getRowCsvOfficeReto();

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[0], new Territorial(), new Challenge());
        $this->assertTrue($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[1], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[2], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[3], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[4], new Territorial(), new Challenge());
        $this->assertFalse($historicService);

        $historicService = $this->historicService->createOrUpdateAccumulated($csv[5], new Territorial(), new Challenge());
        $this->assertFalse($historicService);
    }
}
