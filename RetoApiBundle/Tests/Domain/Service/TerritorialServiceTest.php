<?php

namespace RetoApiBundle\Tests\Domain\Service;


use PHPUnit\Framework\TestCase;
use RetoApiBundle\Domain\Exception\Territorial\TerritorialNotFoundException;
use RetoApiBundle\Domain\Service\TerritorialService;
use RetoApiBundle\Entity\Challenge;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Infraestructure\Repository\csvRepository;
use RetoApiBundle\Infraestructure\Repository\FileRepository;
use RetoApiBundle\Repository\TerritorialRepository;

class TerritorialServiceTest extends TestCase
{
    private $territorialService;

    private $challenge;

    private $historic;

    private $historicService;

    private $territorialRepository;

    public function testProcess()
    {
        $this->historicService->expects($this->exactly(6))
            ->method('createOrUpdateAccumulated');

        $this->territorialRepository->expects($this->exactly(6))
            ->method('createOrUpdate');

        $file = $this->getFile();
        $this->territorialService->process(
            $file['Territorial'],
            $this->challenge,
            $this->historic
        );
    }

    public function testExtractBSId()
    {
        $this->assertEquals(716, $this->getTerritorialId());
    }

    public function testCreateTerritorial()
    {
        $territorial = $this->territorialService->create(
            $this->getTerritorialId(),
            $this->getRowCsvTerritorial()
        );

        $this->assertEquals('DIR TERR CATALUNYA', $territorial->getName());
        $this->assertEquals(get_class($territorial), 'RetoApiBundle\Entity\Territorial');
    }

    private function getFile()
    {
        $retoDir = __DIR__.'/../../Files/csv';
        $fileRepository = new FileRepository($retoDir);
        $file = $fileRepository->get('csv_reto_test.csv');

        $csvRepository = new csvRepository();
        return $csvRepository->get($file);
    }

    private function getRowCsvTerritorial()
    {
        $file = $this->getFile();

        return $file['Territorial'][0];
    }
    private function getTerritorialId()
    {
        return $this->territorialService->extractBSId($this->getRowCsvTerritorial());
    }

    public function setUp()
    {
        $this->territorialRepository = $this->getMockBuilder(TerritorialRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array('createOrUpdate', 'findOneByBankSabadellIdOrFail'))
            ->getMock();

        $this->territorialRepository->method('createOrUpdate')->withAnyParameters()->willReturn(true);
        $this->territorialRepository->method('findOneByBankSabadellIdOrFail')->willThrowException(new TerritorialNotFoundException());

        $targetService = $this->getMockBuilder(\RetoApiBundle\Domain\Service\TargetService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->historicService = $this->getMockBuilder(\RetoApiBundle\Domain\Service\HistoricService::class)
            ->disableOriginalConstructor()
            ->setMethods(array('createOrUpdateAccumulated', 'create'))
            ->getMock();

        $this->challenge = $this->getMockBuilder(Challenge::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->historic = $this->getMockBuilder(Historic::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->territorialService = new TerritorialService($this->territorialRepository, $this->historicService, $targetService);
    }
}


