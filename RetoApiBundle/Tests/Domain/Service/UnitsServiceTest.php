<?php

namespace RetoApiBundle\Tests\Domain\Service;


use PHPUnit\Framework\TestCase;
use RetoApiBundle\Domain\Service\OfficeRetoService;
use RetoApiBundle\Domain\Service\RedService;
use RetoApiBundle\Domain\Service\RegionalService;
use RetoApiBundle\Domain\Service\TerritorialService;
use RetoApiBundle\Domain\Service\UnitsService;
use RetoApiBundle\Entity\Challenge;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Infraestructure\Repository\csvRepository;
use RetoApiBundle\Infraestructure\Repository\FileRepository;
use RetoApiBundle\Repository\TerritorialRepository;

class UnitsServiceTest extends TestCase
{
    public function testProcess()
    {
        $this->territorialService->expects($this->exactly(1))
            ->method('process');

        $this->redService->expects($this->exactly(1))
            ->method('process');

        $this->regionalService->expects($this->exactly(1))
            ->method('process');

        $this->officeRetoService->expects($this->exactly(1))
            ->method('process');

        $this->unitsService->process(
            $this->historic,
            $this->challenge,
            $this->getFile()
        );
    }

    private function getFile()
    {
        $retoDir = __DIR__.'/../../Files/csv';
        $fileRepository = new FileRepository($retoDir);
        $file = $fileRepository->get('csv_reto_test.csv');

        $csvRepository = new csvRepository();
        return $csvRepository->get($file);
    }

    public function setUp()
    {
        $this->historic = $this->getMockBuilder(Historic::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->challenge = $this->getMockBuilder(Challenge::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->territorialService = $this->getMockBuilder(TerritorialService::class)
            ->disableOriginalConstructor()
            ->setMethods(array('process'))
            ->getMock();

        $this->redService = $this->getMockBuilder(RedService::class)
            ->disableOriginalConstructor()
            ->setMethods(array('process'))
            ->getMock();

        $this->regionalService = $this->getMockBuilder(RegionalService::class)
            ->disableOriginalConstructor()
            ->setMethods(array('process'))
            ->getMock();

        $this->officeRetoService = $this->getMockBuilder(OfficeRetoService::class)
            ->disableOriginalConstructor()
            ->setMethods(array('process'))
            ->getMock();

        $this->unitsService = new UnitsService(
            $this->territorialService,
            $this->redService,
            $this->regionalService,
            $this->officeRetoService
        );
    }

    protected $unitsService;

    protected $historic;

    protected $challenge;

    protected $territorialService;

    protected $redService;

    protected $regionalService;

    protected $officeRetoService;
}
