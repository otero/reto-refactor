<?php

namespace RetoApiBundle\Tests\Domain\Service;


use PHPUnit\Framework\TestCase;
use RetoApiBundle\Domain\Exception\OfficeReto\OfficeRetoNotFoundException;
use RetoApiBundle\Domain\Service\OfficeRetoService;
use RetoApiBundle\Entity\Challenge;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Infraestructure\Repository\csvRepository;
use RetoApiBundle\Infraestructure\Repository\FileRepository;
use RetoApiBundle\Repository\OfficeRetoRepository;
use RetoApiBundle\Repository\RegionalRepository;

class OfficeServiceTest extends TestCase
{
    private $officeService;

    private $challenge;

    private $historic;

    private $historicService;

    private $officeRepository;

    private $regionalRepository;

    public function testProcess()
    {
        $this->historicService->expects($this->exactly(6))
            ->method('createOrUpdateAccumulated');

        $this->regionalRepository->expects($this->exactly(6))
            ->method('findOneByBankSabadellIdOrFail');

        $file = $this->getFile();
        $this->officeService->process(
            $file['OfficeReto'],
            $this->challenge,
            $this->historic
        );
    }

    public function testExtractBSId()
    {
        $this->assertEquals(20, $this->getOfficeId());
    }

    public function testCreateOfficeId()
    {
        $office = $this->officeService->create(
            $this->getOfficeId(),
            $this->getRowCsvOffice()
        );

        $this->assertEquals('VILASSAR DE MAR', $office->getName());
        $this->assertEquals(get_class($office), 'RetoApiBundle\Entity\OfficeReto');
    }

    private function getFile()
    {
        $retoDir = __DIR__.'/../../Files/csv';
        $fileRepository = new FileRepository($retoDir);
        $file = $fileRepository->get('csv_reto_test.csv');

        $csvRepository = new csvRepository();
        return $csvRepository->get($file);
    }

    private function getRowCsvOffice()
    {
        $file = $this->getFile();

        return $file['OfficeReto'][0];
    }
    private function getOfficeId()
    {
        return $this->officeService->extractBSId($this->getRowCsvOffice());
    }

    public function setUp()
    {
        $this->officeRepository = $this->getMockBuilder(OfficeRetoRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array('findOneByBankSabadellIdOrFail', 'createOrUpdate'))
            ->getMock();
        $this->officeRepository->method('createOrUpdate')->withAnyParameters()->willReturn(true);
        $this->officeRepository->method('findOneByBankSabadellIdOrFail')->willThrowException(new OfficeRetoNotFoundException());

        $this->regionalRepository = $this->getMockBuilder(RegionalRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array('findOneByBankSabadellIdOrFail', 'createOrUpdate'))
            ->getMock();

        $targetService = $this->getMockBuilder(\RetoApiBundle\Domain\Service\TargetService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->historicService = $this->getMockBuilder(\RetoApiBundle\Domain\Service\HistoricService::class)
            ->disableOriginalConstructor()
            ->setMethods(array('createOrUpdateAccumulated', 'create'))
            ->getMock();

        $this->challenge = $this->getMockBuilder(Challenge::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->historic = $this->getMockBuilder(Historic::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->officeService = new OfficeRetoService(
            $this->officeRepository,
            $this->historicService,
            $targetService,
            $this->regionalRepository
        );
    }
}


