<?php

namespace RetoApiBundle\Tests\Domain\Service;


use PHPUnit\Framework\TestCase;
use RetoApiBundle\Domain\Exception\Red\RedNotFoundException;
use RetoApiBundle\Domain\Service\RedService;
use RetoApiBundle\Entity\Challenge;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Infraestructure\Repository\csvRepository;
use RetoApiBundle\Infraestructure\Repository\FileRepository;
use RetoApiBundle\Repository\RedRepository;
use RetoApiBundle\Repository\TerritorialRepository;

class RedServiceTest extends TestCase
{
    private $redService;

    private $challenge;

    private $historic;

    private $historicService;

    private $redRepository;

    private $territorialRepository;

    public function testProcess()
    {
        $this->historicService->expects($this->exactly(6))
            ->method('createOrUpdateAccumulated');

        $this->territorialRepository->expects($this->exactly(6))
            ->method('findOneByBankSabadellIdOrFail');

        $file = $this->getFile();
        $this->redService->process(
            $file['Red'],
            $this->challenge,
            $this->historic
        );
    }

    public function testExtractBSId()
    {
        $this->assertEquals(747, $this->getRedId());
    }

    public function testCreateRedId()
    {
        $red = $this->redService->create(
            $this->getRedId(),
            $this->getRowCsvRed()
        );

        $this->assertEquals('DIR RED BARCELONA', $red->getName());
        $this->assertEquals(get_class($red), 'RetoApiBundle\Entity\Red');
    }

    private function getFile()
    {
        $retoDir = __DIR__.'/../../Files/csv';
        $fileRepository = new FileRepository($retoDir);
        $file = $fileRepository->get('csv_reto_test.csv');

        $csvRepository = new csvRepository();
        return $csvRepository->get($file);
    }

    private function getRowCsvRed()
    {
        $file = $this->getFile();

        return $file['Red'][0];
    }
    private function getRedId()
    {
        return $this->redService->extractBSId($this->getRowCsvRed());
    }

    public function setUp()
    {
        $this->redRepository = $this->getMockBuilder(RedRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array('findOneByBankSabadellIdOrFail', 'createOrUpdate'))
            ->getMock();
        $this->redRepository->method('createOrUpdate')->withAnyParameters()->willReturn(true);
        $this->redRepository->method('findOneByBankSabadellIdOrFail')->willThrowException(new RedNotFoundException());

        $this->territorialRepository = $this->getMockBuilder(TerritorialRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array('findOneByBankSabadellIdOrFail', 'createOrUpdate'))
            ->getMock();

        $targetService = $this->getMockBuilder(\RetoApiBundle\Domain\Service\TargetService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->historicService = $this->getMockBuilder(\RetoApiBundle\Domain\Service\HistoricService::class)
            ->disableOriginalConstructor()
            ->setMethods(array('createOrUpdateAccumulated', 'create'))
            ->getMock();

        $this->challenge = $this->getMockBuilder(Challenge::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->historic = $this->getMockBuilder(Historic::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->redService = new RedService(
            $this->redRepository,
            $this->historicService,
            $targetService,
            $this->territorialRepository
        );
    }
}


