<?php

namespace RetoApiBundle\Application\DTO;


class GetRegional
{
    private $regional;

    public function __construct(array $regional)
    {
        $this->regional = $regional;
    }

    /**
     * @return array
     */
    public function getRegional()
    {
        return $this->regional;
    }
}
