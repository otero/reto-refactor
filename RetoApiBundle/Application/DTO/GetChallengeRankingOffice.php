<?php

namespace RetoApiBundle\Application\DTO;


class GetChallengeRankingOffice
{
    private $red;

    private $regional;

    private $territorial;

    private $limit;

    public function __construct(array $regional, array $red, array $territorial, $limit)
    {
        $this->regional = $regional;
        $this->red = $red;
        $this->territorial = $territorial;
        $this->limit = $limit;
    }

    /**
     * @return array
     */
    public function getRed()
    {
        return $this->red;
    }

    /**
     * @return array
     */
    public function getRegional()
    {
        return $this->regional;
    }

    /**
     * @return array
     */
    public function getTerritorial()
    {
        return $this->territorial;
    }

    /**
     * @return integer
     */
    public function getLimit()
    {
        return $this->limit;
    }
}
