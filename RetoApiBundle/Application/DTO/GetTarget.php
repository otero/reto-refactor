<?php

namespace RetoApiBundle\Application\DTO;


class GetTarget
{
    private $target;

    public function __construct($target)
    {
        $this->target = $target;
    }

    public function getTarget()
    {
        return $this->target;
    }
}
