<?php

namespace RetoApiBundle\Application\DTO;


class GetChallengeRankingRegional
{
    private $red;

    private $territorial;

    public function __construct(array $red, array $territorial)
    {
        $this->red = $red;
        $this->territorial = $territorial;
    }

    /**
     * @return array
     */
    public function getRed()
    {
        return $this->red;
    }

    /**
     * @return array
     */
    public function getTerritorial()
    {
        return $this->territorial;
    }
}
