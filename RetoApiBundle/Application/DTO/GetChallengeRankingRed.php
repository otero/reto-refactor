<?php

namespace RetoApiBundle\Application\DTO;


class GetChallengeRankingRed
{
    private $territorial;

    public function __construct(array $territorial)
    {
        $this->territorial = $territorial;
    }

    /**
     * @return array
     */
    public function getTerritorial()
    {
        return $this->territorial;
    }
}
