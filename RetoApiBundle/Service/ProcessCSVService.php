<?php

namespace RetoApiBundle\Service;

use RetoApiBundle\Entity\CSV;
use RetoApiBundle\Util\Utils;
use Doctrine\ORM\EntityManager;
use RetoApiBundle\Entity\Historic;
use Symfony\Component\HttpFoundation\File\File;
use RetoApiBundle\Entity\Interfaces\HistoricInterface;
use RetoApiBundle\Entity\Interfaces\ChallengeInterface;

class ProcessCSVService
{
    /**
     * @var File
     */
    private $csv;

    /**
     * @var ChallengeInterface
     */
    private $challenge;

    /**
     * @var HistoricInterface
     */
    private $historic;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TerritorialService
     */
    private $territorialService;

    /**
     * @var RedService
     */
    private $redService;

    /**
     * @var RegionalService
     */
    private $regionalService;

    /**
     * @var OfficeRetoService
     */
    private $officeRetoService;

    /**
     * @var HistoricService
     */
    private $historicService;

    /**
     * @var TargetService
     */
    private $targetService;

    /**
     * ProcessCSVService constructor.
     * @param $entityManager
     * @param $territorialService
     * @param $redService
     * @param $regionalService
     * @param $officeRetoService
     * @param $historicService
     * @param $targetService
     */
    public function __construct(
        EntityManager $entityManager,
        TerritorialService $territorialService,
        RedService $redService,
        RegionalService $regionalService,
        OfficeRetoService $officeRetoService,
        HistoricService $historicService,
        TargetService $targetService
    ) {
        $this->entityManager = $entityManager;
        $this->territorialService = $territorialService;
        $this->redService = $redService;
        $this->regionalService = $regionalService;
        $this->officeRetoService = $officeRetoService;
        $this->historicService = $historicService;
        $this->targetService = $targetService;
    }

    public function execute()
    {
        foreach (Historic::UNITS as $unitType) {
            $handle = fopen($this->csv->getRealPath(), "r");
            fgets($handle);
            $lines = 0;

            while (($row = fgetcsv($handle, null, Historic::DELIMITER)) !== FALSE) {
                $row = array_map('utf8_encode', $row);
                if (array_key_exists($row[CSV::POS_UNIT], Historic::UNITS)
                    && ($unitType === Historic::UNITS[$row[CSV::POS_UNIT]])) {

                    $acronymUnit = array_search($unitType,Historic::UNITS);
                    $this->processRow($acronymUnit, $row);
                    $lines++;
                }
            }

            fclose($handle);
        }
    }

    /**
     * @param $acronymUnit
     * @param $data
     * @throws \Exception
     */
    private function processRow($acronymUnit, $data)
    {
        $idBS = $this->extractBSId($acronymUnit, $data);
        $unitType = Historic::UNITS[$acronymUnit];

        try {
            $unitEntity = $this->entityManager->getRepository('RetoApiBundle:'.$unitType)
                ->findOneByBankSabadellIdOrFail(
                    array('bancsabadellId' => $idBS)
                );

        } catch (\Exception $e) {
            $this->createUnit($unitType, $idBS, $data);
        }

        $accomp = Utils::transformStringToFloat($data[CSV::POS_TARGET_ACCMP]);

        if ($data[CSV::POS_TARGET] == 0) {
            $dataAcc = $this->generateData($data);
            $this->historicService->createOrUpdateAccumulated($unitEntity, $this->challenge, $dataAcc);
            return;
        }

        $target = $this->targetService
            ->getTargetOrCreate($data[CSV::POS_TARGET], $data[CSV::POS_TARGET_DESCR]);

        $this->historicService->create($unitEntity, $this->historic, $target, $accomp);
    }

    /**
     * @param $data
     * @return array
     */
    private function generateData($data)
    {
        return array(
            Historic::ACC_ANUAL => Utils::transformStringToFloat($data[CSV::POS_ACCMP_ACUMULAT]),
            Historic::ACC_TRIM1 => Utils::transformStringToFloat($data[CSV::POS_ACCMP_TRIM_1]),
            Historic::ACC_TRIM2 => Utils::transformStringToFloat($data[CSV::POS_ACCMP_TRIM_2]),
            Historic::ACC_TRIM3 => Utils::transformStringToFloat($data[CSV::POS_ACCMP_TRIM_3]),
            Historic::ACC_TRIM4 => Utils::transformStringToFloat($data[CSV::POS_ACCMP_TRIM_4])
        );
    }

    /**
     * @param $unitType
     * @param $idBS
     * @param $data
     * @return null|object|\RetoApiBundle\Entity\OfficeReto|\RetoApiBundle\Entity\Red|\RetoApiBundle\Entity\Territorial
     * @throws \Exception
     */
    private function createUnit($unitType, $idBS, $data)
    {
        switch($unitType) {
            case Historic::UNIT_OFFICERETO:
                $regionalBSId = $this->extractBSId(Historic::ACRONYM_REGIONAL, $data);
                $unitEntity = $this->officeRetoService->create($idBS, $data[CSV::POS_OFFICE_NAME], $regionalBSId);
                break;

            case Historic::UNIT_REGIONAL:
                $redBSId = $this->extractBSId(Historic::ACRONYM_RED, $data);
                $unitEntity = $this->regionalService->create($idBS, $data[CSV::POS_REGIONAL_NAME], $redBSId);
                break;

            case Historic::UNIT_RED:
                $territorialBSId = $this->extractBSId(Historic::ACRONYM_TERRITORIAL, $data);
                $unitEntity = $this->redService->create($idBS, $data[CSV::POS_RED_NAME], $territorialBSId);
                break;

            case Historic::UNIT_TERRITORIAL:
                $unitEntity = $this->territorialService->create($idBS, $data[CSV::POS_TERRITORIAL_NAME]);
                break;

            default:
                throw new \Exception('The unit type not exist');
        }

        return $unitEntity;
    }

    /**
     * @param $unitType
     * @param $row
     * @return bool|int|string
     * @throws \Exception
     */
    private function extractBSId($unitType, $row)
    {
        switch ($unitType) {
            case Historic::ACRONYM_TERRITORIAL:
                $idBS = $row[CSV::POS_TERRITORIAL_BS_ID];
                break;
            case Historic::ACRONYM_RED:
                $idBS = $row[CSV::POS_RED_BS_ID];
                break;
            case Historic::ACRONYM_REGIONAL:
                $idBS = $row[CSV::POS_REGIONAL_BS_ID];
                break;
            case Historic::ACRONYM_OFFICE:
                $csvId = $row[CSV::POS_OFFICE_BS_ID];
                foreach (Historic::OFFICE_PREFIX as $prefix) {
                    $lengthExtract = strlen($prefix);
                    if (strpos($csvId, $prefix) !== false) {
                        return substr($csvId, $lengthExtract, (strlen($csvId)-strlen($lengthExtract)));
                    }

                    $idBS = $csvId;
                }
                break;
            default:
                //todo: It should be a log service to regonized when does not exist the unit type
                throw new \Exception('Not recognized unit -->' . $unitType);
                break;
        }

        $prefixBs = Historic::PREFIX_BANK_ID;
        return (int)substr((int)$idBS, strlen($prefixBs));
    }
}
