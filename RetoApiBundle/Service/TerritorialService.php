<?php

namespace RetoApiBundle\Service;

use RetoApiBundle\Entity\Bank;
use Doctrine\ORM\EntityManager;
use RetoApiBundle\Entity\Territorial;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TerritorialService
{
    private $em;

    private $container;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function create($bancSabadellId, $name)
    {
        $territorial = new Territorial();
        $territorial->create($bancSabadellId, $name);

        $this->em->getRepository('RetoApiBundle:Territorial')->createOrUpdate($territorial);

        return $territorial;
    }

    public function getTerritorial($id)
    {
        if ($id === false) {
            return new Territorial();
        } else {
            $territorial = $this->em->getRepository('RetoApiBundle:Territorial')->find($id);
            return ($territorial)
                ? $territorial
                : new Territorial();
        }
    }

    public function getTerritorialByNum($num)
    {
        if ($num == false) {
            return false;
        } else {
            $territorial = $this->em->getRepository('RetoApiBundle:Territorial')->findOneByBancsabadellId($num);
            return ($territorial)
                ? $territorial
                : false;
        }
    }

    public function updateData(Territorial $territorial)
    {
        if (!$territorial->getId()) {
            $territorial->setCreatedAt(new \DateTime());
        } else {
            $territorial->setUpdatedAt(new \DateTime());
        }

        $this->em->persist($territorial);

        $this->em->flush();
    }

    public function getTotal()
    {
        return count($this->em->getRepository('RetoApiBundle:Territorial')->findAll());
    }

    public function getPunctualTerritorialTopRanking(Bank $bank, $limit = '')
    {
        if (empty($limit)) {
            $punctual_territorials_ranking = $this->em->getRepository('RetoApiBundle:Territorial')->findBy(
                ['bank' => $bank],
                ['points' => 'DESC']
            );
        } else {
            $punctual_territorials_ranking = $this->em->getRepository('RetoApiBundle:Territorial')->findBy(
                ['bank' => $bank],
                ['points' => 'DESC'],
                $limit
            );
        }

        $territorial_ranking = array();
        foreach ($punctual_territorials_ranking as $territorial) {
            if ($territorial->getCurrentPunctualPosition() < $territorial->getLastPunctualPosition()) {
                $status = 'up';
            } elseif ($territorial->getCurrentPunctualPosition() == $territorial->getLastPunctualPosition()) {
                $status = 'equal';
            } else {
                $status = 'down';
            }

            $territorial_ranking[] = [
                'name'          =>  $territorial->getName(),
                'percentage'    =>  $territorial->getPoints(),
                'status'        =>  $status,
                'id'            =>  $territorial->getId(),
                'bs_id'         =>  $territorial->getBancSabadellId(),
            ];
        }

        return $territorial_ranking;
    }
}
