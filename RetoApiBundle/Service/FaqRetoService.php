<?php
/**
 * Created by PhpStorm.
 * User: Marc
 * Date: 18/07/2017
 * Time: 18:07
 */

namespace RetoApiBundle\Service;


use Doctrine\ORM\EntityManager;
use RetoApiBundle\Entity\FaqReto;
use RetoApiBundle\Repository\FaqRetoRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class FaqRetoService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var FaqRetoRepository
     */
    private $repository;

    public function __construct(EntityManager $entityManager, ContainerInterface $container, FaqRetoRepository $entityRepository)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repository = $entityRepository;
    }

    /**
     * return Faq object or new object
     * @param $id
     * @return null|object|FaqReto
     */
    public function getFaq($id)
    {
        if ($id === false) {
            return new FaqReto();
        } else {
            $faq = $this->em->getRepository('RetoApiBundle:FaqReto')->find($id);
            return ($faq)
                ? $faq
                : new FaqReto();
        }
    }

    /**
     * Save data
     * @param FaqReto $faqReto
     */
    public function updateData(FaqReto $faqReto)
    {
        if (!$faqReto->getId()) {
            $faqReto->setCreatedAt(new \DateTime());
        } else {
            $faqReto->setUpdatedAt(new \DateTime());
        }

        $this->em->persist($faqReto);

        $this->em->flush();
    }

    /**
     * Save images for FAQ
     * @param UploadedFile|null $image
     * @param $idFaq
     * @return bool
     */
    public function setImage(UploadedFile $image = null, $idFaq)
    {
        if ($image && $image->isValid() && exif_imagetype($image)) {
            $faqReto = $this->em->getRepository('RetoApiBundle:FaqReto')->find($idFaq);
            $uploadDirectory = $this->container->get('kernel')->getRootDir() . '/../web/';

            // check if we have previous media to delete it
            if ($faqReto->getImageUrl() !== null) {
                $finder = new Finder();
                $finder->name('img_faq*');
                $finder->files()->in($uploadDirectory);

                foreach($finder as $file) {
                    unlink($file->getRealPath());
                }
            }

            // save image
            $faqNum = 'faq_' . $idFaq;
            $directory = FaqReto::UPLOAD_DIR . '/' . $faqNum . '/';
            $filename = 'img_faq.' . $image->getClientOriginalExtension();

            $faqReto->setImageUrl($directory.$filename);

            $image->move($uploadDirectory.$directory, $filename);

            $this->em->flush();
            return true;
        }
    }

    /**
     * Save media for FAQ
     * @param UploadedFile|null $media
     * @param $idFaq
     * @return bool
     */
    public function setMedia(UploadedFile $media = null, $idFaq)
    {
        if ($media && $media->isValid()) {
            $faqReto = $this->em->getRepository('RetoApiBundle:FaqReto')->find($idFaq);
            $uploadDirectory = $this->container->get('kernel')->getRootDir() . '/../web/';

            // check if we have previous media to delete it
            if ($faqReto->getMediaUrl() !== null) {
                $a_mediaName = explode('/',$faqReto->getMediaUrl());
                $mediaName = end($a_mediaName);

                $finder = new Finder();
                $finder->name($mediaName);
                $finder->files()->in($uploadDirectory);

                foreach($finder as $file) {
                    unlink($file->getRealPath());
                }
            }

            // save media
            $faqNum = 'faq_' . $idFaq;
            $directory = FaqReto::UPLOAD_DIR . '/' . $faqNum . '/';
            $filename = $media->getClientOriginalName();

            $faqReto->setMediaUrl($directory.$filename);

            $media->move($uploadDirectory.$directory, $filename);

            $this->em->flush();
            return true;
        }
    }

    /**
     * Return FAQs at sidebar on HOME
     * @return array|string
     */
    public function findHomeFaqs()
    {
        $records = $this->repository->findHomeFaqs();

        // modify output data to add new key:value (mediaName)
        $outputData = array();
        foreach ($records as $idx => $elem) {
            $mediaName = null;
            if (isset($elem['mediaUrl'])) {
                $mediaItem = explode("/", $elem['mediaUrl']);
                $mediaName = end($mediaItem);
            }
            $outputData[] = array(
                'id' => $elem['id'],
                'question'      =>  $elem['question'],
                'position'      =>  $elem['position'],
            );
        }

        return $outputData;
    }

    /**
     * Return all FAQs available
     * @return array
     */
    public function findFaqs(Request $request)
    {
        $records = $this->repository->findAllFaqs();

        // modify output data to add new key:value (mediaName)
        $contact_reto = $this->em->getRepository('PtiBundle:Configuration')->findOneBy(
            ['name' => 'contact_reto'],
            ['id' => 'DESC']
        );
        $contact_email = ($contact_reto && filter_var($contact_reto->getValue(), FILTER_VALIDATE_EMAIL) !== false)
            ? $contact_reto->getValue()
            : 'PlanTransformacion@bancsabadell.com';

        $outputData = array('contact_email' => $contact_email, 'faqs' => []);

        foreach ($records as $idx => $elem) {

            $mediaName = null;
            if (isset($elem['mediaUrl'])) {
                $mediaItem = explode("/", $elem['mediaUrl']);
                $mediaName = end($mediaItem);
            }

            $question = [
                'id'            => $elem['id'],
                'question'      => $elem['question'],
                'answer'        => $elem['answer'],
                'image_url'     => $elem['imageUrl'],
                'media_url'     => $request->getSchemeAndHttpHost() . '/' . $elem['mediaUrl'],
                'media_name'    => $mediaName,
                'position'      => $elem['position'],
            ];

            array_push($outputData['faqs'], (object) $question);
        }

        return $outputData;
    }
}