<?php

namespace RetoApiBundle\Service;

use Doctrine\ORM\EntityManager;
use RetoApiBundle\Entity\OfficeReto;
use RetoApiBundle\Entity\Regional;
use RetoApiBundle\Repository\OfficeRetoRepository;


class OfficeRetoService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var OfficeRetoRepository
     */
    private $officeRetoRepo;

    public function __construct(EntityManager $entityManager, OfficeRetoRepository $officeRetoRepository)
    {
        $this->em = $entityManager;
        $this->officeRetoRepo = $officeRetoRepository;
    }

    /**
     * @param $bancSabadellId
     * @param $name
     * @param $regionalBSId
     * @return OfficeReto
     * @throws \Exception
     */
    public function create($bancSabadellId, $name, $regionalBSId)
    {
        $regional = $this->em->getRepository(Regional::class)->findOneByBankSabadellIdOrFail($regionalBSId);

        $officeReto = new OfficeReto();
        $officeReto->create($bancSabadellId, $name, $regional);

        $this->em->getRepository(OfficeReto::class)->createOrUpdate($officeReto);

        return $officeReto;
    }

    /**
     * @param $id
     * @return null|object|OfficeReto
     */
    public function getOffice($id)
    {
        if ($id === false) {
            return new OfficeReto();
        } else {
            $office = $this->em->getRepository('RetoApiBundle:OfficeReto')->find($id);
            return ($office)
                ? $office
                : new OfficeReto();
        }
    }

    public function getOfficeByNum($num)
    {
        if ($num == false) {
            return false;
        } else {
            $office = $this->em->getRepository('RetoApiBundle:OfficeReto')->findOneByBancsabadellId($num);
            return ($office)
                ? $office
                : false;
        }
    }

    public function updateData(OfficeReto $officeReto)
    {
        if (!$officeReto->getId()) {
            $officeReto->setCreatedAt(new \DateTime());
        } else {
            $officeReto->setUpdatedAt(new \DateTime());
        }

        $this->em->persist($officeReto);

        $this->em->flush();
    }
}
