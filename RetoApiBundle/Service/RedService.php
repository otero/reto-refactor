<?php

namespace RetoApiBundle\Service;

use RetoApiBundle\Entity\Red;
use Doctrine\ORM\EntityManager;
use RetoApiBundle\Entity\Territorial;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RedService
{

    private $em;

    private $container;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function create($bancSabadellId, $name, $territorialBSId)
    {
        $territorial = $this->em->getRepository(Territorial::class)->findOneByBankSabadellIdOrFail($territorialBSId);

        $red = new Red();
        $red->create($bancSabadellId, $name, $territorial);

        $this->em->getRepository(Red::class)->createOrUpdate($red);

        return $red;
    }

    public function getRed($id)
    {
        if ($id === false) {
            return new Red();
        } else {
            $red = $this->em->getRepository('RetoApiBundle:Red')->find($id);
            return ($red)
                ? $red
                : new Red();
        }
    }

    public function getRedByNum($num)
    {
        if ($num == false) {
            return false;
        } else {
            $red = $this->em->getRepository('RetoApiBundle:Red')->findOneByBancsabadellId($num);
            return ($red)
                ? $red
                : false;
        }
    }

    public function updateData(Red $red)
    {
        if (!$red->getId()) {
            $red->setCreatedAt(new \DateTime());
        } else {
            $red->setUpdatedAt(new \DateTime());
        }

        $this->em->persist($red);

        $this->em->flush();
    }

    public function getTotal()
    {
        return count($this->em->getRepository('RetoApiBundle:Red')->findAll());
    }

    public function getPunctualRedTopRanking(Territorial $territorial, $limit = '')
    {
        if (empty($limit)) {
            $punctual_redes_ranking = $this->em->getRepository('RetoApiBundle:Red')->findBy(
                ['territorial' => $territorial],
                ['points' => 'DESC']
            );
        } else {
            $punctual_redes_ranking = $this->em->getRepository('RetoApiBundle:Red')->findBy(
                ['territorial' => $territorial],
                ['points' => 'DESC'],
                $limit
            );
        }

        $redes_ranking = array();

        foreach ($punctual_redes_ranking as $red) {
            if ($red->getCurrentPunctualPosition() < $red->getLastPunctualPosition()) {
                $status = 'up';
            } elseif ($red->getCurrentPunctualPosition() == $red->getLastPunctualPosition()) {
                $status = 'equal';
            } else {
                $status = 'down';
            }

            $redes_ranking[] = [
                'name'          =>  $red->getName(),
                'percentage'    =>  $red->getPoints(),
                'status'        =>  $status,
                'id'            =>  $red->getId(),
                'bs_id'         =>  $red->getBancSabadellId(),
            ];
        }

        return $redes_ranking;
    }
}