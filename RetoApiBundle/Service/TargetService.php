<?php

namespace RetoApiBundle\Service;

use Doctrine\ORM\EntityManager;
use RetoApiBundle\Entity\Target;


class TargetService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * TargetService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $bsIndicador
     * @param $name
     * @return Target
     */
    public function getTargetOrCreate($bsIndicador, $name)
    {
        try {
            $target = $this->em->getRepository(Target::class)
                ->findOneByBsIndicatorOrFail($bsIndicador);
        } catch (\Exception $e) {
            $target = new Target();
            $target->create(
                $bsIndicador,
                $name
            );

            $this->em->getRepository(Target::class)
                ->createOrUpdate($target);
        }

        return $target;
    }
}
