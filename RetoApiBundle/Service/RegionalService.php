<?php

namespace RetoApiBundle\Service;

use RetoApiBundle\Entity\Red;
use Doctrine\ORM\EntityManager;
use RetoApiBundle\Entity\Regional;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RegionalService
{
    private $em;

    private $container;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function create($bancSabadellId, $name, $redBSId)
    {
        $red = $this->em->getRepository(Red::class)->findOneByBankSabadellIdOrFail($redBSId);

        $regional = new Regional();
        $regional->create($bancSabadellId, $name, $red);

        $this->em->getRepository(Red::class)->createOrUpdate($red);

        return $red;
    }


    public function getRegional($id)
    {
        if ($id === false) {
            return new Regional();
        } else {
            $regional = $this->em->getRepository('RetoApiBundle:Regional')->find($id);
            return ($regional)
                ? $regional
                : new Regional();
        }
    }

    public function getRegionalByNum($num)
    {
        if ($num == false) {
            return false;
        } else {
            $regional = $this->em->getRepository('RetoApiBundle:Regional')->findOneByBancsabadellId($num);
            return ($regional)
                ? $regional
                : false;
        }
    }

    public function updateData(Regional $regional)
    {
        if (!$regional->getId()) {
            $regional->setCreatedAt(new \DateTime());
        } else {
            $regional->setUpdatedAt(new \DateTime());
        }

        $this->em->persist($regional);

        $this->em->flush();
    }

    public function getTotal()
    {
        return count($this->em->getRepository('RetoApiBundle:Regional')->findAll());
    }

    public function getPunctualRegionalTopRanking(Red $red, $limit = '')
    {
        if(empty($limit)) {
            $punctual_regionals_ranking = $this->em->getRepository('RetoApiBundle:Regional')->findBy(
                ['red' => $red],
                ['points' => 'DESC']
            );
        } else {
            $punctual_regionals_ranking = $this->em->getRepository('RetoApiBundle:Regional')->findBy(
                ['red' => $red],
                ['points' => 'DESC'],
                $limit
            );
        }

        $regionals_ranking = array();
        /** @var Regional $regional */
        foreach ($punctual_regionals_ranking as $regional) {
            if ($regional->getCurrentPunctualPosition() < $regional->getLastPunctualPosition()) {
                $status = 'up';
            } elseif ($regional->getCurrentPunctualPosition() == $regional->getLastPunctualPosition()) {
                $status = 'equal';
            } else {
                $status = 'down';
            }

            $regionals_ranking[] = [
                'name'          =>  $regional->getName(),
                'percentage'    =>  $regional->getPoints(),
                'status'        =>  $status,
                'id'            =>  $regional->getId(),
                'bs_id'         =>  $regional->getBancSabadellId(),
            ];
        }

        return $regionals_ranking;
    }
}
