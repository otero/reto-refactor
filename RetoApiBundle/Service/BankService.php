<?php
/**
 * Created by PhpStorm.
 * User: Marc Ginovart <marc.ginovart@berepublic.es>
 * Date: 17/07/2017
 * Time: 14:48
 */

namespace RetoApiBundle\Service;


use Doctrine\ORM\EntityManager;
use RetoApiBundle\Entity\Bank;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BankService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * BankService constructor.
     * @param EntityManager $entityManager
     * @param ContainerInterface $container
     */
    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    /**
     * return Bank object or new object
     * @param $id
     * @return null|object|Bank
     */
    public function getBank($id)
    {
        if ($id === false) {
            return new Bank();
        } else {
            $bank = $this->em->getRepository('RetoApiBundle:Bank')->find($id);
            return ($bank)
                ? $bank
                : new Bank();
        }
    }

    /**
     * return Bank's users on Challenge onficina
     * @param $num
     * @return bool
     */
    public function getBankByNum($num)
    {
        if ($num == false) {
            return false;
        } else {
            $bank = $this->em->getRepository('RetoApiBundle:Bank')->findOneByBancsabadellId($num);
            return ($bank)
                ? $bank
                : false;
        }
    }

    /**
     * Save data
     * @param Bank $bank
     */
    public function updateData(Bank $bank)
    {
        if (!$bank->getId()) {
            $bank->setCreatedAt(new \DateTime());
        } else {
            $bank->setUpdatedAt(new \DateTime());
        }

        $this->em->persist($bank);

        $this->em->flush();
    }

    /**
     * Get totals bank
     *
     * @return int
     */
    public function getTotal()
    {
        return count($this->em->getRepository('RetoApiBundle:Bank')->findAll());
    }
}