<?php

namespace RetoApiBundle\Service;


use Exce4ption;
use RetoApiBundle\Entity\Red;
use Doctrine\ORM\EntityManager;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\Regional;
use RetoApiBundle\Entity\Challenge;
use RetoApiBundle\Entity\OfficeReto;
use RetoApiBundle\Entity\Territorial;
use RetoApiBundle\Entity\HistoricRed;
use RetoApiBundle\Entity\HistoricOffice;
use RetoApiBundle\Entity\HistoricRegional;
use RetoApiBundle\Entity\HistoricTerritorial;
use RetoApiBundle\Entity\HistoricRedAccumulated;
use RetoApiBundle\Entity\HistoricOfficeAccumulated;
use RetoApiBundle\Entity\HistoricRegionalAccumulated;
use RetoApiBundle\Entity\HistoricTerritorialAccumulated;

class HistoricService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * HistoricService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $unit
     * @param $historic
     * @param $target
     * @param $accomplishment
     */
    public function create($unit, $historic, $target, $accomplishment)
    {
        $historicUnit = $this->createHistoricInstance($unit);
        $historicUnit->create($unit, $historic, $target, $accomplishment, 0);

        $this->em->getRepository(get_class($historicUnit))
                        ->createOrUpdate($historicUnit);
    }

    /**
     * @param $unit
     * @param $challenge
     * @param $data
     */
    public function createOrUpdateAccumulated($unit, $challenge, $data)
    {
        $historicAccumulatedRepository = $this->createHistoricAccumulatedRepositoryInstance($unit);

        try {
            $historicAcc = $historicAccumulatedRepository->findOneByBankSabadellIdAndChallengeOrFail(
                $unit->getBancsabadellId(),
                $challenge->getId()
            );

            $historicAcc->update(
                $unit,
                $unit->getBancsabadellId(),
                $data[Historic::ACC_TRIM1],
                $data[Historic::ACC_TRIM2],
                $data[Historic::ACC_TRIM3],
                $data[Historic::ACC_TRIM4],
                $data[Historic::ACC_ANUAL]
            );

        } catch (\Exception $e) {
            $historicAcc = $this->createHistoricAccumulatedInstance($unit);

            $historicAcc->create(
                $unit,
                $unit->getBancsabadellId(),
                $data[Historic::ACC_TRIM1],
                $data[Historic::ACC_TRIM2],
                $data[Historic::ACC_TRIM3],
                $data[Historic::ACC_TRIM4],
                $data[Historic::ACC_ANUAL]
            );
        }

        $historicAccumulatedRepository->createorUpdate($historicAcc);
    }

    /**
     * @param $unit
     * @return \Doctrine\ORM\EntityRepository|\RetoApiBundle\Repository\HistoricOfficeAccumulatedRepository|\RetoApiBundle\Repository\HistoricRedAccumulatedRepository|\RetoApiBundle\Repository\HistoricRegionalAccumulatedRepository|\RetoApiBundle\Repository\HistoricTerritorialAccumulatedRepository
     */
    private function createHistoricAccumulatedRepositoryInstance($unit)
    {
        $class = get_class($unit);

        switch ($class) {
            case OfficeReto::class:
                $repository = $this->em->getRepository(HistoricOfficeAccumulated::class);
                break;
            case Regional::class:
                $repository = $this->em->getRepository(HistoricRegionalAccumulated::class);
                break;
            case Red::class:
                $repository = $this->em->getRepository(HistoricRedAccumulated::class);
                break;
            case Territorial::class:
                $repository = $this->em->getRepository(HistoricTerritorialAccumulated::class);
                break;
            default:
                throw new Exception('Not instance -->' . $class);
        }

        return $repository;
    }

    /**
     * @param $unit
     * @return HistoricOffice|HistoricRed|HistoricRegional|HistoricTerritorial
     */
    private function createHistoricInstance($unit)
    {
        $class = get_class($unit);

        switch ($class) {
            case OfficeReto::class:
                $entity =  new HistoricOffice();
                break;
            case Regional::class:
                $entity = new HistoricRegional();
                break;
            case Red::class:
                $entity = new HistoricRed();
                break;
            case Territorial::class:
                $entity = new HistoricTerritorial();
                break;
            default:
                throw new Exception('Not recognized class: ' . $class);
        }

        return $entity;
    }

    /**
     * @param $unit
     * @return HistoricOfficeAccumulated|HistoricRedAccumulated|HistoricRegionalAccumulated|HistoricTerritorialAccumulated
     */
    private function createHistoricAccumulatedInstance($unit)
    {
        $class = get_class($unit);

        switch ($class) {
            case OfficeReto::class:
                $entity = new HistoricOfficeAccumulated();
                break;
            case Regional::class:
                $entity = new HistoricRegionalAccumulated();
                break;
            case Red::class:
                $entity = new HistoricRedAccumulated();
                break;
            case Territorial::class:
                $entity = new HistoricTerritorialAccumulated();
                break;
            default:
                throw new Exception('Not instance -->' . $class);
        }

        return $entity;
    }
}
