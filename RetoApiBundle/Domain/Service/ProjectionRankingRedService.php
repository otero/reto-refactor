<?php

namespace RetoApiBundle\Domain\Service;


use RetoApiBundle\Domain\Exception\ProjectionRankingRed\ProjectionRankingRedException;
use RetoApiBundle\Entity\ProjectionRankingRed;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingRedRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingOfficeRepositoryInterface;
use Doctrine\ORM\EntityManager;

final class ProjectionRankingRedService
{
    /**
     * @var ProjectionRankingOfficeRepositoryInterface
     */
    private $projectionRankingOfficeRepository;

    /**
     * @var ProjectionRankingRedRepositoryInterface
     */
    private $projectionRankingRedRepository;

    /**
     * ProjectionRankingRedService constructor.
     * @param ProjectionRankingOfficeRepositoryInterface $projectionRankingOfficeRepository
     * @param ProjectionRankingRedRepositoryInterface $projectionRakingRedRepository
     */

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(
        ProjectionRankingOfficeRepositoryInterface $projectionRankingOfficeRepository,
        ProjectionRankingRedRepositoryInterface $projectionRakingRedRepository,
        EntityManager $entityManager
    ) {
        $this->projectionRankingOfficeRepository = $projectionRankingOfficeRepository;
        $this->projectionRankingRedRepository = $projectionRakingRedRepository;
        $this->em = $entityManager;
    }

    public function generate($redRanking)
    {
        foreach ($redRanking as $red) {
            $officesTransform = $this->projectionRankingOfficeRepository->findOfficesTransformByRed($red['red_id']);

            try {
                $projectionRankingRed = $this->projectionRankingRedRepository
                    ->findOneByHistoricRedAccumulatedOrFail($red['historic_red_accumulated_id']);

                $projectionRankingRed->update(
                    $red['historic_red_accumulated_id'],
                    $red['red_id'],
                    $red['red_name'],
                    $red['territorial_id'],
                    $red['territorial_name'],
                    $red['ranking'],
                    $red['historic_red_accumulated_annual'],
                    $red['historic_red_accumulated_trim1'],
                    $red['historic_red_accumulated_trim2'],
                    $red['historic_red_accumulated_trim3'],
                    $red['historic_red_accumulated_trim4'],
                    $officesTransform[0]['count_offices_transform'] / $officesTransform[0]['count_total_offices']
                );
            } catch (ProjectionRankingRedException $e) {

                $projectionRankingRed = new ProjectionRankingRed();
                $projectionRankingRed->create(
                    $red['historic_red_accumulated_id'],
                    $red['red_id'],
                    $red['red_name'],
                    $red['territorial_id'],
                    $red['territorial_name'],
                    $red['ranking'],
                    $red['historic_red_accumulated_annual'],
                    $red['historic_red_accumulated_trim1'],
                    $red['historic_red_accumulated_trim2'],
                    $red['historic_red_accumulated_trim3'],
                    $red['historic_red_accumulated_trim4'],
                    $officesTransform[0]['count_offices_transform'] / $officesTransform[0]['count_total_offices']
                );
            }

            $this->projectionRankingRedRepository->createOrUpdate($projectionRankingRed);
        }
        $this->em->flush();
        $this->em->clear();

    }
}
