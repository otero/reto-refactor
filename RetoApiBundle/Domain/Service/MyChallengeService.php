<?php

namespace RetoApiBundle\Domain\Service;


final class MyChallengeService
{
    public function generate($targets, $historicOfficeRetoCurrent, $historicOfficeRetoLast)
    {
        $myTarget = array();

        foreach($targets as $target) {

            $arrayTarget = array(
                'id' => $target['id'],
                'name' => $target['name'],
                'bsIndicator' => $target['bsIndicador']
            );

            foreach($historicOfficeRetoCurrent as $horc) {
                if ($target['id'] == $horc['target_id']) {
                    $arrayHistoricOfficeRetoCurrent = array(
                        'historic_office_id' =>   $horc['historic_office_id'],
                        'historic_accomplishment' => $horc['historic_accomplishment'],
                        'historic_accomplishment_percentage' => $horc['historic_accomplishment'] * 100
                    );
                }
            }

            $comparative = 0;

            if ($historicOfficeRetoLast) {
                foreach($historicOfficeRetoLast as $horl) {
                    if ($target['id'] == $horl['target_id']) {
                        $comparative = $arrayHistoricOfficeRetoCurrent['historic_accomplishment']
                            - $horl['historic_accomplishment'];
                    }
                }
            }

            $arrayHistoricOfficeRetoCurrent['comparative_last_month'] = $comparative * 100;

            $arrayTarget['percentage'] = $arrayHistoricOfficeRetoCurrent;

            array_push($myTarget, $arrayTarget);
        }

        return $myTarget;
    }

}