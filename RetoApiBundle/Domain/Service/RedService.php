<?php

namespace RetoApiBundle\Domain\Service;


use RetoApiBundle\Entity\CSV;
use RetoApiBundle\Entity\Red;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\Interfaces\HistoricInterface;
use RetoApiBundle\Entity\Interfaces\ChallengeInterface;
use RetoApiBundle\Domain\Exception\Red\RedNotFoundException;
use RetoApiBundle\Repository\Interfaces\RedRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\TerritorialRepositoryInterface;

class RedService
{
    /**
     * @var RedRepositoryInterface
     */
    private $redRepository;

    /**
     * @var TerritorialRepositoryInterface
     */
    private $territorialRepository;

    /**
     * @var HistoricService
     */
    private $historicService;

    /**
     * @var TargetService
     */
    private $targetService;

    /**
     * RedService constructor.
     * @param RedRepositoryInterface $redRepository
     * @param HistoricService $historicService
     * @param TargetService $targetService
     * @param TerritorialRepositoryInterface $territorialRepository
     */
    public function __construct(
        RedRepositoryInterface $redRepository,
        HistoricService $historicService,
        TargetService $targetService,
        TerritorialRepositoryInterface $territorialRepository
    ) {
        $this->redRepository = $redRepository;
        $this->historicService = $historicService;
        $this->targetService = $targetService;
        $this->territorialRepository = $territorialRepository;
    }

    /**
     * @param array $redRows
     * @param ChallengeInterface $challenge
     * @param HistoricInterface $historic
     */
    public function process(array $redRows, ChallengeInterface $challenge, HistoricInterface $historic)
    {
        foreach ($redRows as $row) {
            $id = $this->extractBSId($row);
            try {
                $red = $this->redRepository->findOneByBankSabadellIdOrFail(
                    array('bancsabadellId' => $id)
                );
            } catch (RedNotFoundException $e) {
                $red = $this->create($id, $row);
            }

            if (!$this->historicService->createOrUpdateAccumulated($row, $red, $challenge)) {
                $target = $this->targetService->getTargetOrCreate($row[CSV::POS_TARGET], $row[CSV::POS_TARGET_DESCR]);
                $this->historicService->create($red, $historic, $target, $row);
            }
        }
    }

    /**
     * @param $row
     * @return mixed
     */
    public static function extractBSId($row)
    {
        $idBS =  $row[CSV::POS_RED_BS_ID];
        $prefixBs = Historic::PREFIX_BANK_ID;
        return (int)substr((int)$idBS, strlen($prefixBs));
    }

    /**
     * @param $id
     * @param $row
     * @return Red
     */
    public function create($id, $row)
    {
        $territorial = $this->territorialRepository->findOneByBankSabadellIdOrFail(TerritorialService::extractBSId($row));

        $red = new Red();
        $red->create($id, $row[CSV::POS_RED_NAME], $territorial);

        $this->redRepository->createOrUpdate($red);

        return $red;
    }
}
