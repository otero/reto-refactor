<?php

namespace RetoApiBundle\Domain\Service;


use RetoApiBundle\Entity\Target;
use RetoApiBundle\Repository\Interfaces\TargetRepositoryInterface;
use RetoApiBundle\Domain\Exception\Target\TargetNotFoundException;

class TargetService
{
    private $targetRepository;

    public function __construct(TargetRepositoryInterface $targetRepository)
    {
        $this->targetRepository = $targetRepository;
    }

    public function getTargetOrCreate($bsIndicador, $name)
    {
        try {
            $target = $this->targetRepository->findOneByBsIndicatorOrFail($bsIndicador);
        } catch (TargetNotFoundException $e) {

            $target = new Target();
            $target->create(
                $bsIndicador,
                $name
            );

            $this->targetRepository->createOrUpdate($target);
        }

        return $target;
    }
}
