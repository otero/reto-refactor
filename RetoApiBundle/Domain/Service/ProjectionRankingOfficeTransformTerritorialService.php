<?php

namespace RetoApiBundle\Domain\Service;

use RetoApiBundle\Repository\Interfaces\ProjectionRankingTerritorialRepositoryInterface;
use Doctrine\ORM\EntityManager;

class ProjectionRankingOfficeTransformTerritorialService
{
    /**
     * @var ProjectionRankingTerritorialRepositoryInterface
     */
    private $projectionRankingTerritorialRepository;

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(
        ProjectionRankingTerritorialRepositoryInterface $projectionRakingTerritorialRepository,
        EntityManager $entityManager
    )
    {
        $this->projectionRankingTerritorialRepository = $projectionRakingTerritorialRepository;
        $this->em = $entityManager;
    }

    public function generate($officeTransformTerritorialRanking)
    {
        foreach ($officeTransformTerritorialRanking as $ranking) {

            $projectionRankingTerritorial = $this->projectionRankingTerritorialRepository
                ->findOneByHistoricTerritorialAccumulatedOrFail($ranking['historic_territorial_accumulated_id']);

            $position = $projectionRankingTerritorial::LAST_UP;

            if ($ranking['ranking'] > $projectionRankingTerritorial->getRankingOfficesTransform()) {
                $position = $projectionRankingTerritorial::LAST_DOWN;
            }

            if ($ranking['ranking'] == $projectionRankingTerritorial->getRankingOfficesTransform()) {
                $position = $projectionRankingTerritorial::LAST_EQUAL;
            }

            if ($projectionRankingTerritorial->getRankingOfficesTransform() === null) {
                $position = $projectionRankingTerritorial::LAST_EQUAL;
            }

            $projectionRankingTerritorial->updateRankingOfficesTransform(
                $ranking['ranking'],
                $position
            );

            $this->projectionRankingTerritorialRepository
                ->createOrUpdate($projectionRankingTerritorial);
        }
        $this->em->flush();
        $this->em->clear();

    }
}
