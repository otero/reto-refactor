<?php

namespace RetoApiBundle\Domain\Service;


use RetoApiBundle\Entity\CSV;
use RetoApiBundle\Entity\Regional;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\Interfaces\HistoricInterface;
use RetoApiBundle\Entity\Interfaces\ChallengeInterface;
use RetoApiBundle\Repository\Interfaces\RedRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\RegionalRepositoryInterface;
use RetoApiBundle\Domain\Exception\Regional\RegionalNotFoundException;

class RegionalService
{
    /**
     * @var RedRepositoryInterface
     */
    private $redRepository;

    /**
     * @var RegionalRepositoryInterface
     */
    private $regionalRepository;

    /**
     * @var HistoricService
     */
    private $historicService;

    /**
     * @var TargetService
     */
    private $targetService;

    /**
     * RegionalService constructor.
     * @param RegionalRepositoryInterface $regionalRepository
     * @param HistoricService $historicService
     * @param TargetService $targetService
     * @param RedRepositoryInterface $redRepository
     */
    public function __construct(
        RegionalRepositoryInterface $regionalRepository,
        HistoricService $historicService,
        TargetService $targetService,
        RedRepositoryInterface $redRepository
    ) {
        $this->regionalRepository = $regionalRepository;
        $this->historicService = $historicService;
        $this->targetService = $targetService;
        $this->redRepository = $redRepository;
    }

    /**
     * @param array $regionalRows
     * @param ChallengeInterface $challenge
     * @param HistoricInterface $historic
     */
    public function process(array $regionalRows, ChallengeInterface $challenge, HistoricInterface $historic)
    {
        foreach ($regionalRows as $row) {
            $id = $this->extractBSId($row);
            try {
                $regional = $this->regionalRepository->findOneByBankSabadellIdOrFail(
                    array('bancsabadellId' => $id)
                );
            } catch (RegionalNotFoundException $e) {
                $regional = $this->create($id, $row);
            }

            if (!$this->historicService->createOrUpdateAccumulated($row, $regional, $challenge)) {
                $target = $this->targetService->getTargetOrCreate($row[CSV::POS_TARGET], $row[CSV::POS_TARGET_DESCR]);
                $this->historicService->create($regional, $historic, $target, $row);
            }
        }
    }

    /**
     * @param $row
     * @return mixed
     */
    public static function extractBSId($row)
    {
        $idBS =  $row[CSV::POS_REGIONAL_BS_ID];
        $prefixBs = Historic::PREFIX_BANK_ID;
        return (int)substr((int)$idBS, strlen($prefixBs));
    }

    /**
     * @param $id
     * @param $row
     * @return Regional
     */
    public function create($id, $row)
    {
        $red = $this->redRepository->findOneByBankSabadellIdOrFail(RedService::extractBSId($row));

        $regional = new Regional();
        $regional->create($id, $row[CSV::POS_REGIONAL_NAME], $red);

        $this->regionalRepository->createOrUpdate($regional);

        return $regional;
    }
}
