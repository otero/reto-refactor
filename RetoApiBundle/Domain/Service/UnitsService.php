<?php

namespace RetoApiBundle\Domain\Service;


use RetoApiBundle\Entity\Interfaces\HistoricInterface;
use RetoApiBundle\Entity\Interfaces\ChallengeInterface;
use RetoApiBundle\Domain\Service\Interfaces\UnitsServiceInterface;
use Doctrine\ORM\EntityManager;

class UnitsService implements UnitsServiceInterface
{
    /**
     * @var TerritorialService
     */
    private $territorialService;

    /**
     * @var RedService
     */
    private $redService;

    /**
     * @var RegionalService
     */
    private $regionalService;

    /**
     * @var OfficeRetoService
     */
    private $officeRetoService;

    /**
     * @var OfficeRetoService
     */
    private $em;

    /**
     * UnitsService constructor.
     * @param TerritorialService $territorialService
     * @param RedService $redService
     * @param RegionalService $regionalService
     * @param OfficeRetoService $officeRetoService
     */
    public function __construct(
        TerritorialService $territorialService,
        RedService $redService,
        RegionalService $regionalService,
        OfficeRetoService $officeRetoService,
        EntityManager $entityManager
    ) {
        $this->territorialService = $territorialService;
        $this->redService = $redService;
        $this->regionalService = $regionalService;
        $this->officeRetoService = $officeRetoService;
        $this->em = $entityManager;
    }

    /**
     * @param HistoricInterface $historic
     * @param ChallengeInterface $challenge
     * @param array $units
     */
    public function process(HistoricInterface $historic, ChallengeInterface $challenge, array $units)
    {
        foreach ($units as $key => $value)
        {
           switch ($key) {
               case $historic::UNIT_TERRITORIAL:
                   $this->territorialService->process($value, $challenge, $historic);
                   break;
               case $historic::UNIT_RED:
                   $this->redService->process($value, $challenge, $historic);
                   break;
               case $historic::UNIT_REGIONAL:
                   $this->regionalService->process($value, $challenge, $historic);
                   break;
               case $historic::UNIT_OFFICERETO:
                   $this->officeRetoService->process($value, $challenge, $historic);
                   break;
           }
           $this->em->flush();

        }

    }
}
