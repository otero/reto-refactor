<?php

namespace RetoApiBundle\Domain\Service\Interfaces;


interface FileServiceInterface
{
    public function generateFileCsv($fileName);
}
