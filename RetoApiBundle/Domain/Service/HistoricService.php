<?php

namespace RetoApiBundle\Domain\Service;


use RetoApiBundle\Entity\CSV;
use RetoApiBundle\Entity\Red;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\Regional;
use RetoApiBundle\Entity\OfficeReto;
use RetoApiBundle\Entity\Territorial;
use RetoApiBundle\Domain\Common\Utils;

class HistoricService
{
    /**
     * @var HistoricTerritorialService
     */
    protected $historicTerritorialService;

    /**
     * @var HistoricRedService
     */
    protected $historicRedService;

    /**
     * @var HistoricRegionalService
     */
    protected $historicRegionalService;

    /**
     * @var HistoricOfficeRetoService
     */
    protected $historicOfficeRetoService;

    /**
     * HistoricService constructor.
     * @param HistoricTerritorialService $historicTerritorialService
     * @param HistoricRedService $historicRedService
     * @param HistoricRegionalService $historicRegionalService
     * @param HistoricOfficeRetoService $historicOfficeRetoService
     */
    public function __construct(
        HistoricTerritorialService $historicTerritorialService,
        HistoricRedService $historicRedService,
        HistoricRegionalService $historicRegionalService,
        HistoricOfficeRetoService $historicOfficeRetoService
    ) {
        $this->historicTerritorialService = $historicTerritorialService;
        $this->historicRedService = $historicRedService;
        $this->historicRegionalService = $historicRegionalService;
        $this->historicOfficeRetoService = $historicOfficeRetoService;
    }

    public function create($unit, $historic, $target, $row)
    {
        $class = get_class($unit);
        $accomplishment = Utils::transformStringToFloat($row[CSV::POS_TARGET_ACCMP]);

        switch ($class)
        {
            case Territorial::class:
                $this->historicTerritorialService->create($unit, $historic, $target, $accomplishment);
                break;
            case Red::class:
                $this->historicRedService->create($unit, $historic, $target, $accomplishment);
                break;
            case Regional::class:
                $this->historicRegionalService->create($unit, $historic, $target, $accomplishment);
                break;
            case OfficeReto::class:
                $this->historicOfficeRetoService->create($unit, $historic, $target, $accomplishment);
                break;
        }
    }

    public function createOrUpdateAccumulated($row, $unit, $challenge)
    {
        if ($row[CSV::POS_TARGET] == 0) {
            $data = $this->generateData($row);

            $class = get_class($unit);

            switch ($class)
            {
                case Territorial::class:
                    $this->historicTerritorialService->createOrUpdateAccumulated($unit, $challenge, $data);
                    break;
                case Red::class:
                    $this->historicRedService->createOrUpdateAccumulated($unit, $challenge, $data);
                    break;
                case Regional::class:
                    $this->historicRegionalService->createOrUpdateAccumulated($unit, $challenge, $data);
                    break;
                case OfficeReto::class:
                    $this->historicOfficeRetoService->createOrUpdateAccumulated($unit, $challenge, $data);
                    break;
            }

            return true;
        }

        return false;
    }

    /**
     * @param $data
     * @return array
     */
    public function generateData($data)
    {
        return array(
            Historic::ACC_ANUAL => Utils::transformStringToFloat($data[CSV::POS_ACCMP_ACUMULAT]),
            Historic::ACC_TRIM1 => Utils::transformStringToFloat($data[CSV::POS_ACCMP_TRIM_1]),
            Historic::ACC_TRIM2 => Utils::transformStringToFloat($data[CSV::POS_ACCMP_TRIM_2]),
            Historic::ACC_TRIM3 => Utils::transformStringToFloat($data[CSV::POS_ACCMP_TRIM_3]),
            Historic::ACC_TRIM4 => Utils::transformStringToFloat($data[CSV::POS_ACCMP_TRIM_4])
        );
    }
}
