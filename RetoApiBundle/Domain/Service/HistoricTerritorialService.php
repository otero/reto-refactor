<?php

namespace RetoApiBundle\Domain\Service;


use Psr\Log\LoggerInterface;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Entity\HistoricTerritorial;
use RetoApiBundle\Entity\Interfaces\ChallengeInterface;
use RetoApiBundle\Entity\HistoricTerritorialAccumulated;
use RetoApiBundle\Entity\Interfaces\TerritorialInterface;
use RetoApiBundle\Repository\Interfaces\HistoricTerritorialRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\HistoricTerritorialAccumulatedRepositoryInterface;
use RetoApiBundle\Domain\Exception\HistoricTerritorialAccumulated\HistoricTerritorialAccumulatedNotFoundException;

class HistoricTerritorialService
{
    /**
     * @var HistoricTerritorialAccumulatedRepositoryInterface
     */
    private $historicTerritorialAccumulatedRepository;

    /**
     * @var HistoricTerritorialRepositoryInterface
     */
    private $historicTerritorialRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * HistoricTerritorialService constructor.
     * @param HistoricTerritorialAccumulatedRepositoryInterface $historicTerritorialAccumulatedRepository
     * @param HistoricTerritorialRepositoryInterface $historicTerritorialRepository
     */
    public function __construct
    (
        HistoricTerritorialAccumulatedRepositoryInterface $historicTerritorialAccumulatedRepository,
        HistoricTerritorialRepositoryInterface $historicTerritorialRepository,
        LoggerInterface $logger
    ) {
        $this->historicTerritorialAccumulatedRepository = $historicTerritorialAccumulatedRepository;
        $this->historicTerritorialRepository = $historicTerritorialRepository;
        $this->logger = $logger;
    }

    public function create($unit, $historic, $target, $accomplishment)
    {
        try {
            $isExist = $this->historicTerritorialRepository->findOneByHistoricAndTarget($historic, $target, $unit);

            if ($isExist == null) {
                $historicTerritorial = new HistoricTerritorial();
                $historicTerritorial->create($unit, $historic, $target, $accomplishment, 0);

                $this->historicTerritorialRepository->createOrUpdate($historicTerritorial);
            }

        } catch (\Doctrine\DBAL\DBALException $e) {

           $this->logger($unit, $historic, $target, $accomplishment, $e->getMessage());
        }
    }

    /**
     * @param TerritorialInterface $unit
     * @param ChallengeInterface $challenge
     * @param array $data
     */
    public function createOrUpdateAccumulated(TerritorialInterface $unit, ChallengeInterface $challenge, array $data)
    {
        try {
            $historicTerritorialAccumulated = $this->historicTerritorialAccumulatedRepository->findOneByBankSabadellIdAndChallengeOrFail(
                $unit->getBancsabadellId(),
                $challenge->getId()
            );

            $historicTerritorialAccumulated->update(
                $unit,
                $challenge,
                $unit->getBancsabadellId(),
                $data[Historic::ACC_TRIM1],
                $data[Historic::ACC_TRIM2],
                $data[Historic::ACC_TRIM3],
                $data[Historic::ACC_TRIM4],
                $data[Historic::ACC_ANUAL]
            );

        } catch (HistoricTerritorialAccumulatedNotFoundException $e) {

            $historicTerritorialAccumulated = new HistoricTerritorialAccumulated();

            $historicTerritorialAccumulated->create(
                $unit,
                $challenge,
                $unit->getBancsabadellId(),
                $data[Historic::ACC_TRIM1],
                $data[Historic::ACC_TRIM2],
                $data[Historic::ACC_TRIM3],
                $data[Historic::ACC_TRIM4],
                $data[Historic::ACC_ANUAL]
            );
        }

        $this->historicTerritorialAccumulatedRepository->createorUpdate($historicTerritorialAccumulated);
    }

    private function logger($unit, $historic, $target, $accomplishment, $exceptionMessage)
    {
        $this->logger->error($exceptionMessage, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array(
                'unit' => array(
                    'class' => get_class($unit),
                    'unit_id' => $unit->getId(),
                    'unit_name' => $unit->getName()
                ),
                'historic' => array(
                    'class' => get_class($historic),
                    'historic_id' => $historic->getId()
                ),
                'target' => array(
                    'class' => get_class($target),
                    'target_id' => $target->getId(),
                    'target_name' => $target->getName()
                ),
                'accomplishment' => $accomplishment
            )
        ));
    }
}
