<?php

namespace RetoApiBundle\Domain\Service;


use RetoApiBundle\Entity\ProjectionRankingOffice;
use RetoApiBundle\Entity\ProjectionRankingOfficeLast;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingOfficeRepositoryInterface;
use RetoApiBundle\Domain\Exception\ProjectionRankingOffice\ProjectionRankingOfficeException;
use Doctrine\ORM\EntityManager;

final class ProjectionRankingOfficeService
{
    /**
     * @var HistoricOfficeAccumulatedRepositoryInterface
     */
    private $historicOfficeAccumulatedRepository;

    /**
     * @var ProjectionRankingOfficeRepositoryInterface
     */
    private $projectionRankingOfficeRepository;

    /**
     * @var ProjectionRankingOfficeLastRepository
     */
    private $projectionRankingOfficeLastRepository;

    /**
     * @var em
     */
    private $em;
    /**
     * ProjectionRankingOfficeService constructor.
     * @param HistoricOfficeAccumulatedRepositoryInterface $historicOfficeAccumulatedRepository
     * @param ProjectionRankingOfficeRepositoryInterface $projectionRankingOfficeRepository
     * @param ProjectionRankingOfficeLastRepository $projectionRankingOfficeLastRepository
     */
    public function __construct(
        $historicOfficeAccumulatedRepository,
        ProjectionRankingOfficeRepositoryInterface $projectionRankingOfficeRepository,
        $projectionRankingOfficeLastRepository,
        EntityManager $entityManager

    ) {
        $this->historicOfficeAccumulatedRepository = $historicOfficeAccumulatedRepository;
        $this->projectionRankingOfficeRepository = $projectionRankingOfficeRepository;
        $this->projectionRankingOfficeLastRepository = $projectionRankingOfficeLastRepository;
        $this->em = $entityManager;
    }

    public function generate($reds)
    {
        $officePlatinum = $this->historicOfficeAccumulatedRepository->findTop1Ranking();

        foreach ($reds as $red) {
            $results = $this->historicOfficeAccumulatedRepository
                ->findRankingByRedLessPlatinum($red['id']);
            $indexTopRanking = 1;

            foreach ($results as $result) {
                $isRedPlatinum = false;

                if ($officePlatinum[0]['red_id'] === $result['red_id']) {
                  $isRedPlatinum = true;
                }

                try {
                    $projectionRankingOffice = $this->projectionRankingOfficeRepository
                        ->findByHistoricOfficeAccumulatedOrFail($result['office_id']);

                    $projectionRankingOffice->update(
                        $result['historic_office_accumulated_id'],
                        $result['office_id'],
                        $result['office_name'],
                        $result['regional_id'],
                        $result['regional_name'],
                        $result['territorial_id'],
                        $result['territorial_name'],
                        $result['red_id'],
                        $result['red_name'],
                        $result['ranking'],
                        $result['historic_office_accumulated_trim1'],
                        $result['historic_office_accumulated_trim2'],
                        $result['historic_office_accumulated_trim3'],
                        $result['historic_office_accumulated_trim4'],
                        $result['historic_office_accumulated_annual'],
                        $indexTopRanking,
                        $isRedPlatinum
                    );

                } catch (ProjectionRankingOfficeException $e) {
                    $projectionRankingOffice = new ProjectionRankingOffice();
                    $projectionRankingOffice->create(
                        $result['historic_office_accumulated_id'],
                        $result['office_id'],
                        $result['office_name'],
                        $result['regional_id'],
                        $result['regional_name'],
                        $result['territorial_id'],
                        $result['territorial_name'],
                        $result['red_id'],
                        $result['red_name'],
                        $result['ranking'],
                        $result['historic_office_accumulated_trim1'],
                        $result['historic_office_accumulated_trim2'],
                        $result['historic_office_accumulated_trim3'],
                        $result['historic_office_accumulated_trim4'],
                        $result['historic_office_accumulated_annual'],
                        $indexTopRanking,
                        $isRedPlatinum
                    );
                }

                $this->projectionRankingOfficeRepository->createOrUpdate($projectionRankingOffice);
                $indexTopRanking++;
            }
        }
        $this->em->flush();
        $this->em->clear();
    }

    /**
     * 1. read CpmAnnualLast and RakningLast from _last entity
     * 2. update office enttity
     * 3. update _last entity
     */
    public function generateLast()
    {
        $offices = $this->projectionRankingOfficeRepository->findAll();

        foreach ($offices as $office) {

            $result = $this->projectionRankingOfficeLastRepository
                ->findOneByOfficeId($office->getOfficeId());

            if($result === NULL) {

                // CREATE LAST
                $projectionRankingOfficeLast = new ProjectionRankingOfficeLast();
                $projectionRankingOfficeLast->create(
                    $office->getOfficeId(),
                    $office->getRanking(),
                    $office->getCmpAnnual()
                );

                $this->projectionRankingOfficeLastRepository->createOrUpdate($projectionRankingOfficeLast);
            }
            else {

                // READ LAST [UPDATE OFFICE]

                $projectionRankingOffice = $this->projectionRankingOfficeRepository
                    ->findOneByOfficeId($office->getOfficeId());

                $projectionRankingOffice->setRankingLast($result->getRankingLast());
                $projectionRankingOffice->setCmpAnnualLast($result->getCmpAnnualLast());

                $this->projectionRankingOfficeRepository->createOrUpdate($projectionRankingOffice);

                // UPDATE LAST
                $projectionRankingOfficeLast = $this->projectionRankingOfficeLastRepository
                    ->findOneByOfficeId($result->getOfficeId());

                $projectionRankingOfficeLast->update(
                    $office->getOfficeId(),
                    $office->getRanking(),
                    $office->getCmpAnnual()
                );

                $this->projectionRankingOfficeLastRepository->createOrUpdate($projectionRankingOfficeLast);
            }

        }
        $this->em->flush();
        $this->em->clear();
    }
}
