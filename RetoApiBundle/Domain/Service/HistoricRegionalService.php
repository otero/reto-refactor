<?php

namespace RetoApiBundle\Domain\Service;


use Doctrine\DBAL\DBALException;
use Psr\Log\LoggerInterface;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Entity\HistoricRegional;
use RetoApiBundle\Entity\HistoricRegionalAccumulated;
use RetoApiBundle\Entity\Interfaces\RegionalInterface;
use RetoApiBundle\Entity\Interfaces\ChallengeInterface;
use RetoApiBundle\Repository\Interfaces\HistoricRegionalRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\HistoricRegionalAccumulatedRepositoryInterface;
use RetoApiBundle\Domain\Exception\HistoricRegionalAccumulated\HistoricRegionalAccumulatedNotFoundException;

class HistoricRegionalService
{
    private $historicRegionalAccumulatedRepository;

    private $historicRegionalRepository;

    protected $logger;

    public function __construct
    (
        HistoricRegionalAccumulatedRepositoryInterface $historicRegionalAccumulatedRepository,
        HistoricRegionalRepositoryInterface $historicRegionalRepository,
        LoggerInterface $logger
    ) {
        $this->historicRegionalAccumulatedRepository = $historicRegionalAccumulatedRepository;
        $this->historicRegionalRepository = $historicRegionalRepository;
        $this->logger = $logger;
    }

    public function create($unit, $historic, $target, $accomplishment)
    {
        try {
            $isExist = $this->historicRegionalRepository->findOneByHistoricAndTarget($historic, $target, $unit);

            if ($isExist == null) {
                $historicRegional = new HistoricRegional();
                $historicRegional->create($unit, $historic, $target, $accomplishment, 0);

                $this->historicRegionalRepository->createOrUpdate($historicRegional);
            }

        } catch (DBALException $e) {

            $this->logger($unit, $historic, $target, $accomplishment, $e->getMessage());
        }
    }

    public function createOrUpdateAccumulated(RegionalInterface $unit, ChallengeInterface $challenge, array $data)
    {
        try {
            $historicRegionalAccumulated = $this->historicRegionalAccumulatedRepository->findOneByBankSabadellIdAndChallengeOrFail(
                $unit->getBancsabadellId(),
                $challenge->getId()
            );

            $historicRegionalAccumulated->update(
                $unit,
                $challenge,
                $unit->getBancsabadellId(),
                $data[Historic::ACC_TRIM1],
                $data[Historic::ACC_TRIM2],
                $data[Historic::ACC_TRIM3],
                $data[Historic::ACC_TRIM4],
                $data[Historic::ACC_ANUAL]
            );

        } catch (HistoricRegionalAccumulatedNotFoundException $e) {
            $historicRegionalAccumulated = new HistoricRegionalAccumulated();

            $historicRegionalAccumulated->create(
                $unit,
                $challenge,
                $unit->getBancsabadellId(),
                $data[Historic::ACC_TRIM1],
                $data[Historic::ACC_TRIM2],
                $data[Historic::ACC_TRIM3],
                $data[Historic::ACC_TRIM4],
                $data[Historic::ACC_ANUAL]
            );
        }

        $this->historicRegionalAccumulatedRepository->createorUpdate($historicRegionalAccumulated);
    }

    private function logger($unit, $historic, $target, $accomplishment, $exceptionMessage)
    {
        $this->logger->error($exceptionMessage, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array(
                'unit' => array(
                    'class' => get_class($unit),
                    'unit_id' => $unit->getId(),
                    'unit_name' => $unit->getName()
                ),
                'historic' => array(
                    'class' => get_class($historic),
                    'historic_id' => $historic->getId()
                ),
                'target' => array(
                    'class' => get_class($target),
                    'target_id' => $target->getId(),
                    'target_name' => $target->getName()
                ),
                'accomplishment' => $accomplishment
            )
        ));
    }
}
