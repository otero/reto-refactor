<?php

namespace RetoApiBundle\Domain\Service;


use Doctrine\DBAL\DBALException;
use Psr\Log\LoggerInterface;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Entity\HistoricOffice;
use RetoApiBundle\Entity\HistoricOfficeAccumulated;
use RetoApiBundle\Entity\Interfaces\OfficeRetoInterface;
use RetoApiBundle\Entity\Interfaces\ChallengeInterface;
use RetoApiBundle\Repository\Interfaces\HistoricOfficeRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\HistoricOfficeAccumulatedRepositoryInterface;
use RetoApiBundle\Domain\Exception\HistoricOfficeRetoAccumulated\HistoricOfficeRetoAccumulatedNotFoundException;

class HistoricOfficeRetoService
{
    private $historicOfficeRetoAccumulatedRepository;

    private $historicOfficeRetoRepository;

    protected $logger;

    public function __construct
    (
        HistoricOfficeAccumulatedRepositoryInterface $historicOfficeRetoAccumulatedRepository,
        HistoricOfficeRepositoryInterface $historicOfficeRetoRepository,
        LoggerInterface $logger
    ) {
        $this->historicOfficeRetoAccumulatedRepository = $historicOfficeRetoAccumulatedRepository;
        $this->historicOfficeRetoRepository = $historicOfficeRetoRepository;
        $this->logger = $logger;
    }

    public function create($unit, $historic, $target, $accomplishment)
    {
        try {
            $isExist = $this->historicOfficeRetoRepository->findOneByHistoricAndTarget($historic, $target, $unit);

            if ($isExist == null) {
                $historicOfficeReto = new HistoricOffice();
                $historicOfficeReto->create($unit, $historic, $target, $accomplishment, 0);

                $this->historicOfficeRetoRepository->createOrUpdate($historicOfficeReto);
            }
        } catch (DBALException $e) {

            $this->logger($unit, $historic, $target, $accomplishment, $e->getMessage());
        }
    }

    public function createOrUpdateAccumulated(OfficeRetoInterface $unit, ChallengeInterface $challenge, array $data)
    {
        try {
            $historicOfficeRetoAccumulated = $this->historicOfficeRetoAccumulatedRepository->findOneByBankSabadellIdAndChallengeOrFail(
                $unit->getBancsabadellId(),
                $challenge->getId()
            );

            $historicOfficeRetoAccumulated->update(
                $unit,
                $challenge,
                $unit->getBancsabadellId(),
                $data[Historic::ACC_TRIM1],
                $data[Historic::ACC_TRIM2],
                $data[Historic::ACC_TRIM3],
                $data[Historic::ACC_TRIM4],
                $data[Historic::ACC_ANUAL]
            );

        } catch (HistoricOfficeRetoAccumulatedNotFoundException $e) {
            $historicOfficeRetoAccumulated = new HistoricOfficeAccumulated();

            $historicOfficeRetoAccumulated->create(
                $unit,
                $challenge,
                $unit->getBancsabadellId(),
                $data[Historic::ACC_TRIM1],
                $data[Historic::ACC_TRIM2],
                $data[Historic::ACC_TRIM3],
                $data[Historic::ACC_TRIM4],
                $data[Historic::ACC_ANUAL]
            );
        }

        $this->historicOfficeRetoAccumulatedRepository->createorUpdate($historicOfficeRetoAccumulated);
    }

    private function logger($unit, $historic, $target, $accomplishment, $exceptionMessage)
    {
        $this->logger->error($exceptionMessage, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array(
                'unit' => array(
                    'class' => get_class($unit),
                    'unit_id' => $unit->getId(),
                    'unit_name' => $unit->getName()
                ),
                'historic' => array(
                    'class' => get_class($historic),
                    'historic_id' => $historic->getId()
                ),
                'target' => array(
                    'class' => get_class($target),
                    'target_id' => $target->getId(),
                    'target_name' => $target->getName()
                ),
                'accomplishment' => $accomplishment
            )
        ));
    }

}
