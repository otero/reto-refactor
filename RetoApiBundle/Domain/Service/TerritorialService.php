<?php

namespace RetoApiBundle\Domain\Service;


use RetoApiBundle\Entity\CSV;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\Territorial;
use RetoApiBundle\Entity\Interfaces\HistoricInterface;
use RetoApiBundle\Entity\Interfaces\ChallengeInterface;
use RetoApiBundle\Repository\Interfaces\TerritorialRepositoryInterface;
use RetoApiBundle\Domain\Exception\Territorial\TerritorialNotFoundException;

class TerritorialService
{
    /**
     * @var TerritorialRepositoryInterface
     */
    private $territorialRepository;

    /**
     * @var HistoricService
     */
    private $historicService;

    /**
     * @var TargetService
     */
    private $targetService;

    /**
     * TerritorialService constructor.
     * @param TerritorialRepositoryInterface $territorialRepository
     * @param HistoricService $historicService
     * @param TargetService $targetService
     */
    public function __construct(
        TerritorialRepositoryInterface $territorialRepository,
        HistoricService $historicService,
        TargetService $targetService
    ) {
        $this->territorialRepository = $territorialRepository;
        $this->historicService = $historicService;
        $this->targetService = $targetService;
    }

    /**
     * @param array $territorialRows
     * @param ChallengeInterface $challenge
     * @param HistoricInterface $historic
     */
    public function process(array $territorialRows, ChallengeInterface $challenge, HistoricInterface $historic)
    {
        foreach ($territorialRows as $row) {
            $id = $this->extractBSId($row);
            try {
                $territorial = $this->territorialRepository->findOneByBankSabadellIdOrFail(
                    array('bancsabadellId' => $id)
                );
            } catch (TerritorialNotFoundException $e) {
                $territorial = $this->create($id, $row);
            }

            if (!$this->historicService->createOrUpdateAccumulated($row, $territorial, $challenge)) {
                $target = $this->targetService->getTargetOrCreate($row[CSV::POS_TARGET], $row[CSV::POS_TARGET_DESCR]);
                $this->historicService->create($territorial, $historic, $target, $row);
            }
        }
    }

    /**
     * @param $row
     * @return mixed
     */
    public static function extractBSId($row)
    {
        $idBS =  $row[CSV::POS_TERRITORIAL_BS_ID];
        $prefixBs = Historic::PREFIX_BANK_ID;
        return (int)substr((int)$idBS, strlen($prefixBs));
    }

    /**
     * @param $id
     * @param $row
     * @return Territorial
     */
    public function create($id, $row)
    {
        $territorial = new Territorial();
        $territorial->create($id, $row[CSV::POS_TERRITORIAL_NAME]);

        $this->territorialRepository->createOrUpdate($territorial);

        return $territorial;
    }
}
