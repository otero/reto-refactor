<?php

namespace RetoApiBundle\Domain\Service;


use RetoApiBundle\Entity\ProjectionRankingTerritorial;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingOfficeRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingTerritorialRepositoryInterface;
use RetoApiBundle\Domain\Exception\ProjectionRankingTerritorial\ProjectionRankingTerritorialException;
use Doctrine\ORM\EntityManager;

final class ProjectionRankingTerritorialService
{
    /**
     * @var ProjectionRankingOfficeRepositoryInterface
     */
    private $projectionRankingOfficeRepository;

    /**
     * @var ProjectionRankingTerritorialRepositoryInterface
     */
    private $projectionRankingTerritorialRepository;

    /**
     * @var ProjectionRankingTerritorialRepositoryInterface
     */
    private $em;

    public function __construct(
        ProjectionRankingOfficeRepositoryInterface $projectionRankingOfficeRepository,
        ProjectionRankingTerritorialRepositoryInterface $projectionRakingTerritorialRepository,
        EntityManager $entityManager
    ) {
        $this->projectionRankingOfficeRepository = $projectionRankingOfficeRepository;
        $this->projectionRankingTerritorialRepository = $projectionRakingTerritorialRepository;
        $this->em = $entityManager;
    }

    public function generate($territorialRanking)
    {
        foreach ($territorialRanking as $territorial) {
            $officesTransform = $this->projectionRankingOfficeRepository->findOfficesTransformByTerritorial($territorial['territorial_id']);

            try {
                $projectionRankingTerritorial = $this->projectionRankingTerritorialRepository
                    ->findOneByHistoricTerritorialAccumulatedOrFail($territorial['historic_territorial_accumulated_id']);

                $projectionRankingTerritorial->update(
                    $territorial['historic_territorial_accumulated_id'],
                    $territorial['territorial_id'],
                    $territorial['territorial_name'],
                    $territorial['ranking'],
                    $territorial['historic_territorial_accumulated_annual'],
                    $territorial['historic_territorial_accumulated_trim1'],
                    $territorial['historic_territorial_accumulated_trim2'],
                    $territorial['historic_territorial_accumulated_trim3'],
                    $territorial['historic_territorial_accumulated_trim4'],
                    $officesTransform[0]['count_offices_transform'] / $officesTransform[0]['count_total_offices']
                );
            } catch (ProjectionRankingTerritorialException $e) {

                $projectionRankingTerritorial = new ProjectionRankingTerritorial();
                $projectionRankingTerritorial->create(
                    $territorial['historic_territorial_accumulated_id'],
                    $territorial['territorial_id'],
                    $territorial['territorial_name'],
                    $territorial['ranking'],
                    $territorial['historic_territorial_accumulated_annual'],
                    $territorial['historic_territorial_accumulated_trim1'],
                    $territorial['historic_territorial_accumulated_trim2'],
                    $territorial['historic_territorial_accumulated_trim3'],
                    $territorial['historic_territorial_accumulated_trim4'],
                    $officesTransform[0]['count_offices_transform'] / $officesTransform[0]['count_total_offices']
                );
            }

            $this->projectionRankingTerritorialRepository->createOrUpdate($projectionRankingTerritorial);
        }
        $this->em->flush();
        $this->em->clear();
    }
}
