<?php

namespace RetoApiBundle\Domain\Service;


use Doctrine\DBAL\DBALException;
use Psr\Log\LoggerInterface;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Entity\HistoricRed;
use RetoApiBundle\Entity\HistoricRedAccumulated;
use RetoApiBundle\Entity\Interfaces\RedInterface;
use RetoApiBundle\Entity\Interfaces\ChallengeInterface;
use RetoApiBundle\Repository\Interfaces\HistoricRedRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\HistoricRedAccumulatedRepositoryInterface;
use RetoApiBundle\Domain\Exception\HistoricRedAccumulated\HistoricRedAccumulatedNotFoundException;

class HistoricRedService
{
    private $historicRedAccumulatedRepository;

    private $historicRedRepository;

    protected $logger;

    public function __construct
    (
        HistoricRedAccumulatedRepositoryInterface $historicRedAccumulatedRepository,
        HistoricRedRepositoryInterface $historicRedRepository,
        LoggerInterface $logger
    ) {
        $this->historicRedAccumulatedRepository = $historicRedAccumulatedRepository;
        $this->historicRedRepository = $historicRedRepository;
        $this->logger = $logger;
    }

    public function create($unit, $historic, $target, $accomplishment)
    {
        try {
            $isExist = $this->historicRedRepository->findOneByHistoricAndTarget($historic, $target, $unit);

            if ($isExist == null) {
                $historicRed = new HistoricRed();
                $historicRed->create($unit, $historic, $target, $accomplishment, 0);

                $this->historicRedRepository->createOrUpdate($historicRed);
            }
        } catch (DBALException $e) {

            $this->logger($unit, $historic, $target, $accomplishment, $e->getMessage());
        }
    }

    public function createOrUpdateAccumulated(RedInterface $unit, ChallengeInterface $challenge, array $data)
    {
        try {
            $historicRedAccumulated = $this->historicRedAccumulatedRepository->findOneByBankSabadellIdAndChallengeOrFail(
                $unit->getBancsabadellId(),
                $challenge->getId()
            );

            $historicRedAccumulated->update(
                $unit,
                $challenge,
                $unit->getBancsabadellId(),
                $data[Historic::ACC_TRIM1],
                $data[Historic::ACC_TRIM2],
                $data[Historic::ACC_TRIM3],
                $data[Historic::ACC_TRIM4],
                $data[Historic::ACC_ANUAL]
            );

        } catch (HistoricRedAccumulatedNotFoundException $e) {
            $historicRedAccumulated = new HistoricRedAccumulated();

            $historicRedAccumulated->create(
                $unit,
                $challenge,
                $unit->getBancsabadellId(),
                $data[Historic::ACC_TRIM1],
                $data[Historic::ACC_TRIM2],
                $data[Historic::ACC_TRIM3],
                $data[Historic::ACC_TRIM4],
                $data[Historic::ACC_ANUAL]
            );
        }

        $this->historicRedAccumulatedRepository->createorUpdate($historicRedAccumulated);
    }

    private function logger($unit, $historic, $target, $accomplishment, $exceptionMessage)
    {
        $this->logger->error($exceptionMessage, array(
            'bundle' => HistoricLog::BUNDLE_RETO,
            'channel' => HistoricLog::CHANNEL_CSV,
            'data' => array(
                'unit' => array(
                    'class' => get_class($unit),
                    'unit_id' => $unit->getId(),
                    'unit_name' => $unit->getName()
                ),
                'historic' => array(
                    'class' => get_class($historic),
                    'historic_id' => $historic->getId()
                ),
                'target' => array(
                    'class' => get_class($target),
                    'target_id' => $target->getId(),
                    'target_name' => $target->getName()
                ),
                'accomplishment' => $accomplishment
            )
        ));
    }
}
