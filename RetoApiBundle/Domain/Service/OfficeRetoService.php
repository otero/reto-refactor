<?php

namespace RetoApiBundle\Domain\Service;


use RetoApiBundle\Entity\CSV;
use RetoApiBundle\Entity\OfficeReto;
use RetoApiBundle\Entity\Historic;
use RetoApiBundle\Entity\Interfaces\HistoricInterface;
use RetoApiBundle\Entity\Interfaces\ChallengeInterface;
use RetoApiBundle\Domain\Exception\OfficeReto\OfficeRetoNotFoundException;
use RetoApiBundle\Repository\Interfaces\OfficeRetoRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\RegionalRepositoryInterface;

class OfficeRetoService
{
    /**
     * @var OfficeRetoRepositoryInterface
     */
    private $officeRetoRepository;

    /**
     * @var RegionalRepositoryInterface
     */
    private $regionalRepository;

    /**
     * @var HistoricService
     */
    private $historicService;

    /**
     * @var TargetService
     */
    private $targetService;

    /**
     * RegionalService constructor.
     * @param OfficeRetoRepositoryInterface $officeRetoRepository
     * @param HistoricService $historicService
     * @param TargetService $targetService
     * @param RegionalRepositoryInterface $regionalRepository
     */
    public function __construct(
        OfficeRetoRepositoryInterface $officeRetoRepository,
        HistoricService $historicService,
        TargetService $targetService,
        RegionalRepositoryInterface $regionalRepository
    ) {
        $this->officeRetoRepository = $officeRetoRepository;
        $this->historicService = $historicService;
        $this->targetService = $targetService;
        $this->regionalRepository = $regionalRepository;
    }

    /**
     * @param array $officeRetoRows
     * @param ChallengeInterface $challenge
     * @param HistoricInterface $historic
     */
    public function process(array $officeRetoRows, ChallengeInterface $challenge, HistoricInterface $historic)
    {
        foreach ($officeRetoRows as $row) {
            $id = $this->extractBSId($row);
            try {
                $officeReto = $this->officeRetoRepository->findOneByBankSabadellIdOrFail(
                    array('bancsabadellId' => $id)
                );
            } catch (OfficeRetoNotFoundException $e) {
                $officeReto = $this->create($id, $row);
            }

            if (!$this->historicService->createOrUpdateAccumulated($row, $officeReto, $challenge)) {
                $target = $this->targetService->getTargetOrCreate($row[CSV::POS_TARGET], $row[CSV::POS_TARGET_DESCR]);
                $this->historicService->create($officeReto, $historic, $target, $row);
            }
        }
    }

    /**
     * @param $row
     * @return mixed
     */
    public static function extractBSId($row)
    {
        $csvId = $row[CSV::POS_OFFICE_BS_ID];

        foreach (Historic::OFFICE_PREFIX as $prefix) {
            $lengthExtract = strlen($prefix);
            if (strpos($csvId, $prefix) !== false) {
                return substr($csvId, $lengthExtract, (strlen($csvId)-strlen($lengthExtract)));
            } else {
                $idBS = $csvId;
            }
        }

        $prefixBs = Historic::PREFIX_BANK_ID;
        $idBS = (int)substr((int)$idBS, strlen($prefixBs));
        return $idBS;
    }

    /**
     * @param $id
     * @param $row
     * @return OfficeReto
     */
    public function create($id, $row)
    {
        $regional = $this->regionalRepository->findOneByBankSabadellIdOrFail(RegionalService::extractBSId($row));

        $officeReto = new OfficeReto();
        $officeReto->create($id, $row[CSV::POS_OFFICE_NAME], $regional);

        $this->officeRetoRepository->createOrUpdate($officeReto);

        return $officeReto;
    }
}
