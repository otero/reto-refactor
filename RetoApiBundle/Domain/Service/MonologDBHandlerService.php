<?php

namespace RetoApiBundle\Domain\Service;

use Monolog\Handler\AbstractProcessingHandler;
use RetoApiBundle\Entity\HistoricLog;
use RetoApiBundle\Repository\Interfaces\HistoricLogRepositoryInterface;

class MonologDBHandlerService extends AbstractProcessingHandler
{
    protected $historicLogRepository;

    public function __construct(HistoricLogRepositoryInterface $historicLogRepository)
    {
        $this->historicLogRepository = $historicLogRepository;
    }

    protected function write(array $record)
    {
        $historicLog = new HistoricLog();
        $historicLog->create(
            $record['context']['channel'],
            $record['context']['bundle'],
            $record['formatted'],
            $record['context'],
            $record['level'],
            $record['level_name']
        );

        $this->historicLogRepository->createOrUpdate($historicLog);
    }
}