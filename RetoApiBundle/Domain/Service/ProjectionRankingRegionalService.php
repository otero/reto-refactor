<?php

namespace RetoApiBundle\Domain\Service;


use RetoApiBundle\Entity\ProjectionRankingRegional;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingOfficeRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\ProjectionRankingRegionalRepositoryInterface;
use RetoApiBundle\Domain\Exception\ProjectionRankingRegional\ProjectionRankingRegionalException;
use Doctrine\ORM\EntityManager;

final class ProjectionRankingRegionalService
{
    /**
     * @var ProjectionRankingOfficeRepositoryInterface
     */
    private $projectionRankingOfficeRepository;

    /**
     * @var ProjectionRankingRegionalRepositoryInterface
     */
    private $projectionRankingRegionalRepository;

    /**
     * ProjectionRankingRegionalService constructor.
     * @param ProjectionRankingOfficeRepositoryInterface $projectionRankingOfficeRepository
     * @param ProjectionRankingRegionalRepositoryInterface $projectionRakingRegionalRepository
     */

    /**
     * @var EntityManager;
     */
    private $em;

    public function __construct(
        ProjectionRankingOfficeRepositoryInterface $projectionRankingOfficeRepository,
        ProjectionRankingRegionalRepositoryInterface $projectionRakingRegionalRepository,
        EntityManager $entityManager
    ) {
        $this->projectionRankingOfficeRepository = $projectionRankingOfficeRepository;
        $this->projectionRankingRegionalRepository = $projectionRakingRegionalRepository;
        $this->em = $entityManager;
    }

    public function generate($regionalRanking)
    {
        foreach ($regionalRanking as $regionalAccumulated) {
           $medalOfRegional = $this->projectionRankingOfficeRepository->findMedalsByRegional($regionalAccumulated['regional_id']);

            try {
                $projectionRankingRegional = $this->projectionRankingRegionalRepository
                    ->findOneByHistoricRegionalAccumulatedOrFail($regionalAccumulated['historic_regional_accumulated_id']);

                $projectionRankingRegional->update(
                    $regionalAccumulated['historic_regional_accumulated_id'],
                    $regionalAccumulated['regional_id'],
                    $regionalAccumulated['regional_name'],
                    $regionalAccumulated['red_id'],
                    $regionalAccumulated['red_name'],
                    $regionalAccumulated['territorial_id'],
                    $regionalAccumulated['regional_name'],
                    $regionalAccumulated['ranking'],
                    $medalOfRegional[0]['count_medals_platinum'],
                    $medalOfRegional[0]['count_medals_golden'],
                    $medalOfRegional[0]['count_medals_silver'],
                    $medalOfRegional[0]['count_medals_bronze'],
                    $regionalAccumulated['historic_regional_accumulated_annual'],
                    $regionalAccumulated['historic_regional_accumulated_trim1'],
                    $regionalAccumulated['historic_regional_accumulated_trim2'],
                    $regionalAccumulated['historic_regional_accumulated_trim3'],
                    $regionalAccumulated['historic_regional_accumulated_trim4'],
                    $medalOfRegional[0]['count_offices_transform'] / $medalOfRegional[0]['count_total_offices']
                );

            } catch (ProjectionRankingRegionalException $e) {

                $projectionRankingRegional = new ProjectionRankingRegional();
                $projectionRankingRegional->create(
                    $regionalAccumulated['historic_regional_accumulated_id'],
                    $regionalAccumulated['regional_id'],
                    $regionalAccumulated['regional_name'],
                    $regionalAccumulated['red_id'],
                    $regionalAccumulated['red_name'],
                    $regionalAccumulated['territorial_id'],
                    $regionalAccumulated['regional_name'],
                    $regionalAccumulated['ranking'],
                    $medalOfRegional[0]['count_medals_platinum'],
                    $medalOfRegional[0]['count_medals_golden'],
                    $medalOfRegional[0]['count_medals_silver'],
                    $medalOfRegional[0]['count_medals_bronze'],
                    $regionalAccumulated['historic_regional_accumulated_annual'],
                    $regionalAccumulated['historic_regional_accumulated_trim1'],
                    $regionalAccumulated['historic_regional_accumulated_trim2'],
                    $regionalAccumulated['historic_regional_accumulated_trim3'],
                    $regionalAccumulated['historic_regional_accumulated_trim4'],
                    $medalOfRegional[0]['count_offices_transform'] / $medalOfRegional[0]['count_total_offices']
                );
            }

            $this->projectionRankingRegionalRepository->createOrUpdate($projectionRankingRegional);
        }
        $this->em->flush();
        $this->em->clear();
    }
}
