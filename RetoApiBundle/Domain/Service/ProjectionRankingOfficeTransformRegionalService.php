<?php

namespace RetoApiBundle\Domain\Service;

use RetoApiBundle\Repository\Interfaces\ProjectionRankingRegionalRepositoryInterface;
use Doctrine\ORM\EntityManager;

class ProjectionRankingOfficeTransformRegionalService
{
    /**
     * @var ProjectionRankingRegionalRepositoryInterface
     */
    private $projectionRankingRegionalRepository;

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(
        ProjectionRankingRegionalRepositoryInterface $projectionRakingRegionalRepository,
        EntityManager $entityManager
    )
    {
        $this->projectionRankingRegionalRepository = $projectionRakingRegionalRepository;
        $this->em = $entityManager;
    }

    public function generate($officeTransformRegionalRanking)
    {
        foreach ($officeTransformRegionalRanking as $ranking) {

            $projectionRankingRegional = $this->projectionRankingRegionalRepository
                ->findOneByHistoricRegionalAccumulatedOrFail($ranking['historic_regional_accumulated_id']);

            $position = $projectionRankingRegional::LAST_UP;

            if ($ranking['ranking'] > $projectionRankingRegional->getRankingOfficesTransform()) {
                $position = $projectionRankingRegional::LAST_DOWN;
            }

            if ($ranking['ranking'] == $projectionRankingRegional->getRankingOfficesTransform()) {
                $position = $projectionRankingRegional::LAST_EQUAL;
            }

            if ($projectionRankingRegional->getRankingOfficesTransform() === null) {
                $position = $projectionRankingRegional::LAST_EQUAL;
            }

            $projectionRankingRegional->updateRankingOfficesTransform(
                $ranking['ranking'],
                $position
            );

            $this->projectionRankingRegionalRepository
                ->createOrUpdate($projectionRankingRegional);
        }
        $this->em->flush();
        $this->em->clear();
    }
}
