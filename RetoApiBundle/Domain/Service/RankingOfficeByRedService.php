<?php

namespace RetoApiBundle\Domain\Service;


use RetoApiBundle\Domain\Exception\RankingRed\RankingRedNotFoundException;
use RetoApiBundle\Entity\RankingRed;
use RetoApiBundle\Repository\Interfaces\HistoricOfficeAccumulatedRepositoryInterface;
use RetoApiBundle\Repository\Interfaces\RankingRedRepositoryInterface;
use Doctrine\ORM\EntityManager;

final class RankingOfficeByRedService
{
    /**
     * @var HistoricOfficeAccumulatedRepositoryInterface
     */
    private $historicOfficeAccumulatedRepository;

    /**
     * @var RankingRedRepositoryInterface
     */
    private $rankingRedRepository;

    /**
     * @var RankingRedRepositoryInterface
     */
    private $em;

    /**
     * RankingOfficeByRedService constructor.
     * @param HistoricOfficeAccumulatedRepositoryInterface $historicOfficeAccumulatedRepository
     * @param RankingRedRepositoryInterface $rankingRedRepository
     */

    public function __construct(
      HistoricOfficeAccumulatedRepositoryInterface $historicOfficeAccumulatedRepository,
      RankingRedRepositoryInterface $rankingRedRepository,
      EntityManager $entityManager
    ) {
        $this->historicOfficeAccumulatedRepository = $historicOfficeAccumulatedRepository;
        $this->rankingRedRepository = $rankingRedRepository;
        $this->em = $entityManager;
    }

    public function generate($reds)
    {
        foreach ($reds as $red) {
            $results = $this->historicOfficeAccumulatedRepository->findRankingByRed($red['id']);

            foreach ($results as $result) {
                try {
                    $rankingRed = $this->rankingRedRepository->findByHistoricOfficeAccumulatedOrFail($result['id']);

                    $position = $rankingRed::LAST_UP;

                    if ($result['ranking'] > $rankingRed->getRanking()) {
                        $position = $rankingRed::LAST_DOWN;
                    }

                    if ($result['ranking'] == $rankingRed->getRanking()) {
                        $position = $rankingRed::LAST_EQUAL;
                    }

                    $rankingRed->update(
                        $result['anual'],
                        $result['ranking'],
                        $position
                    );
                } catch (RankingRedNotFoundException $e) {
                    $historicOfficeAccumulated = $this->historicOfficeAccumulatedRepository->findOneById($result['id']);

                    $rankingRed = new RankingRed();
                    $rankingRed->create(
                        $historicOfficeAccumulated,
                        $result['anual'],
                        $result['ranking'],
                        $rankingRed::LAST_EQUAL
                    );
                }

                $this->rankingRedRepository->createOrUpdate($rankingRed);
            }
        }
        $this->em->flush();
        $this->em->clear();
    }
}
