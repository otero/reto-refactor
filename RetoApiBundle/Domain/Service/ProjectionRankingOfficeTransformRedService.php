<?php

namespace RetoApiBundle\Domain\Service;

use RetoApiBundle\Repository\Interfaces\ProjectionRankingRedRepositoryInterface;
use Doctrine\ORM\EntityManager;

class ProjectionRankingOfficeTransformRedService
{
    /**
     * @var ProjectionRankingRedRepositoryInterface
     */
    private $projectionRankingRedRepository;

    /**
     * @var EntityManager
     */

    private $em;

    public function __construct(
        ProjectionRankingRedRepositoryInterface $projectionRakingRedRepository,
        EntityManager $entityManager
    )
    {
        $this->projectionRankingRedRepository = $projectionRakingRedRepository;
        $this->em = $entityManager;
    }

    public function generate($officeTransformRedRanking)
    {
        foreach ($officeTransformRedRanking as $ranking) {

            $projectionRankingRed = $this->projectionRankingRedRepository
                ->findOneByHistoricRedAccumulatedOrFail($ranking['historic_red_accumulated_id']);

            $position = $projectionRankingRed::LAST_UP;

            if ($ranking['ranking'] > $projectionRankingRed->getRankingOfficesTransform()) {
                $position = $projectionRankingRed::LAST_DOWN;
            }

            if ($ranking['ranking'] == $projectionRankingRed->getRankingOfficesTransform()) {
                $position = $projectionRankingRed::LAST_EQUAL;
            }

            if ($projectionRankingRed->getRankingOfficesTransform() === null) {
                $position = $projectionRankingRed::LAST_EQUAL;
            }

            $projectionRankingRed->updateRankingOfficesTransform(
                $ranking['ranking'],
                $position
            );

            $this->projectionRankingRedRepository
                ->createOrUpdate($projectionRankingRed);
        }
        $this->em->flush();
        $this->em->clear();

    }
}
