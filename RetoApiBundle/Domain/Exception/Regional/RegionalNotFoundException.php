<?php

namespace RetoApiBundle\Domain\Exception\Regional;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class RegionalNotFoundException extends NotFoundException
{
    /**
     * RegionalNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("regional.exception.not_found", 400);
    }
}
