<?php

namespace RetoApiBundle\Domain\Exception\HistoricRegionalAccumulated;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class HistoricRegionalAccumulatedNotFoundException extends NotFoundException
{
    /**
     * HistoricRegionalAccumulatedNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct("historic_regional_accumulated.exception.not_found", 400);
    }
}
