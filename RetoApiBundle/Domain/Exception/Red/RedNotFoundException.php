<?php

namespace RetoApiBundle\Domain\Exception\Red;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class RedNotFoundException extends NotFoundException
{
    /**
     * RedNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("red.exception.not_found", 400);
    }
}
