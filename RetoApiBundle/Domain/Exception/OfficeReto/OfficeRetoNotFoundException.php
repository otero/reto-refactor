<?php

namespace RetoApiBundle\Domain\Exception\OfficeReto;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class OfficeRetoNotFoundException extends NotFoundException
{
    /**
     * OfficeRetoNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("office_reto.exception.not_found", 400);
    }
}
