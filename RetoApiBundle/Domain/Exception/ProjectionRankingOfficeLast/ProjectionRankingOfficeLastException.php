<?php

namespace RetoApiBundle\Domain\Exception\ProjectionRankingOfficeLast;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class ProjectionRankingOfficeLastException extends NotFoundException
{
    /**
     * ProjectionRankingOfficeNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("projection_ranking_office_last.exception.not_found", 400);
    }
}
