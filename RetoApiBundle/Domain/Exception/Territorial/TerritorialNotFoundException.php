<?php

namespace RetoApiBundle\Domain\Exception\Territorial;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class TerritorialNotFoundException extends NotFoundException
{
    /**
     * TerritorialNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("territorial.exception.not_found", 400);
    }
}
