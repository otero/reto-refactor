<?php

namespace RetoApiBundle\Domain\Exception\Target;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class TargetMeanNotFoundException extends NotFoundException
{
    /**
     * TargetMeanNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("target_mean.exception.not_found", 400);
    }
}
