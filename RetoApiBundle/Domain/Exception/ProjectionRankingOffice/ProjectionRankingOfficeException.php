<?php

namespace RetoApiBundle\Domain\Exception\ProjectionRankingOffice;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class ProjectionRankingOfficeException extends NotFoundException
{
    /**
     * ProjectionRankingOfficeNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("projection_ranking_office.exception.not_found", 400);
    }
}
