<?php

namespace RetoApiBundle\Domain\Exception\Challenge;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class ChallengeNotFoundException extends NotFoundException
{
    /**
     * ChallengeFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct("challenge.exception.not_found", 400);
    }
}
