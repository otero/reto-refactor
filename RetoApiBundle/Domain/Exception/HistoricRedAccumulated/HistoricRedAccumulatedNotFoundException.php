<?php

namespace RetoApiBundle\Domain\Exception\HistoricRedAccumulated;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class HistoricRedAccumulatedNotFoundException extends NotFoundException
{
    /**
     * HistoricRedAccumulatedNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct("historic_red_accumulated.exception.not_found", 400);
    }
}
