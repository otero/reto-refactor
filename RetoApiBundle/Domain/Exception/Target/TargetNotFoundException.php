<?php

namespace RetoApiBundle\Domain\Exception\Target;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class TargetNotFoundException extends NotFoundException
{
    /**
     * TargetNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("target.exception.not_found", 400);
    }
}
