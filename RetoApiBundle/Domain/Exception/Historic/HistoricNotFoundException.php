<?php

namespace RetoApiBundle\Domain\Exception\Historic;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class HistoricNotFoundException extends NotFoundException
{
    /**
     * HistoricFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct("historic.exception.not_found", 400);
    }
}
