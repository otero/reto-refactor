<?php

namespace RetoApiBundle\Domain\Exception\Common;

/**
 * Class NotFoundException
 * @package RetoApiBundle\Domain\Exception\Common
 */
abstract class NotFoundException extends \Exception
{

}

