<?php

namespace RetoApiBundle\Domain\Exception\RankingRed;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class RankingRedNotFoundException extends NotFoundException
{
    /**
     * RankingRedNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("ranking_red.exception.not_found", 400);
    }
}
