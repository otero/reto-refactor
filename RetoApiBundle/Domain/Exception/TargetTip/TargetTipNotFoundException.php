<?php

namespace RetoApiBundle\Domain\Exception\Target;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class TargetTipNotFoundException extends NotFoundException
{
    /**
     * TargetTipNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("target_tip.exception.not_found", 400);
    }
}
