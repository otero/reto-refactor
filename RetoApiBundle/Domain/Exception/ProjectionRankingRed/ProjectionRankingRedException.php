<?php

namespace RetoApiBundle\Domain\Exception\ProjectionRankingRed;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class ProjectionRankingRedException extends NotFoundException
{
    /**
     * ProjectionRankingRedNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("projection_ranking_red.exception.not_found", 400);
    }
}
