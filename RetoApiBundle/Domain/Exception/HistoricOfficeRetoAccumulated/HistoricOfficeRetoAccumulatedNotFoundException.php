<?php

namespace RetoApiBundle\Domain\Exception\HistoricOfficeRetoAccumulated;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class HistoricOfficeRetoAccumulatedNotFoundException extends NotFoundException
{
    /**
     * HistoricOfficeRetoAccumulatedNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct("historic_office_reto_accumulated.exception.not_found", 400);
    }
}
