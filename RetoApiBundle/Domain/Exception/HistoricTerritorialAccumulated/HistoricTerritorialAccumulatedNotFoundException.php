<?php

namespace RetoApiBundle\Domain\Exception\HistoricTerritorialAccumulated;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class HistoricTerritorialAccumulatedNotFoundException extends NotFoundException
{
    /**
     * HistoricTerritorialAccumulatedNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct("historic_territorial_accumulated.exception.not_found", 400);
    }
}
