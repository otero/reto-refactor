<?php

namespace RetoApiBundle\Domain\Exception\ProjectionRankingRegional;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class ProjectionRankingRegionalException extends NotFoundException
{
    /**
     * ProjectionRankingRegionalNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("projection_ranking_regional.exception.not_found", 400);
    }
}
