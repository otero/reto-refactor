<?php

namespace RetoApiBundle\Domain\Exception\ProjectionRankingTerritorial;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class ProjectionRankingTerritorialException extends NotFoundException
{
    /**
     * ProjectionRankingTerritorialNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct("projection_ranking_territorial.exception.not_found", 400);
    }
}
