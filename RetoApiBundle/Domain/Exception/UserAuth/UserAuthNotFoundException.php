<?php

namespace RetoApiBundle\Domain\Exception\Historic;


use RetoApiBundle\Domain\Exception\Common\NotFoundException;

class UserAuthNotFoundException extends NotFoundException
{
    public function __construct()
    {
        parent::__construct("user_auth.exception.not_found", 400);
    }
}
