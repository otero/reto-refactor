<?php

namespace RetoApiBundle\Domain\Repository;


interface FileRepositoryInterface
{
    public function upload($pathFile);

    public function get($pathFile);
}