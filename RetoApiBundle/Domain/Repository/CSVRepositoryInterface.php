<?php

namespace RetoApiBundle\Domain\Repository;


interface CSVRepositoryInterface
{
    public function get($file);
}