<?php

namespace RetoApiBundle\Domain\Repository;


interface UserAuthRepositoryInterface
{
    public function findUser();

    public function findUserOrFail();
}