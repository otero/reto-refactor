<?php

namespace RetoApiBundle\Util;


class Utils {

    /**
     * @param $number
     * @return float
     */
    static public function transformStringToFloat($number)
    {
        $number = str_replace(",", ".", $number);
        return floatval($number);
    }

    /**
     * @return float
     */
    static public function microTimeFloat()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
}
