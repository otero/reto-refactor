<?php

namespace RetoApiBundle\Controller;


use RetoApiBundle\Application\DTO\GetChallengeRankingOffice;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ChallengeRankingOfficeController extends Controller
{
    public function challengeRankingOfficeAction(Request $request)
    {
        $challengeRankingOfficeUseCase = $this->get('reto.challenge_ranking_office.use_case');
        $challengeRankingOffice = $challengeRankingOfficeUseCase->execute(
            new GetChallengeRankingOffice(
                array_filter(explode(',', $request->query->get('regional', null))),
                array_filter(explode(',', $request->query->get('red', null))),
                array_filter(explode(',', $request->query->get('territorial', null))),
                $request->query->get('limit', null)
            )
        );

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array (
                'data' =>  $challengeRankingOffice,
                'http_code' => Response::HTTP_OK
            ),
            Response::HTTP_OK,
            $headers
        );
    }

    public function topOfficeAction()
    {
        $topOffice = $this->get('reto.challenge_ranking_top_office.use_case');

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' => $topOffice->execute(), 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );
    }
}