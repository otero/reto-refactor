<?php

namespace RetoApiBundle\Controller;


use RetoApiBundle\Application\DTO\GetRed;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RedController extends Controller
{
    public function redAction(Request $request)
    {
        $getRed = $this->get('reto.get_red.use_case');

        $reds = $getRed->execute(
            new GetRed(
                array_filter(explode(',', $request->query->get('territorial', null)))
            )
        );

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' => $reds, 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );
    }
}