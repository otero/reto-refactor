<?php

namespace RetoApiBundle\Controller;


use RetoApiBundle\Domain\Exception\OfficeReto\OfficeRetoNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ChallengeController extends Controller
{
    public function myChallengeAction()
    {

        $diamondMedal ="";
        try {
            $challengeUseCase = $this->get('reto.challenge_user.use_case');
            $myChallenge = $challengeUseCase->execute();

            $ranking = $this->get('reto.top_five_ranking_red.use_case');
            $topFive = $ranking->execute();

            $ranking = $this->get('reto.ranking_my_office.use_case');
            $rankingMyOffice = $ranking->execute();

            $hierarchy = 'low_level';

        } catch (OfficeRetoNotFoundException $e) {

            $challengeUseCase = $this->get('reto.challenge_high_level.use_case');
            $myChallenge = $challengeUseCase->execute();

            $ranking = $this->get('reto.top_five_ranking_territorial.use_case');
            $topFive = $ranking->execute();

            if (isset($topFive['diamondMedal'])){
                $diamondMedal = $topFive['diamondMedal'];
                unset($topFive['diamondMedal']);
            }

            $rankingMyOffice = [];
            $hierarchy = 'high_level';
        }

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
                array('data' =>
                    array(
                        'hierarchy' => $hierarchy,
                        'my-challenge' => $myChallenge,
                        'top-five' => $topFive,
                        'diamond-medal' => $diamondMedal,
                        'ranking-my-office' => $rankingMyOffice
                         ),
                    'http_code' => Response::HTTP_OK
                ),
                Response::HTTP_OK,
                $headers
            );
    }

}
