<?php

namespace RetoApiBundle\Controller;


use RetoApiBundle\Application\DTO\GetChallengeRankingRed;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ChallengeRankingRedController extends Controller
{
    public function challengeRankingRedAction(Request $request)
    {
        $challengeRankingRedUseCase = $this->get('reto.challenge_ranking_red.use_case');
        $challengeRankingRed = $challengeRankingRedUseCase->execute(
            new GetChallengeRankingRed(
                array_filter(explode(',', $request->query->get('territorial', null)))
            )
        );

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array(
                'data' => $challengeRankingRed,
                'http_code' => Response::HTTP_OK
            ),
            Response::HTTP_OK,
            $headers
        );
    }

}