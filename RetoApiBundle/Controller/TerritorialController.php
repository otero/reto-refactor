<?php

namespace RetoApiBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class TerritorialController extends Controller
{
    public function territorialAction()
    {
        $getTerritorial = $this->get('reto.get_territorial.use_case');

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' => $getTerritorial->execute(), 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );
    }
}