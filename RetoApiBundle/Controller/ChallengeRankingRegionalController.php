<?php

namespace RetoApiBundle\Controller;


use RetoApiBundle\Application\DTO\GetChallengeRankingRegional;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ChallengeRankingRegionalController extends Controller
{
    public function challengeRankingRegionalAction(Request $request)
    {
        $challengeRankingRegionalUseCase = $this->get('reto.challenge_ranking_regional.use_case');
        $challengeRankingRegional = $challengeRankingRegionalUseCase->execute(
            new GetChallengeRankingRegional(
                array_filter(explode(',', $request->query->get('red', null))),
                array_filter(explode(',', $request->query->get('territorial', null)))
            )
        );

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' => $challengeRankingRegional, 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );
    }

}