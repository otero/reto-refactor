<?php

namespace RetoApiBundle\Controller;


use RetoApiBundle\Application\DTO\GetChallengeRankingRed;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ChallengeRankingTerritorialController extends Controller
{
    public function challengeRankingTerritorialAction(Request $request)
    {
        $challengeRankingTerritorialUseCase = $this->get('reto.challenge_ranking_territorial.use_case');
        $challengeRankingTerritorial = $challengeRankingTerritorialUseCase->execute();

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' => $challengeRankingTerritorial, 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );
    }

}