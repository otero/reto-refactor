<?php

namespace RetoApiBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ChallengeUserDataController extends Controller
{
    public function challengeUserDataAction()
    {
        $challengeUserDataUseCase = $this->get('reto.challenge_user_data.use_case');

        $data = $challengeUserDataUseCase->execute();

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' => $data, 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );
    }

}