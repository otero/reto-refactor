<?php

namespace RetoApiBundle\Controller;

use PtiBundle\Entity\HomeReto;
use PtiBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * Default action to show page "Reto Onficina"
     * @return Response
     */
    public function indexAction()
    {
        /** @var User $user */

        $user = $this->getUser();
//        $user_office_num = $user->getOffice()->getNum();


        return $this->render(
            'RetoApiBundle:Default:index.html.twig', [
//                'current_page' => 'reto'
            ]
        );
    }




    /**
     * Get all FAQs to show on Reto-Home
     * @param Request $request
     * @return Response
     */
    public function getHomeFaqsAction(Request $request)
    {
        $faqRetoService = $this->get('reto.faq.service');
        $data = $faqRetoService->findHomeFaqs();

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return new JsonResponse(['data' => $data], Response::HTTP_OK, $headers);
    }

    /**
     * Get all FAQs to show
     * @param Request $request
     * @return JsonResponse
     */
    public function getFaqsAction(Request $request)
    {
        $faqRetoService = $this->get('reto.faq.service');
        $data = $faqRetoService->findFaqs($request);

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return new JsonResponse(['data' => $data], Response::HTTP_OK, $headers);
    }
}