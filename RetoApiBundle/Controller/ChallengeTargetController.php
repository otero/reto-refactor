<?php

namespace RetoApiBundle\Controller;


use RetoApiBundle\Application\DTO\GetTarget;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ChallengeTargetController extends Controller
{
    public function challengeTargetAction(Request $request)
    {
        $challengeTargetUseCase = $this->get('reto.challenge_target.use_case');

        // @todo AG: this is hacky - we are passing in the Request so we can return absolute URI for image
        $data = $challengeTargetUseCase->execute(
            new GetTarget(
                $request->query->get('bsIndicator', null)
            ),
            $request
        );

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' => $data, 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );
    }

}