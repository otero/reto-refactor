<?php

namespace RetoApiBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class SwitchController extends Controller
{
    public function changeOfficeAction()
    {
        $changeUserForHasOfficeRetoUseCase = $this->container->get('reto.change_user_office_reto.use_case');
        $changeUserForHasOfficeRetoUseCase->execute();

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
                array('data' =>
                    array(
                        'mode' => "OFFICE MODE",

                    ),
                    'http_code' => Response::HTTP_OK
                ),
                Response::HTTP_OK,
                $headers
            );
    }

    public function changeSSCCAction()
    {
        $changeUserForHasOfficeRetoUseCase = $this->container->get('reto.change_user_SSCC_reto.use_case');
        $changeUserForHasOfficeRetoUseCase->execute();

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' =>
                array(
                    'mode' => "SSCC MODE",

                ),
                'http_code' => Response::HTTP_OK
            ),
            Response::HTTP_OK,
            $headers
        );
    }

}
