<?php

namespace RetoApiBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class OfficeController extends Controller
{
    public function officeAction()
    {
        $getOffices = $this->get('reto.get_offices.use_case');

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' => $getOffices->execute(), 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );
    }
}