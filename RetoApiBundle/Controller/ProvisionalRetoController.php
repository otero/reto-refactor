<?php

namespace RetoApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use RetoApiBundle\Application\DTO\GetTarget;
use Doctrine\ORM\Query;

class ProvisionalRetoController extends Controller
{


    public function targetsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $dql = "SELECT t.bsIndicador , t.name ,ti.percentaje as percentaje,ti.sub_percentaje as sub_percentaje,ti.features as description_features
                        FROM RetoApiBundle:TargetInformation ti
                        JOIN ti.target t 
                        ORDER BY t.bsIndicador ASC";


        $targets = $em
            ->createQuery($dql)
            ->getResult(Query::HYDRATE_ARRAY);
        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        if(file_exists ("uploads/top5.png"))
        {
            $img_route = "/uploads/top5.png";
        }
        else
        {
            $img_route = null;
        }
        $data= array("targets"=>$targets,"img_route"=>$img_route);

        return JsonResponse::create(
            array('data' => $data, 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );

    }

   public function resourcesAction(Request $request)
    {
        $challengeTargetUseCase = $this->get('reto.challenge_target.use_case');

        // @todo AG: this is hacky - we are passing in the Request so we can return absolute URI for image
        $data = $challengeTargetUseCase->execute(
            new GetTarget(
                $request->query->get('bsIndicator', null)
            ),
            $request
        );

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' => $data, 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );
    }


    public function retoTypePageAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $reto_page = $em->getRepository("PtiBundle:Configuration")->findOneByName("reto_page");

        $reto_page = $reto_page?$reto_page->getValue():"real";

        $data = array("reto_page"=>$reto_page);

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' => $data, 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );
    }



}
