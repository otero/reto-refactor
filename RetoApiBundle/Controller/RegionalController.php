<?php

namespace RetoApiBundle\Controller;


use RetoApiBundle\Application\DTO\GetRegional;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RegionalController extends Controller
{
    public function regionalAction(Request $request)
    {
        $getRegionalUseCase = $this->get('reto.get_regional.use_case');

        $regional = $getRegionalUseCase->execute(
            new GetRegional(
                array_filter(explode(',', $request->query->get('red', null)))
            )
        );

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' => $regional, 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );
    }

    public function estructureAction()
    {
        $getRegional = $this->get('reto.get_regional.use_case');

        $headers = array();
        if ($this->getParameter('reto.access_control.send_headers')){
            $headers = $this->getParameter('reto.access_control.headers');
        }

        return JsonResponse::create(
            array('data' => $getRegional->estructure(), 'http_code' => Response::HTTP_OK),
            Response::HTTP_OK,
            $headers
        );
    }

}